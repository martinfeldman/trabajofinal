



SELECT prof.profesional_nombre, prof.profesional_apellido, tipologias.tipologia as tipologia  from expedientes as expt
inner join obras on expt.obra_id = obras.obra_id
inner join propietarios as prop on obras.propietario_id = prop.propietario_id

inner join profesionales as prof on expt.profesional_id = prof.profesional_id
inner join tipologias on expt.tipologia_id = tipologias.tipologia_id

group by prof.profesional_nombre, prof.profesional_apellido
 ;
