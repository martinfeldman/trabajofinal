






-- estados

INSERT INTO `estados`(`estado`) VALUES ('abierto');
INSERT INTO `estados`(`estado`) VALUES ('cerrado');


-- objeto

INSERT INTO `objetos`(`objeto`) VALUES ('nuevo');
INSERT INTO `objetos`(`objeto`) VALUES ('existente');




-- tipologías

INSERT INTO `tipologias`(`tipologia`) VALUES ('Vivienda Unifamiliar');
INSERT INTO `tipologias`(`tipologia`) VALUES ('Vivienda Colectiva');
INSERT INTO `tipologias`(`tipologia`) VALUES ('Equipamientos Comerciales');
INSERT INTO `tipologias`(`tipologia`) VALUES ('Equipamientos Culturales');
INSERT INTO `tipologias`(`tipologia`) VALUES ('Equipamientos Educativos');
INSERT INTO `tipologias`(`tipologia`) VALUES ('Equipamientos Industriales');
INSERT INTO `tipologias`(`tipologia`) VALUES ('Obras Especiales');


-- tipos tareas

-- necesitan plano {
INSERT INTO `tipos_tareas`(`tipo_tarea`) VALUES ('Anteproyecto');
INSERT INTO `tipos_tareas`(`tipo_tarea`) VALUES ('Proyecto');
INSERT INTO `tipos_tareas`(`tipo_tarea`) VALUES ('Dirección de Obra');
INSERT INTO `tipos_tareas`(`tipo_tarea`) VALUES ('Relevamiento');
INSERT INTO `tipos_tareas`(`tipo_tarea`) VALUES ('Remodelación');
INSERT INTO `tipos_tareas`(`tipo_tarea`) VALUES ('Instalación Electríca');
INSERT INTO `tipos_tareas`(`tipo_tarea`) VALUES ('Instalación Gas');
INSERT INTO `tipos_tareas`(`tipo_tarea`) VALUES ('Instalación Agua, Cloaca');
INSERT INTO `tipos_tareas`(`tipo_tarea`) VALUES ('Instalación Contra Incendios');
INSERT INTO `tipos_tareas`(`tipo_tarea`) VALUES ('Ascensores');
INSERT INTO `tipos_tareas`(`tipo_tarea`) VALUES ('Cálculo de estructura');


--}

-- requieren documento {
INSERT INTO `tipos_tareas`(`tipo_tarea`) VALUES ('Plan regulador');
INSERT INTO `tipos_tareas`(`tipo_tarea`) VALUES ('Peritaje');
INSERT INTO `tipos_tareas`(`tipo_tarea`) VALUES ('Representación técnica');
INSERT INTO `tipos_tareas`(`tipo_tarea`) VALUES ('Tasación');
--}




-- condiciones

INSERT INTO `condiciones`(`condicion`, `condicion_short`) VALUES ('Formulario Desaprobado', 'Formulario Desaprobado');
INSERT INTO `condiciones`(`condicion`, `condicion_short`) VALUES ('Formulario a ser revisado por el Colegio', 'Formulario para ser revisado');
INSERT INTO `condiciones`(`condicion`, `condicion_short`) VALUES ('Formulario Desaprobado');
INSERT INTO `condiciones`(`condicion`, `condicion_short`) VALUES ('Formulario Desaprobado');
INSERT INTO `condiciones`(`condicion`, `condicion_short`) VALUES ('Formulario Desaprobado');
INSERT INTO `condiciones`(`condicion`, `condicion_short`) VALUES ('Formulario Desaprobado');
INSERT INTO `condiciones`(`condicion`, `condicion_short`) VALUES ('Formulario Desaprobado');
INSERT INTO `condiciones`(`condicion`, `condicion_short`) VALUES ('Formulario Desaprobado');
INSERT INTO `condiciones`(`condicion`, `condicion_short`) VALUES ('Formulario Desaprobado');
INSERT INTO `condiciones`(`condicion`, `condicion_short`) VALUES ('Formulario Desaprobado');
INSERT INTO `condiciones`(`condicion`, `condicion_short`) VALUES ('Formulario Desaprobado');
INSERT INTO `condiciones`(`condicion`, `condicion_short`) VALUES ('Formulario Desaprobado');
INSERT INTO `condiciones`(`condicion`, `condicion_short`) VALUES ('Formulario Desaprobado');