

 

 Antes, recordar que los posibles Estados de un expediente son:
- abierto 
- cerrado 

<br>

## Condiciones de un expediente

- Formulario para revisión --> es evaluado por Supervisor. Puede ser aprobado  o desaprobado


Si es aprobado, el estado del expediente puede ser [abierto], en cuyo caso, se tiene: {


### Desarrollo habitual de ciclo de vida de un expediente -->

Para poder liquidar, es necesario que al menos haya un plano o documento.. por lo que: 

- Iniciado, a la espera de un plano o documento para liquidar

- Listo para liquidación 


Como se menciona en el caso de uso, cuando un empleado administrativo o supevisor liquida un expediente, 

 el sistema (a través del módulo automatizado 1) envía con notificación al usuario e email datos para proceder con el pago al profesional

y a continuación, dependiendo de si el profesional ha incluido el email del mismo al agregarlo/registrarlo, tenemos dos casos posibles:

1) SI INCLUYÓ EL EMAIL: se envían los datos al propietario;

- A la espera de Carga de Comprobante de Pago; el sistema ha enviado los datos para realizar el pago a ambos, profesional y propietario. 

2) NO INCLUYÓ EL EMAIL: dependerá del profesional que el propietario disponga de los datos para realizar el pago;

Y aquí nos interesa - a través de un elemento tipo check/tilde - que el profesional marque si ha dispuesto al propietario de los datos, de alguna forma. 

A partir de ello se tienen las siguientes dos condiciones: 

- A la espera de Carga de Comprobante de Pago; sin confirmacion (del profesional) de que el propietario dispone de los datos.

- A la espera de Carga de Comprobante de Pago; con confirmación (del profesional) de que el propietario dispone de los datos.


- Para cerrar (luego de que Empleado Administrativo haya confirmado la autenticidad del Comprobante de Pago).



<br> </br>



##### Casos de Uso que pueden repercutir en el ciclo de vida habitual de un expediente:





#####- Aprobar Reemplazo Profesional : 
        
        ACLARACION: el profesional 1 y expediente 1 hacen referencia al saliente y su expediente, que va a ser modificado.
        Profesional entrante es quien solicita reemplazar al profesional 1 y un formulario para el expediente 2  es creado con habitualidad para registrar la tarea de profesional 2.
        
        Punto de partida: sólo existe Expediente 1. 



</br> 
### Desarrollo de ciclos de vida de expedientes con Reemplazo Profesional

        0. Solicitud enviada por Profesional 2. Ninguna condicion es requerida aún.
        1. Empleado Administrativo aprueba -> 
             a)  se modifica el expediente 1 incorporando en forma textual el evento de Reemplazo de Profesional dentro del expediente. 

        Tras ser aprobado un Reemplazo de Profesional, el módulo automatizado: 
        - notificará al profesional 1 del evento,
<br>   



### Condiciones de expedientes 1 y 2 en su Ciclo de vida con Caso Reemplazo Profesional: 

        Expediente 1:
        1. Tras ser aprobado el Reemplazo Profesional, si el expediente posee otras tareas además de Dirección de Obra, 
        entonces seguirá el ciclo de vida habitual de un expediente con esas tareas. 
        En cambio, si es la única tarea del expediente se cierra.
        En ambos casos la única información que el expediente 1 conoce sobre el Reemplazo Profesional es una simple observación 

        Expediente 2:
        1. Se crea y sigue el ciclo de vida habitual de un expediente


        - Entonces un formulario de Expediente 2 es creado para el profesional 2 con los datos que ya ha sumunistrado en la solicitud (profesional, propietario, ubicacion, datos catasrales y partida/s inmobiliaria/s).
        Este expediente(2) contendrá unicamente la tarea Dirección de Obra y acceso al archivo de Nota Reemplazo Profesional que fue presentado al Empleado Adminstrativo, el valor y la foto que el Avance de Obra. 
        Cuando sea requerido, este profesional (2) podrá agregar más tareas desde el caso de uso Modificar Expediente.

}



Segun estado: [cerrado] {

- Expediente cerrado, sin Direccion de Obra Finalizada
- Expediente cerrado, con Direccion de Obra Finalizada
}









