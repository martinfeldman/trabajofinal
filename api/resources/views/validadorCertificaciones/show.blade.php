@extends ('layouts.admin')



{{--
    /**
    /*
    /*
        VISTA DE EXPEDIENTES SHOW

        PANTALLA IMPORTANTE QUE EXPONE LA INFORMACIÓN DE UN EXPEDIENTE EN PARTICULAR, Y
        EN LA CUAL APARECEN BOTONES PARA QUE LOS USUARIOS DE TODOS LOS ROLES PUEDAN LLEVAR A CABO LOS CASOS DE USO
        DE ACUERDO A LA CONDICIÓN DEL EXPEDIENTE
        
        La condición es el estado en los ciclos de vida que puede tener éste, tanto en el ideal (habitual)
        como en el que extiende a otros casos de uso (menos habitual).

        -> VARIABLES DE ENTRADA: 
    

    Para poder mostrar un expediente, esta vista recibe 
    
    - un expediente,
    - datosExtraExpediente
       


    Los profesionales pueden abrir expedientes exclusivamente para sí mismos



        -> SALIDA: 


        Expediente 



        -> HISTORIAS DE USUARIO RELACIONADAS: 


        - Crear Expediente

    */ 
--}}
    
   





   

    {{-- {{dd($expediente->comprobante_pago_id)}} --}}
    {{-- {{dd($dataExtraExpediente[0]);}} --}}
    {{-- {{dd($dataExtraExpediente[0]);}} --}}
    {{-- {{ $test = $data[0]}} --}}
    {{dd($expediente);}}


 

@section('contenido')
    
   
<div class="container col-lg-12 col-md-12 col-sm-12 col-xs-12">

    <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12" name="title">
    
       

            <h2 class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >
            
                @if ($expediente->condicion_id == 1)
                    <strong>Viendo Formulario de Expediente </strong>

                @else 
                    <strong>Viendo Expediente </strong>    
                @endif
            </h2>
            {{-- <br> --}}
            
    
            
            
            @if (count($errors)>0)    
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li> {{$error}} </li>
                        @endforeach
                    
                    </ul>
                </div>
            @endif    
              
       

    </div> <!-- .row  name="title"-->
    


    
            


            {!! Form::open(array('url'=>'expediente','method'=>'POST','autocomplete'=>'off', 'files'=>'true')) !!}
            {{ Form::token() }}


            {{-- VOY A TRATAR DE ELIMINAR EL ERROR EN LA POSICION DEL NAV BAR EN ESTA VISTA  --}}
            {{-- <div class="container col-lg-12 col-md-12 col-sm-12 col-xs-12"> --}}



                <div class="row col-lg-8 col-sm-8 col-md-8 col-xs-8" id="columna-izquierda">



                    <div class="row col-lg-12 col-sm-12 col-md-12 col-xs-12" name="primerFila">
                        
                        <div class="col-lg-7 col-sm-7 col-md-7 col-xs-7">
                            <h3><strong>NÚMERO : {{$expediente->expediente_numero}}</strong></h3>
                        </div>

                        <div class="col-lg-5 col-sm-5 col-md-5 col-xs-5">
                            <h3><strong>FECHA DE INICIO :</strong> {{$expediente->fecha_inicio}}</h3>
                        </div>

                    </div> <!-- .row  name="primerFila" -->





                    <div class="row col-lg-12 col-sm-12 col-md-12 col-xs-12" name="segundaFila">


                        <div class="col-lg-8 col-sm-8 col-md-8 col-xs-8">

                            <!-- Profesional -->

                            <h3><strong>PROFESIONAL :</strong>

                                {{$dataExtraExpediente[0]->profesional_nombres}}
                                {{$dataExtraExpediente[0]->profesional_apellidos}}

                            </h3>

                        </div>   
                    
                    


                        <div class="col-lg-4 col-sm-4 col-md-4 col-xs-4">
                       
                            <h3><strong>CUIT :</strong>
                                {{$dataExtraExpediente[0]->profesional_numero_matricula}}
                        
                            </h3>

                        </div>    


                    </div>    <!-- .row  name="segundaFila" -->




                    <div class="row col-lg-12 col-sm-12 col-md-12 col-xs-12" name="tercerFila">



                        <div class="col-lg-8 col-sm-8 col-md-8 col-xs-8">
                        
                            <!--  Propietarios -->

                            <h3><strong>PROPIETARIO :</strong> 
                        
                                {{$dataExtraExpediente[0]->propietario_nombres}}
                                {{$dataExtraExpediente[0]->propietario_apellidos}}
                          
                        
                            </h3> 

                        </div>
                    
                    
                        <div class=" col-lg-4 col-sm-4 col-md-4 col-xs-4">
                        
                            <h3><strong>CUIT :</strong>
                                {{$dataExtraExpediente[0]->propietario_cuit}} 
                            
                            </h3>

                        </div>    

                    
                    

                   
                    </div>    <!-- .row  name="tercerFila--> 

                    
                    
                    
                  



                <div class="row col-lg-12 col-sm-12 col-md-12 col-xs-12" name="TareasPlanosDocumentos">
                    

                    <!-- Tareas y Planos/Documentos -->

                    <br>
                    <h3 class="col-lg-12 col-sm-12 col-md-12 col-xs-12"><strong>TAREAS Y PLANOS / DOCUMENTOS</strong></h3> 




                    <div class="col-lg-6 col-sm-6 col-md-6 col-xs-6">

                        <!-- Tareas -->

                        <h4> <strong>TAREA :</strong> 
                            {{$dataExtraExpediente[0]->tipo_tarea}}
                        </h4>

                    </div>

               

                    


                     
                    
                    <div class="col-lg-6 col-sm-6 col-md-6 col-xs-6">

                        <!-- Plano/s o documento/s -->       
                        <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">

                            <h4 class="col-lg-8 col-sm-8 col-md-8 col-xs-8" ><strong>ARCHIVO :</strong> 
                                {{$dataExtraExpediente[0]->nombre_archivo}} 
                                
                            </h4>

                            {!! Form::open(array('url'=>'expedientes','method'=>'post','autocomplete'=>'off', 'files'=>true)) !!}
                            {{ Form::token() }}
                            {{-- @method('GET') --}}
                            {{csrf_field()}}

                                <div class="form-group row col-lg-4 col-sm-4 col-md-4 col-xs-4">
                                <form method="post" enctype="multipart/form-data" action="{{route('expedientes.descargarPlanoODocumento', ['id' => $expediente->expediente_id, 'nombre_archivo' =>$dataExtraExpediente[0]->nombre_archivo])}}">
                                    @csrf

                                    <a href="{{route('expedientes.descargarPlanoODocumento', ['id' => $expediente->expediente_id, 'nombre_archivo' =>$dataExtraExpediente[0]->nombre_archivo])}}" {{-- data-target="#modal-DescargarComprobantePago{{$expediente->expediente_id}}" data-toggle="modal" --}}>
                                        <button class="btn col-lg-12 col-sm-12 col-md-12 col-xs-12 btn btn-info" type="submit" 
                                         formaction="{{url('/expedientes/descargarPlanoODocumento', ['id' => $expediente->expediente_id, 'nombre_archivo' =>$dataExtraExpediente[0]->nombre_archivo])}}">
                                            Descargar Archivo
                                        </button>
                                    </a>

                                    <br><br>

                                </form>
                                </div> <!-- .form-group row -->     

                            {!!Form::close()!!}    


                        </div>

                    </div>
                    
                    
                </div>   <!-- .row  name="TareasPlanosDocumentos" -->



                    
                    
                  

                <div class="row col-lg-12 col-sm-12 col-md-12 col-xs-12" name="obra">

                    <!-- Obra -->
                
                    
                    <h3 class="col-lg-12 col-sm-12 col-md-12 col-xs-12"><strong>OBRA</strong></h3>  


                    
                    <div class="col-lg-6 col-sm-6 col-md-6 col-xs-6">
                            <!-- Objetos -->

                            <h4><strong>OBJETO :</strong> 
                                {{$dataExtraExpediente[0]->objeto}} 
                            </h4>
                    
                    </div>







                    <div class="col-lg-6 col-sm-6 col-md-6 col-xs-6">

                        <!-- Tipología -->

                        <h4><strong>TIPOLOGÍA :</strong> 
                            {{$dataExtraExpediente[0]->tipologia}} 
                        </h4>

                    
                    </div> 

                
                </div>  <!-- .row   name="obra" -->






               






                    
                    
               

                <div class="row col-lg-12 col-sm-12 col-md-12 col-xs-12" name="ubicacion">


                    <!-- UBICACIÓN -->

                    

                    <h3 class="col-lg-12 col-sm-12 col-md-12 col-xs-12 "><strong>UBICACIÓN</strong></h3>             
                        
      



                        
                    <div class="col-lg-3 col-sm-3 col-md-3 col-xs-3">

                       <!--  Localidad -->
                              
                        <h4><strong>LOCALIDAD :</strong> 
                           {{$dataExtraExpediente[0]->localidad}}  
                        </h4>

                    </div>






                    <div class="col-lg-3 col-sm-3 col-md-3 col-xs-3">

                        <!-- Calle -->   

                        <h4><strong>CALLE :</strong> 
                            {{$dataExtraExpediente[0]->calle}} 
                        </h4>
                            
    
                    </div>
                   







                    <div class="col-lg-3 col-sm-3 col-md-3 col-xs-3">

                        <!-- Número -->     

                        <h4><strong>NÚMERO :</strong> 
                            {{$dataExtraExpediente[0]->numero}}              
                        </h4>

                    </div>





                    <div class="col-lg-3 col-sm-3 col-md-3 col-xs-3">

                        <!-- Barrio --> 

                        <h4><strong>BARRIO :</strong>
                            {{$dataExtraExpediente[0]->barrio}} 
                        </h4>

                    </div>
                    
                    

                </div>     <!-- .row Ubicación -->

                   


                    

                   

                <div class="row col-lg-12 col-sm-12 col-md-12 col-xs-12" name="datosCatastrales">

                        <!-- Datos catastrales -->

                        <br>
                        <h3 class="col-lg-12 col-sm-12 col-md-12 col-xs-12"><strong> DATOS CATASTRALES</strong></h3>  
                    



                        <div class="col-lg-2 col-sm-2 col-md-2 col-xs-2">
                        <!-- Sección -->     

                            <h4><strong>SECCIÓN :</strong>
                                {{$dataExtraExpediente[0]->seccion}} 
                            </h4>   

                        </div>





                        <div class="col-lg-2 col-sm-2 col-md-2 col-xs-2">

                            <!-- Manzana --> 

                            <h4><strong>MANZANA :</strong>
                                 {{$dataExtraExpediente[0]->manzana}} 
                            </h4>

                        </div>





                        <div class="col-lg-2 col-sm-2 col-md-2 col-xs-2">

                            <!-- Chacra -->  

                            <h4><strong>CHACRA :</strong>
                                 {{$dataExtraExpediente[0]->chacra}} 
                            </h4>

                        </div>





                        <div class="col-lg-2 col-sm-2 col-md-2 col-xs-2"> 

                            <!-- Parcela -->        

                            <h4><strong>PARCELA :</strong>
                                 {{$dataExtraExpediente[0]->parcela}} 
                            </h4>   
                        
                        
                        </div>




                        <div class="col-lg-4 col-sm-4 col-md-4 col-xs-4"> 

                            <!-- Partida/s Inmobiliaria/s -->

                            <h4><strong>PARTIDA INMOBILIARIA :</strong> 
                                {{$dataExtraExpediente[0]->partida_inmobiliaria_id}}
                            </h4>
                        
                        </div>
                        
                        
                       

                </div>   <!-- .row    name="datosCatastrales"-->
                        
                    
                    



                    
                    
                    





                   


                     




                    <div class="row col-lg-12 col-sm-12 col-md-12 col-xs-12" name="superficies">


                        <!-- SUPERFICIE -->
                    
                        <br>
                        <h3 class="col-lg-12 col-sm-12 col-md-12 col-xs-12"><strong>SUPERFICIE (en m²)</strong></h3> 






                        <div class="col-lg-4 col-sm-4 col-md-4 col-xs-4">

                            <!-- Superficie a constuir -->  

                            <h4><strong>A CONSTRUIR :</strong> 
                                
                                    @if ($expediente->superficie_a_construir == null) 
                                        -
                                    @else
                                        {{$expediente->superficie_a_construir}}
                                    @endif

                            </h4>
                    
                        </div> 






                        <div class="col-lg-4 col-sm-4 col-md-4 col-xs-4">
                            
                            <!-- Superficie con permiso -->     
                        
                            <h4><strong>CON PERMISO :</strong>


                                @if ($expediente->superficie_con_permiso == null) 
                                    0
                                @else
                                    {{$expediente->superficie_con_permiso}}
                                @endif

                            </h4>
                        
                        </div> 
                    
                    
                    
                    
                    
                    
                    
                    
                        <div class="col-lg-4 col-sm-4 col-md-4 col-xs-4">
                            
                            <!-- Superficie sin permiso --> 
                        
                            <h4><strong>SIN PERMISO :</strong> 
                               

                                @if ($expediente->superficie_sin_permiso == null) 
                                    0
                                @else
                                    {{$expediente->superficie_sin_permiso}}
                                @endif

                            </h4>
                        
                        </div> 


    
                    </div>   <!-- .row  name="superficies" -->    
                    


                </div>   <!-- .row columna izquierda  -->    
               


                




                <div id="columna-intermedia" class="row col-lg-1 col-sm-1 col-md-1 col-xs-1">

                     {{-- Columna Intermedia para generar espacio --}}

                </div>    






                <div class="row col-lg-3 col-sm-3 col-md-3 col-xs-3" id="columna-derecha">


                    <!-- Botones -->       
                    
                    <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
        
                       

                            <h3><strong>CONDICIÓN</strong></h3>

                            <h4>{{$dataExtraExpediente[0]->condicion}}</h4>






                            <h3><strong>ACCIONES</strong></h3>


                            @can('supervisor.expediente.show')

                            
                                @if ($expediente->condicion_id == 1)

                                <a href="{{route('expedientes.valorarFormularioExpediente', ['id' => $expediente->expediente_id])}}" data-target="#modal-ValorarFormularioExpediente-{{$expediente->expediente_id}}" data-toggle="modal">
                                    <button class="btn btn-success" id="botonValorarFormularioExpediente">Valorar Formulario de Expediente</button>
                                </a>

                                @include('expediente.modal-ValorarFormularioExpediente')

                                <br><br>

                                @endif

                               
                                
                            @endcan
            
                           



                            @can('profesional.expediente.show')
                            
                              @if ($dataExtraExpediente[0]->tipo_tarea === $expedienteQueries->getDireccionDeObra())

                                    <a href="{{-- {{route('expedientes.renunciarDireccionObra', ['id' => $expediente->expediente_id])}} --}}" 
                                        data-target="#modal-RenunciarDireccionObra-{{$expediente->expediente_id}}" data-toggle="modal">
                                            <button class="btn btn-danger">Renunciar a Dirección de Obra</button>
                                    </a>

                                    <br><br>
                                    

                                    @include('expediente.modal-RenunciarDireccionObra')

                                @endif

                            @endcan




                               
                            @if($expediente->condicion_id > 1  && $expediente->condicion_id < 4)


                                @can('supervisor.expediente.show')

                                    <a href="{{route('expedientes.liquidarExpediente', ['id' => $expediente->expediente_id])}}" 
                                        data-target="#modal-LiquidarExpediente-{{$expediente->expediente_id}}" data-toggle="modal">
                                            <button class="btn btn-info">Liquidar Expediente</button>
                                    </a>

                                    <br><br>

                                    @include('expediente.modal-LiquidarExpediente')

                                @endcan    



                                
                            @elseif($expediente->condicion_id > 1 && $expediente->condicion_id < 8)                                   
                            
                                @can('profesional.expediente.show')
                                

                                {!! Form::open(array('url'=>'expedientes','method'=>'post','autocomplete'=>'off', 'files'=>true)) !!}
                                {{ Form::token() }}
                               
                                {{csrf_field()}}

                                    <div class="form-group row col-lg-12 col-sm-12 col-md-12 col-xs-12">
                                    
                                        
                                        {{-- <form method="post" enctype="multipart/form-data" action="{{route('expedientesUseCases.store', ['id' => $expediente->expediente_id])}}"> --}}
                                        <form method="post" enctype="multipart/form-data" action="{{route('expedientes.cargarComprobantePago', ['id' => $expediente->expediente_id])}}">
                                            @csrf
                                            
                                                {{-- <label for="comprobantePago">Cargue el archivo aquí</label> <br> --}}

                                                <!-- Entrada de Comprobante de Pago -->       
                                                <input type="file" name="comprobantePago" class="form-control-file" placeholder="vacío"> 
                                            
                                           


                                            <br>

                                            <div class="form-group row col-lg-12 col-sm-12 col-md-12 col-xs-12">

                                                <a href="">
                                                    <button type="submit"  class="btn btn-success" id="botonAprobarExpediente"
                                                        {{-- formaction="{{url('/expedientesUseCases/store', ['id' => $expediente->expediente_id])}}"> --}}
                                                        formaction="{{url('/expedientes/cargarComprobantePago', ['id' => $expediente->expediente_id])}}">
                                                        Cargar Comprobante de Pago
                                                    </button>
                                                </a>
                                            </div>

                                        </form>
                                    </div> <!-- .form-group row -->     
                                    {!!Form::close()!!}            
    
                                

                                    <br><br>
                                @endcan

                        

                            @endif





                           
                            @if ($expediente->condicion_id > 4 && $expediente->comprobante_pago_id = NULL)   


                            {!! Form::open(array('url'=>'expedientes','method'=>'post','autocomplete'=>'off', 'files'=>true)) !!}
                            {{ Form::token() }}
                            {{-- @method('GET') --}}
                            {{csrf_field()}}

                                <div class="form-group row col-lg-12 col-sm-12 col-md-12 col-xs-12">
                                <form method="post" enctype="multipart/form-data" action="{{route('expedientes.descargarComprobantePago', ['id' => $expediente->expediente_id])}}">
                                    @csrf

                                    <a href="{{route('expedientes.descargarComprobantePago', ['id' => $expediente->expediente_id])}}" {{-- data-target="#modal-DescargarComprobantePago{{$expediente->expediente_id}}" data-toggle="modal" --}}>
                                        <button  type="submit" class="btn btn-info" formaction="{{url('/expedientes/descargarComprobantePago', ['id' => $expediente->expediente_id])}}">
                                            Descargar Comprobante de Pago
                                        </button>
                                    </a>

                                    <br><br>

                                </form>
                                </div> <!-- .form-group row -->     

                            {!!Form::close()!!}            
    


                            
                                @if ($expediente->estado_id == 1 )

                                    @can('administrativo.&.supervisor.botonDesestimarComprobantePago.expediente.show')
                                    <a href="{{route('expedientes.desestimarComprobantePago', ['id' => $expediente->expediente_id])}}" 
                                        data-target="#modal-DescargarComprobantePago{{$expediente->expediente_id}}" data-toggle="modal">
                                            <button class="btn btn-danger">Desestimar Comprobante de Pago</button>
                                    </a>

                                    <br><br>
                                    @endcan
                                
                                @endif

                                
                            @endif
                        
                        

                            @if($expediente->condicion_id == 7)

                                @can('supervisor.expediente.show')  
                                    <a href="" data-target="#modal-CerrarExpediente-{{$expediente->expediente_id}}" data-toggle="modal">
                                        <button class="btn btn-info" id="CerrarExpedienteButton">Cerrar Expediente</button>
                                    </a>
                                    
                                    <br><br>

                                    @include('expediente.modal-CerrarExpediente')
                                @endcan 





                            @elseif($expediente->condicion_id > 7)

                                <h3><strong>CERTIFICADO</strong></h3>

                                <button class="btn">Descargar Certificado </button>

                                <br> <br>

                                <button class="btn">Imprimir Certificado </button>


                                    
                            @endif






                     </div>  
                     


                </div>  <!-- .row columna derecha -->  

                {!!Form::close()!!}

            </div>  <!-- .container -->  


     
             
       

                  
        


       {{--  <script>

            var expediente_condicion_id = <?php echo $expediente->condicion_id; ?> 
            var expediente_condicion = "<?php echo $dataExtraExpediente[0]->condicion; ?>" ;
        
        </script> --}}
        
        
    



@endSection





/* {{-- <script> 
    window.onload = asignarEstadoBotonValorarExpediente();
</script> --}} */
