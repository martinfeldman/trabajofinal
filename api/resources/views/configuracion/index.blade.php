
@extends ('layouts.admin')






















    {{-- {{dd($ultima_configuracion)}} --}}



@section('contenido')




    <div class="row" id="main?" style="min-height: 80vh">     

        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            
            <h2><strong>Configuración</strong></h2>  
            <br><br>
        </div>
        
        
        
        @if (count($errors)>0)    
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li> {{$error}} </li>
                @endforeach
            
            </ul>
        </div>
        @endif    
        
        

        <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12">
            
            <span class="col-lg-6 col-md-12 col-sm-12 col-xs-12 text-left" style="border: 0 ; background-color: white; font-size:150%; font-weight: bold">
                Datos de la entidad
            </span>
            

            {{-- <br><br><br> --}}
                
            <span class="col-lg-6 text-left" style="border: 0 ; background-color: white; font-size:150%; font-weight: bold">
                Datos del sistema
            </span>

            <br><br><br>

            {!! Form::open(array('url'=>'configuracion/editar','method'=>'patch','autocomplete'=>'off', 'files'=>true, 'style'=>'display:inline')) !!}
            {{ Form::token() }}
            {{csrf_field()}}

                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">


                    <div class="row" name="primerFila" id="textos">

                        <div class="col-lg-7 col-sm-7 col-md-7 col-xs-7">
                            
                            <div class="form-group" name="nombre_institucion">
                            
                                <label for="nombre_institucion">NOMBRE</label>           
        
                                <input type="text" name="nombre_institucion" class="form-control" placeholder="Nombre"
                                value="{{ null !== old('nombre_institucion') ? old('nombre_institucion') : $ultima_configuracion->nombre_institucion }}">   
        
                            </div> 
                        </div>
                        
                        <div class="col-lg-12"></div>
                        
        
        
        
                        <div class="col-lg-7 col-sm-7 col-md-7 col-xs-7">
        
                            <div class="form-group" name="email">
                                
                                <label for="email">EMAIL</label> <br>
        
                                <input type="text" name="email" class="form-control" placeholder="Email"
                                value="{{ null !== old('email') ? old('email') : $ultima_configuracion->email }}">   
        
                            </div>       
        
                        </div>


                        <div class="col-lg-12"></div>



        
                        <div class="col-lg-7 col-sm-7 col-md-7 col-xs-7">
        
                            <div class="form-group" name="logo">
        
                                <label for="logo">LOGO</label> <br>
        
                                <input type="file" name="logo" class="form-control" placeholder="vacío"
                                value="{{ null !== old('logo') ? old('logo') : $ultima_configuracion->logo }}"> 
        
                            </div>       
        
                        </div>
                            
                        <div class="col-lg-12"></div>


                
                
                        <h4 class="col-lg-12 col-sm-12 col-md-12 col-xs-12"><strong>UBICACIÓN</strong></h4>  
                        
                        
        
                        <div class="col-lg-7 col-sm-7 col-md-7 col-xs-7">
                            
                            <div class="form-group" name="localidad">
                            
                                <label for="localidad">LOCALIDAD</label>           
        
                                <input type="text" name="localidad" class="form-control" placeholder="Localidad"
                                value="{{ null !== old('localidad') ? old('localidad') : $ultima_configuracion->localidad }}">
                                {{-- <x-combo-box.localidades :content="$localidades" size=12 /> --}}

                            </div> 
                        </div>
                        
                        <div class="col-lg-12"></div>
                        
        
        
        
                        <div class="col-lg-7 col-sm-7 col-md-7 col-xs-7">
        
                            <div class="form-group" name="calle">
                                
                                <label for="calle">CALLE</label> <br>
        
                                <input type="text" name="calle" class="form-control" placeholder="Calle" 
                                value="{{ null !== old('calle') ? old('calle') : $ultima_configuracion->calle }}">   
        
                            </div>       
        
                        </div>

                        <div class="col-lg-12"></div>



        
                        <div class="col-lg-3 col-sm-3 col-md-3 col-xs-3">
        
                            <div class="form-group" name="numero">
        
                                <label for="numero">NÚMERO</label> <br>
        
                                <input type="text" name="numero" class="form-control" placeholder="Número"
                                value="{{ null !== old('numero') ? old('numero') : $ultima_configuracion->numero }}"> 
        
                            </div>       
        
                        </div>
                            
                        <div class="col-lg-12"></div>
        
        

                    </div>  {{-- col --}}

                </div>      {{-- row --}}
                
                
                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">        {{-- columna derecha --}}


                    <div class="row" name="primerFila" id="textos">

                        <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                            
                            <label for="tiempo1erRecordatorio">DIAS PARA ENVIAR EL 1er RECORDATORIO POR MAIL PARA LIQUICACION DE EXPEDIENTE</label>        
                            
                            <div class="form-group col-lg-6" name="tiempo1erRecordatorio">
        
                                <input type="text" name="tiempo1erRecordatorio" class="form-control" placeholder="Días para enviar 1er recordatorio de liquidación" 
                                value="{{ null !== old('tiempo1erRecordatorio') ? old('tiempo1erRecordatorio') : $ultima_configuracion->tiempo_primer_recordatorio_1er_proceso_automatizado }}">   
        
                            </div> 
                        </div>

                        
                        <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                            
                            <label for="tiempo2doRecordatorio">DIAS PARA ENVIAR EL 2do RECORDATORIO POR MAIL PARA LIQUICACION DE EXPEDIENTE</label>        
                            
                            <div class="form-group col-lg-6" name="tiempo2doRecordatorio">
        
                                <input type="text" name="tiempo2doRecordatorio" class="form-control " placeholder="Días para enviar 2do recordatorio de liquidación"
                                value="{{ null !== old('tiempo2doRecordatorio') ? old('tiempo2doRecordatorio') : $ultima_configuracion->tiempo_segundo_recordatorio_1er_proceso_automatizado }}">   
        
                            </div> 
                        </div>


                        <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                            
                            <label for="tiempoCierreExpediente">DIAS PARA CERRAR EXPEDIENTE POR TRAMITE INCONCLUSO</label>        <br>
                            
                            <div class="form-group col-lg-6" name="tiempoCierreExpediente">
        
                                <input type="text" name="tiempoCierreExpediente" class="form-control " placeholder="Días para cerrar el expediente por Trámite Inconcluso"
                                value="{{ null !== old('tiempoCierreExpediente') ? old('tiempoCierreExpediente') : $ultima_configuracion->tiempo_cerrar_expediente_1er_proceso_automatizado }}">   
        
                            </div> 
                        </div>

                        {{-- <div class="col-lg-2"></div> --}}
                        
                        <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
                        

                        <div class="form-group col-lg-10">
                        
                            <a href="{{url('configuracion/editar')}}">
                                <button type="submit" class="btn btn-success col-lg-8" style="font-size:130%">
                                    Guardar
                                </button>
                            </a>

                        </div>
                    
                        <div class="col-lg-2"></div>
                        

                    
                        {!! Form::close() !!}

                    </div>      {{-- row --}}

                </div>          {{-- col --}}

                
        </div>                  {{-- row principal que contiene columnas --}}



        {{-- <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12"> --}}

            <br>

            


        {{-- </div> --}}
        
                

                

        

        



    </div> {{-- class="row" id="main?">   --}}




@endSection









<!-- SELECT2 -->


@push('scripts')

<script>
    $('#localidades').select2();        
</script>


@endpush




