<!DOCTYPE html>
<html lang="en">
<head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
        
        {{-- {{ dd($expediente)}} --}}
        {{-- {{ dd($dataExtraExpediente[0]->objeto)}} --}}


</head>
<body>

    
    <div class="wrapper">

        <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12">
          
          <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
            <img src="{{public_path('/storage/LogosSistema/'.$nombre_archivo_logo)}}" alt="LOGO" height="120" width="200" class="float-left">
          </div>


          <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 float-right">
            <img src="data:image/png;base64,{{$codigoQR}}" width="115" height="115" alt="">  
          </div>

          
        </div>    {{-- row --}}  

        <br><br><br> <br> <br>

    </div>  

    <br>

    <div class="wrapper">

      <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <h2 class="text-center" style="color: orange">CERTIFICADO REGISTRO C.A.M.</h2>
        <br>
      </div>

    </div>
        <div >
            <table class="table table-striped">

                <thead>
              
                </thead>

                <tbody>

                  <tr>
                    <th scope="row"></th>
                    <td class="col-lg-6 col-md-6 col-sm-6 col-xs-6">DATOS DE LA OBRA <br> OBJETO/DESTINO</td>
                    <td class="col-lg-6 col-md-6 col-sm-6 col-xs-6">{{$objetoDestino}}</td>
                  
                  </tr>

                  <tr>
                    <th scope="row"></th>
                    <td class="col-lg-6 col-md-6 col-sm-6 col-xs-6">TIPOLOGIA DE OBRA</td>
                    <td class="col-lg-6 col-md-6 col-sm-6 col-xs-6">{{$dataExtraExpediente[0]->tipologia}}</td>
                    
                  </tr>

                  <tr>
                    <th scope="row"></th>
                    <td>PROPIETARIO</td>
                    <td class="col-lg-6 col-md-6 col-sm-6 col-xs-6">{{$dataExtraExpediente[0]->propietario_nombres}} {{$dataExtraExpediente[0]->propietario_apellidos}}</td>
                    
                  </tr>


                  <tr>
                    <th scope="row"></th>
                    <td>DNI Nº / CUIL / CUIT</td>
                    <td>{{$dataExtraExpediente[0]->propietario_cuit}}</td>
                    
                  </tr>


                  <tr>
                    <th scope="row"></th>
                    <td>UBICACION OBRA</td>
                    <td>
                        CALLE {{$dataExtraExpediente[0]->calle}} {{$dataExtraExpediente[0]->numero}}<br>
                        DATOS CATASTRALES: <br>
                        SECC {{$dataExtraExpediente[0]->seccion}} - MZ {{ $dataExtraExpediente[0]->manzana }} - PARC {{$dataExtraExpediente[0]->parcela}} <br>
                        {{$dataExtraExpediente[0]->localidad}}
                    </td>
                    <td></td>
                    
                    
                  </tr>


                  <tr>
                    <th scope="row"></th>
                    <td>SUPERFICIE M²</td>
                    <td>
                        A CONSTRUIR: {{$dataExtraExpediente[0]->superficie_a_construir}}<br>
                        CON PERMISO: {{$dataExtraExpediente[0]->superficie_con_permiso}}<br>
                        SIN PERMISO: {{$dataExtraExpediente[0]->superficie_sin_permiso}}<br>
                    
                    </td>
                    
                  </tr>

                  <tr>
                    <th scope="row"></th>
                    <td>DATOS DEL PROFESIONAL</td>
                    <td>
                        @if ($dataExtraExpediente[0]->profesional_alta == 1)
                            PROFESIONAL HABILITADO
                        @else 
                            PROFESIONAL NO HABILITADO
                        @endif
                    </td>
                    
                  </tr>


                  <tr>
                    <th scope="row"></th>
                    <td>MATRÍCULA</td>
                    <td>{{$dataExtraExpediente[0]->profesional_numero_matricula}}</td>
                    
                  </tr>


                  <tr>
                    <th scope="row"></th>
                    <td>NOMBRE Y APELLIDO</td>
                    <td>{{$dataExtraExpediente[0]->profesional_nombres}} {{$dataExtraExpediente[0]->profesional_apellidos}}</td>
                    
                  </tr>


                  <tr>
                    <th scope="row"></th>
                    <td>TAREA REGISTRADA</td>
                    <td>{{$dataExtraExpediente[0]->tipo_tarea}}</td>
                    
                  </tr>

                  <tr>
                    <th scope="row"></th>
                    <td>FECHA REGISTRO</td>
                    <td>{{$expediente->fecha_inicio}}</td>
                    
                  </tr>

                  <tr>
                    <th scope="row"></th>
                    <td>Nº EXPEDIENTE</td>
                    <td>{{$expediente->expediente_numero}}</td>
                    
                  </tr>

                

                


                </tbody>

              </table>
        </div>
                  
      </div> {{-- class="wrapper" --}}
</body>
</html>