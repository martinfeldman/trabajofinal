<!DOCTYPE html>
<html lang="en">
<head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>


        <TITLE> 
            INFORME C.A.M. sobre Expedientes con trámites inconclusos
        </TITLE> 
        
        <style> 

            table, th, td { 
                border: 1px solid black; 
                } 

        </style> 
        

        @php
        
            use Carbon\Carbon;

        @endphp

</head>


<body>

    <div class="container col-lg-12 col-md-12 col-sm-12 col-xs-12">
    
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">

            <img src="{{public_path('/storage/LogosSistema/'.$nombre_archivo_logo)}}" alt="LOGO" height="100" width="200" class="float-left">
            
        </div>

        <br>   <br>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 float-right text-center ">

            <h5>{{Carbon::now()->setTimeZone('America/Argentina/Buenos_Aires')->format('d-m-Y H:i:s')}}</h5> 
            
        </div>
        <br> <br> <br>
        
        

    </div> {{-- container --}}



    <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center"> 
        <h2>INFORME C.A.M.</h2> <br> 
    </div>

    <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
        <h4>Objeto: Profesionales con mayor número de Expedientes</h4>  {{-- <br>  --}}
    </div>

    <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <h6>Filtros: <br>

            Tipologias:
            @if($tipologia !== null)

                {{$tipologia->tipologia}};

            @else 

                Todas;

            @endif

            <br>
            Condición de Expediente:
            @if($condicion !== null)

                {{$condicion->condicion}};

            @else

                Todas;

            @endif
 
            <br>
            Franja temporal:
            @if($fecha1 !== null && $fecha2 !== null)

                {{$fecha1}} a {{$fecha2}};

            @elseif ($fecha1 == null && $fecha2 == null)

                Ninguna

            @elseif ($fecha1 !== null && $fecha2 == null)

                {{$fecha1}} en adelante

            @else 

                {{$fecha2}} hacia atrás

            @endif


        </h6>
    </div>
    




    <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12">
        

        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> 


            <TABLE style= "width: 90%" align = "center"> 

                <tr class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> 
                    <th class="text-center" style="background-color: lightblue">Profesional</th>
                    <th class="text-center" style="background-color: lightblue">Cantidad Expedientes</th>
                </tr> 

                @forelse ($profesionalesData as $profesionalData)

                    <tr class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> 

                        <td class="text-center">{{$profesionalData->nombreProfesional}}</td>
                        <td class="text-center">{{$profesionalData->totalExpedientes}}</td>

                    </tr> 


                @empty

                    <h3><strong>No existen casos de expedientes con trámites inconclusos aún</strong></h3>

                @endforelse

            </TABLE>
            

        </div> {{-- col --}}

    </div> {{-- row --}} 



</body>




</html>