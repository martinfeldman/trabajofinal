



    @php
    /** 
     * 
     *  variables de entrada
     * 
     *  content: contenido a desplegar en el comboBox
     *  size: tamaño del select
     * 
     * 
     * 
     */




    if (!isset($size)){

        /* dd($size); */
        $size = 12;
    }  






    @endphp





        <!-- ComboBox de Motivos por los que se desaprueba o considera incompleto un expediente -->
        
        <select class="combo col-lg-{{$size}} col-md-{{$size}} col-sm-{{$size}} col-xs-{{$size}}" name="motivo" id="motivoComboBox" >
            <option value="">{{ isset($select) ? $select : 'Seleccione motivo' }}</option>
            
                @if ($page === "formularioExpediente")

                    <option value="{{config('app.motivo_error_en_archivo_entregado')}}">{{config('app.motivo_error_en_archivo_entregado')}}</option>
                    <option value="{{config('app.motivo_error_en_datos_formulario_entregado')}}">{{config('app.motivo_error_en_datos_formulario_entregado')}}</option>
                    
                @elseif ($page === "notaReemplazoProfesional") 

                    <option value="{{config('app.motivo_error_en_archivo_entregado')}}">{{config('app.motivo_error_en_archivo_entregado')}}</option>

                @endif
                    
            
            
                
        
        </select>   