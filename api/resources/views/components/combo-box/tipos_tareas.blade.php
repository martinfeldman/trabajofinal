



    @php
        /** 
         * 
         *  variables de entrada
         * 
         *  content: contenido a desplegar en el comboBox
         *  size: tamaño del select
         * 
         * 
         * 
         */
    



    if (!isset($size)){

        /* dd($size); */
        $size = 12;
    }  

    
    if (!isset($value)){
    
        // dd($size);
        $value = null;
    } else {

        $tipo_tarea_id = App\Models\Tarea::findOrFail($value)->tipo_tarea_id;

        // dd($tipo_tarea_id ,$value, $content);



    }




    @endphp






            <!-- ComboBox de Tareas -->
            
            <select onchange="toggle()" class="combo col-lg-{{$size}} col-md-{{$size}} col-sm-{{$size}} col-xs-{{$size}}" name="tipo_tarea_id" id="tareas" >
                
                
                @if (!isset($value)) 
                
                    <option value="{{-- {{ old('objeto_id') }} --}}" selected disabled>{{ isset($select) ? $select : 'Seleccione tarea' }}</option>
                
                @else
                
                    <option value="{{ $tipo_tarea_id }}">{{ $content[$tipo_tarea_id-1]->tipo_tarea }}</option>
                
                @endif
                
            
                @if (isset($content)) 
            
                    @foreach ($content as $tarea)
                        <option value="{{$tarea->tipo_tarea_id}}">{{$tarea->tipo_tarea}}</option>
                    @endforeach
                
                @endif
            
            
            </select>   