





   @php
   
   /** 
    * 
    *  variables de entrada
    * 
    *  content: contenido a desplegar en el comboBox
    *  size: tamaño del select
    * 
    * 
    * 
    */


   if (!isset($size)){

      /* dd($size); */
      $size = 12;
   }  


   if (!isset($value)){

   // dd($size);
      $value = null;
   }  






   @endphp




         <!-- ComboBox de Profesionales -->

         <select class="combo col-lg-{{$size}} col-md-{{$size}} col-sm-{{$size}} col-xs-{{$size}}" name="profesional_id" id="profesionales" >
         
            {{-- {{dd($select);}} --}}
            
            @if (!isset($value)) 

               <option value="{{-- {{ old('objeto_id') }} --}}" selected disabled>{{ isset($select) ? $select : 'Seleccione profesional' }}</option>
            
            @else

               <option value="{{ $value }}">{{ $content[$value-1]->profesional_nombres }} {{ $content[$value-1]->profesional_apellidos }} ({{ $content[$value-1]->profesional_numero_matricula }})</option>

            @endif
      
            
            @if (isset($content)) 
      
               @foreach ($content as $profesional)

                  <option value="{{$profesional->id}}"> {{$profesional->profesional_nombres }}
                        {{ $profesional->profesional_apellidos }} ({{$profesional->profesional_numero_matricula}})
                  </option>

               @endforeach 
      
            @endif
         
         
         </select>   