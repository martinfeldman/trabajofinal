


    <div class="container col-lg-12 col-md-12 col-sm-12 col-xs-12">                         {{-- container --}}
        {{-- {{dd($user)}} --}}
        
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="font-size:120%">        {{-- roles --}}
            
            <span>1. Elija los roles</span> <br> <br>

            <label class="form-check-label col-lg-2" for="administrativoCheckbox">
                Administrativo

                <input class="form-check-input" type="checkbox" data-group="roleChecboks"
                name="administrativoCheckbox" id="administrativoCheckbox" 
                    wire:click.prevent="administrativoCheckBoxHasBeenChecked" onclick="toggle()"
                {{$this->administrativoCheckBoxIsChecked  ? "checked" : ""}}
                {{$this->administrativoHibridoCheckBoxIsChecked || $this->supervisorCheckBoxIsChecked ? "disabled" : ""}}>

                {{-- {{$this->administrativoCheckBoxIsChecked}} --}}

            </label>
        


            <label class="form-check-label col-lg-2" for="supervisorCheckbox">
                Supervisor

                <input class="form-check-input" type="checkbox" data-group="roleChecboks"
                name="supervisorCheckbox" id="supervisorCheckbox"
                    wire:click.prevent="supervisorCheckBoxHasBeenChecked" onclick="toggle()"
                {{$this->supervisorCheckBoxIsChecked ? 'checked="checked"' : ''}}
                {{$this->administrativoCheckBoxIsChecked || $this->administrativoHibridoCheckBoxIsChecked ? "disabled" : ""}}>

                {{-- {{$this->supervisorCheckBoxIsChecked}} --}}

            </label>




            <label class="form-check-label col-lg-3" for="administrativoHibridoCheckbox">
                Administrativo Híbrido

                <input class="form-check-input" type="checkbox" data-group="roleChecboks"
                name="administrativoHibridoCheckbox" id="administrativoHibridoCheckbox"
                    wire:click.prevent="administrativoHibridoCheckBoxHasBeenChecked" onclick="toggle()"
                {{$this->administrativoHibridoCheckBoxIsChecked ? 'checked="checked"' : ''}}
                {{$this->administrativoCheckBoxIsChecked || $this->supervisorCheckBoxIsChecked ? "disabled" : ""}}>

                {{-- {{$this->administrativoHibrido2heckBoxIsChecked}} --}}

            </label>


        

            <label class="form-check-label col-lg-2" for="profesionalCheckbox" >
                Profesional

                <input class="form-check-input" type="checkbox" data-group="roleChecboks" 
                name="profesionalCheckbox" id="profesionalCheckbox" 
                    wire:click.prevent="profesionalCheckBoxHasBeenChecked" onclick="toggle()"
                {{$this->profesionalCheckBoxIsChecked ? 'checked="checked"' : '' }}>

                {{-- {{$this->profesionalCheckBoxIsChecked}} --}}

            </label>




            <label class="form-check-label col-lg-2" for="propietarioCheckbox">
                Propietario

                <input class="form-check-input" type="checkbox" data-group="roleChecboks" 
                name="propietarioCheckbox" id="propietarioCheckbox" 
                    wire:click.prevent="propietarioCheckBoxHasBeenChecked" onclick="toggle()"
                {{$this->propietarioCheckBoxIsChecked ? 'checked="checked"' : '' }}>

                {{-- {{$this->propietarioCheckBoxIsChecked}} --}}

            </label>


            <label class="form-check-label col-lg-1" for=""></label>

            <br><br>

        </div>          {{-- roles --}}


        {{-- <br><br><br><br><br><br><br><br><br> --}}



        <br><br>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="font-size:120%">   {{-- permisos --}}

            <span>2. Ahora los permisos:</span> <br> <br>


            @if($this->profesionalCheckBoxIsChecked)

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                    <label>
                        <strong><u>
                            Permisos de profesional{{--  (irrevocables actualmente) --}}
                        </u></strong>
                    </label>
                
                    <br><br> 
                
                    @foreach($this->permisosProfesional as $permisoProf)

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        
                            <label class="form-check-label col-lg-11" for="permiso-{{$permisoProf->name}}-Checkbox">
                                {{$permisoProf->name}}
                            </label>
                            <br> 

                        </div>    
                
                    @endforeach

                    <br><br> <br> <br> <br> <br> <br> <br> <br> <br> <br> <br> <br> <br> 
                    <br><br> {{-- <br>  --}}

                </div>  
                

                @if($this->userIsNotProfesional && !$this->user->numero_matricula)

                
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        DADO QUE ESTA PERSONA NO ESTÁ REGISTRADA COMO PROFESIONAL AÚN,  <br>
                        DEBERÁ INGRESAR EL NUMERO DE MATRICULA PARA CREAR EL REGISTRO   <br>

                        <label for="cuit">NUMERO DE MATRICULA</label> <label id="asterisco" for="">*</label>
                        <input type="text" name="numero_matricula" class="form-control col-lg-4 col-md-4" placeholder="Número de matrícula" value="{{ old('numero_matricula') }}">

                        <br><br><br>

                    </div> 
                    @endif



            @endif 


                    

            @if($this->propietarioCheckBoxIsChecked)
            
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                
                    <label>
                        <strong><u>
                            Permisos de propietario (ninguno actualmente)
                        </u></strong>
                    </label>
                
                    <br> <br>
                
                </div>    
                
            @endif

                


            @if($this->administrativoCheckBoxIsChecked)

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <label><strong><u>Permisos de empleado administrativo:</u></strong></label> <br><br>

                    @foreach($permisosAdministrativo as $permisoAdm)

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        
                            <label class="form-check-label col-lg-11" for="permiso-{{$permisoAdm->name}}-Checkbox">
                                {{$permisoAdm->name}}
                            </label>

                            {{-- <input class="form-check-input" type="checkbox" data-group="decision" 
                            name="permiso-{{$permisoAdm->name}}-Checkbox" id="permiso-{{$permisoAdm->name}}-Checkbox"
                            {{$user->getAllPermissions()->contains($permisoAdm) ? 'checked="checked"' : "" }}> --}}
                            {{-- este no usaba {{$this->todosPermisosAdministradorCheckBoxIsChecked ? 'checked="checked"' : 'checked="unchecked"' }} --}}
                        
                            <br> 
                        
                        </div>    
                    
                    @endforeach

                    <br><br> <br> <br> <br> <br> <br> <br> <br> <br> <br> <br> <br> <br> 
                    <br><br> <br> 
                </div>
            @endif





            @if($this->supervisorCheckBoxIsChecked)

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <br>

                    <label><strong><u>Permisos exclusivos de supervisor (posee todos los de empleado administrativo):</u></strong></label> <br><br>

                    @foreach($permisosUnicosSupervisor as $permiso)

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        
                            <label class="form-check-label col-lg-6" for="permiso-{{$permiso->name}}-Checkbox">
                                {{$permiso->name}}
                            </label>
                        
                            {{-- <input class="form-check-input" type="checkbox" data-group="decision" 
                            name="permiso-{{$permiso->name}}-Checkbox" id="permiso-{{$permiso->name}}-Checkbox" 
                            {{$user->getAllPermissions()->contains($permiso) ? 'checked="checked"' : "" }}> --}}
                            {{-- este no usaba {{$this->todosPermisosSupervisorCheckBoxIsChecked ? 'checked="checked"' : 'checked="unchecked"' }} --}}
                        
                        </div>    
                    
                    @endforeach

                    <br><br><br><br>

                </div>
            @endif







            @if($this->administrativoHibridoCheckBoxIsChecked)

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                    <label><strong><u>Permisos que puede tomar un empleado Administrativo Híbrido (de ambos roles administrativos):</u></strong></label> <br><br>



                    @foreach($permisosAdministrativo as $permisoAdm)

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        
                            <label class="form-check-label col-lg-11" for="permiso-{{$permisoAdm->name}}-Checkbox">
                                {{$permisoAdm->name}}
                            </label>
                        

                            <input class="form-check-input" type="checkbox" data-group="decision" 
                            name="permiso-{{$permisoAdm->name}}-Checkbox" id="permiso-{{$permisoAdm->name}}-Checkbox"
                            {{$user->getAllPermissions()->contains($permisoAdm) ? 'checked="checked"' : "" }}>
                            {{-- este no usaba {{$this->todosPermisosAdministradorCheckBoxIsChecked ? 'checked="checked"' : 'checked="unchecked"' }} --}}

                        

                        
                        </div>    
                    
                    @endforeach


                    <br> <br> 


                    @foreach($permisosUnicosSupervisor as $permiso)

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        
                            <label class="form-check-label col-lg-11" for="permiso-{{$permiso->name}}-Checkbox">
                                {{$permiso->name}}
                            </label>
                        
                            <input class="form-check-input" type="checkbox" data-group="decision" 
                            name="permiso-{{$permiso->name}}-Checkbox" id="permiso-{{$permiso->name}}-Checkbox" 
                            {{$user->getAllPermissions()->contains($permiso) ? 'checked="checked"' : "" }}>
                            {{-- este no usaba {{$this->todosPermisosSupervisorCheckBoxIsChecked ? 'checked="checked"' : 'checked="unchecked"' }} --}}

                        
                        </div>    
                    
                    @endforeach
                    
                    <br><br> <br> <br> <br> <br> <br> <br> <br> <br> <br> <br> <br> <br> 
                    <br><br> <br> <br><br> <br> <br><br> <br> <br><br> 

                </div>

                
            @endif
















                
        </div>          {{-- permisos --}}

        <br>

        
        
        {{-- <script type="text/javascript" src="{{asset('js/usuarios/editarUsuarioTablaPermisos.js')}}"></script> --}}
    </div>              {{-- container --}}
    

