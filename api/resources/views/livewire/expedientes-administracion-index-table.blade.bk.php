
{{-- {{dd($expedientes);}} --}}


            <div class="table-responsive" wire:poll.4s>
                <table class="table table-striped table-bordered  table-hover">
                    <thead>

                        <th class="col-lg-1 col-md-1 col-sm-1 col-xs-1 bg-primary text-center">NÚMERO <br> EXPEDIENTE</th>
                        @can('administrativo.&.supervisor.columnaProfesional.expediente.index')
                            <th class="col-lg-2 col-md-2 col-sm-2 col-xs-2 bg-primary text-center">PROFESIONAL</th>
                        @endcan
                        <th class="col-lg-1 col-md-1 col-sm-1 col-xs-1 bg-primary text-center">PROPIETARIO</th>
                        <th class="col-lg-1 col-md-1 col-sm-1 col-xs-1 bg-primary text-center">FECHA INICIO</th>
                        <th class="col-lg-1 col-md-1 col-sm-1 col-xs-1 bg-primary text-center">TIPOLOGÍA</th>
                        <th class="col-lg-1 col-md-1 col-sm-1 col-xs-1 bg-primary text-center">TAREAS</th>
                        <th class="col-lg-1 col-md-1 col-sm-1 col-xs-1 bg-primary text-center">PARTIDA/S INMOBILIARIA/S</th>
                        <th class="col-lg-2 col-md-2 col-sm-2 col-xs-2 bg-primary text-center">OPCIONES</th>
                        <th class="col-lg-2 col-md-2 col-sm-2 col-xs-2 bg-primary text-center">CONDICIÓN</th>
                        

                    </thead>


                        @forelse ($expedientes as $expt)

                            @php
                                $currentUserRole === "profesional" ? $valor_prioridad = $expt->prioridad_profesional : $valor_prioridad = $expt->prioridad_administracion;
                            @endphp


                            <tr>
                                <td class="text-center" style="font-weight: bold; background-color:

                                    @switch($valor_prioridad)
                                        @case(1)

                                            #ff0000;
                                            @break

                                        @case(2)

                                            #ff6666;
                                            @break

                                        @case(3)

                                            #ffb3b3;
                                            @break 
                            
                                    @endswitch
                                ">
                                    {{$expt->expediente_numero}}
                                </td>


                                @can('administrativo.&.supervisor.columnaProfesional.expediente.index')
                                    <td>{{$expt->profesional_nombres}} {{$expt->profesional_apellidos}}</td>
                                @endcan


                                <td>{{$expt->propietario_nombres}} {{$expt->propietario_apellidos}}</td>


                                <td>{{\Carbon\Carbon::createFromTimestamp(strtotime($expt->fecha_inicio))->format('d-m-Y  H:i:s')}}</td>

                                <td>{{$expt->tipologia}}</td>


                                <td>{{$expt->tipo_tarea}}</td>

                                <td>{{$expt->partida_inmobiliaria}}</td>


                                {{-- Botones --}}
                                <td>
                                    <a href="{{route('expedientes.show', ['id' => $expt->expediente_id])}}">
                                        <button  class="btn btn-success"> <i class="fa fa-eye"></i></button>
                                    </a>

                                    @if ($expt->condicion_id == config('app.formulario_a_revisar'))
                                        <a href="{{route('expedientes.edit', ['id' => $expt->expediente_id])}}">
                                            <button class="btn btn-info"> <i class="fa fa-pencil"></i></button>
                                        </a>    
                                    @endif
                                    

                                    @if ($expt->condicion_id >= config('app.cerrado') && $expt->condicion_id <= config('app.cerrado_con_dir_obra_finalizado'))
                                        {!! Form::open(array('url'=>'expedientes','method'=>'post','autocomplete'=>'off', 'files'=>true, 'style'=>'display:inline')) !!}
                                        {{ Form::token() }}
                                        {{-- @method('GET') --}}
                                        {{csrf_field()}}

                                            {{-- <div class="form-group row col-lg-12 col-sm-12 col-md-12 col-xs-12"> --}}
                                            <form method="post" enctype="multipart/form-data" 
                                            action="{{route('expedientes.descargarCertificado', ['id' => $expt->expediente_id])}}" class="d-inline">
                                                @csrf
                                            
                                                <a href="{{route('expedientes.descargarCertificado', ['id' => $expt->expediente_id])}}" {{-- data-target="#modal-DescargarComprobantePago{{$expediente->expediente_id}}" data-toggle="modal" --}}>
                                                    <button  type="submit" class="btn btn-danger d-inline" formaction="{{url('/expedientes/descargarCertificado', ['id' => $expt->expediente_id])}}">
                                                        <i class="fa fa-file-pdf-o"></i>
                                                    </button>
                                                </a>

                                                {{-- <br><br> --}}
                                            
                                            </form>
                                            {{-- </div> <!-- .form-group row -->      --}}
                                        
                                        {!!Form::close()!!}   
                                    @endif


                                    

                                    {{-- <a href="" data-target="#modal-delete-{{$expt->expediente_id}}" data-toggle="modal"><button class="btn btn-danger">Eliminar</button></a> --}}
                                </td>  


                                <td>{{$expt->condicion_short /* . "  . prioridadprofesional : " . $expt->prioridad_profesional . " prioridadADMIN : " . $expt->prioridad_administracion */}}</td>

                            <tr>


                        @empty 
                        
                            <h3><strong>Sin expedientes actualmente o coincidencias con la búsqueda</strong></h3>

                        @endforelse


                            

                    </tr>
                    

                        {{-- @include('expediente.modal') --}}


                    
                </table>
            </div> {{-- table --}}