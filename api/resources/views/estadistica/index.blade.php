
@extends ('layouts.admin')




    {{-- {{dd($fecha2)}} --}}
    {{-- {{debug(json_encode($dataGraficoCantidadMetrosCuadradosConstruidos[1]))}} --}}
    {{-- {{dd($localidadSeleccionada)}} --}}












@section('contenido')

    <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <h2><strong>Estadísticas</strong></h2>
            <br>
        </div> 
    </div>






    <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12" name="filtros" style="font-size: 115%">

        {!! Form::open(array('url'=>'estadistica','method'=>'post','autocomplete'=>'off', 'style'=>'display:inline')) !!}
        {{ Form::token() }}
        {{csrf_field()}}

        <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1"></div>
        

        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">

            <label for="">Localidad</label>
            <select name="localidad" id="localidades" class="form-control">

                <option selected disabled>--Seleccione--</option>
                @foreach ($localidades as $localidad)
                    <option value="{{$localidad->localidad_id}}">{{$localidad->localidad}}</option>
                @endforeach

            </select>

        </div>



        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">

            <div class="form-group">
                <label>Desde</label>
                <input type="date" id="min" name="fecha1" value="" class="form-control">
            </div>

        </div>


        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
            <div class="form-group">
                <label>Hasta</label>
                <input type="date" id="max" name="fecha2" value="" class="form-control">
            </div>
        </div>



        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">

            <br>


            <a href="{{route('estadistica')}}">
                <button type="submit" class="btn btn-primary " id="filtrar" formaction="{{url('/estadistica')}}">
                    Filtrar 
                </button>
            </a>



            <button type="button" class="btn btn-secondary " id="limpiar">
                Limpiar 
            </button>

        </div>

        {!!Form::close()!!}     

        
        {{-- <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3"></div>  --}}

    </div> {{-- row - filtros --}}




    <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center" style="font-size: 115%"><br> <br>
    
        @if (isset($fecha1) && !isset($fecha2))
            <h4> 
                <strong>Datos filtrados desde el {{\Carbon\Carbon::createFromTimestamp(strtotime($fecha1))->format('d-m-Y')}} 
                a la actualidad. Localidad: {{isset($localidadSeleccionada) ? $localidadSeleccionada->localidad : ""}}</strong> 
            </h4>


        @elseif (!isset($fecha1) && isset($fecha2))
        <h4> 
            <strong>Datos filtrados desde el {{\Carbon\Carbon::createFromTimestamp(strtotime($fecha2))->format('d-m-Y')}} 
            hacia atrás. Localidad: {{isset($localidadSeleccionada) ? $localidadSeleccionada->localidad : ""}}</strong> 
        </h4>

        @elseif (isset($fecha1) && isset($fecha2))
        <h4> 
            <strong>Datos filtrados entre el {{\Carbon\Carbon::createFromTimestamp(strtotime($fecha1))->format('d-m-Y')}} 
            al {{\Carbon\Carbon::createFromTimestamp(strtotime($fecha2))->format('d-m-Y')}}. Localidad: {{isset($localidadSeleccionada) ? $localidadSeleccionada->localidad : ""}}</strong> 
        </h4>

        @elseif (!isset($fecha1) && !isset($fecha2) && isset($localidadSeleccionada))
            <h4>
                <strong>Datos filtrados a la localidad: {{$localidadSeleccionada->localidad}}</strong> 
            </h4>

        @endif

        <br>
    </div>








    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="https://code.highcharts.com/modules/export-data.js"></script>
    <script src="https://code.highcharts.com/modules/accessibility.js"></script>

    <script src="https://code.highcharts.com/modules/data.js"></script>
    <script src="https://code.highcharts.com/modules/drilldown.js"></script>




    <div class="row col-lg-6 col-md-6 col-sm-6 col-xs-6">

        <figure class="highcharts-figure">
            <div id="container"></div>
        
        </figure>


    </div>



    <div class="row col-lg-6 col-md-6 col-sm-6 col-xs-6">

        <figure class="highcharts-figure">
            <div id="container2"></div>
        
        </figure>


    </div>



    <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12"><br> <br> <br> <br> </div>





    <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12">

        <figure class="highcharts-figure">
            <div id="container3"></div>
        
        </figure>


    </div>






{{-- <script type="text/javascript" src="{{asset('js/estadistica/grafico1.js')}}" charset="utf-8"> </script> --}}
{{-- <script type="text/javascript" src="{{asset('js/estadistica/grafico2.js')}}"></> --}}
{{-- <script type="text/javascript" src="{{asset('js/estadistica/grafico3-columnDrilldown.js')}}"></script> --}}




<script type="text/javascript" >


    // GRAFICO 1

    Highcharts.chart('container', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'Tipo de tarea profesional más realizada',
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        accessibility: {
            point: {
            valueSuffix: '%'
            }
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %'
                }
            }
        },
        series: [{
            name: 'Brands',
            colorByPoint: true,
            data: <?= $dataGraficoTiposTareasMasRealizadas ?>,
        }]
    });


</script>





<script type="text/javascript">

    /* console.log(dataGraficoTipologiasMasRealizadas) */

    // GRAFICO 2

    Highcharts.chart('container2', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'Tipología más realizada'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        accessibility: {
            point: {
            valueSuffix: '%'
            }
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %'
                }
            }
        },
        series: [{
            name: 'Brands',
            colorByPoint: true,
            data: <?= $dataGraficoTipologiasMasRealizadas ?>,
        }]
    });



</script>






<!-- SELECT2 -->


@push('scripts')


<script>
    $('#localidades').select2();        
</script>



@endpush





<script type="text/javascript">


    // GRAFICO 3


    // Create the chart

    Highcharts.chart("container3", {
        chart: {
            type: "column"
        },
        title: {
            text: "Cantidad de m² construidos según tipología"
        },
        subtitle: {
            text:
                'Puede hacer click sobre las columnas para ver los 5 profesionales que más contribuyeron a la cifra.'
        },
        accessibility: {
            announceNewData: {
                enabled: true
            }
        },
        xAxis: {
            type: "category"
        },
        yAxis: {
            title: {
                text: "m² construidos"
            }
        },
        legend: {
            enabled: false
        },
        plotOptions: {
            series: {
                borderWidth: 0,
                dataLabels: {
                    enabled: true,
                    format: "{point.y:.1f} m²"
                }
            }
        },

        tooltip: {
            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
            pointFormat:
                '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
        },

        series: [
            {
                name: "Browsers",
                colorByPoint: true,
                data: <?= $dataGraficoCantidadMetrosCuadradosConstruidos[0] ?>,
            }
        ],
        drilldown: {
            series: [
                {
                    name: "Chrome",
                    id: "Chrome",
                    data: [
                        ["v65.0", 0.1],
                        ["v64.0", 1.3],
                        ["v63.0", 53.02],
                        ["v62.0", 1.4],
                        ["v61.0", 0.88],
                        ["v60.0", 0.56],
                        ["v59.0", 0.45],
                        ["v58.0", 0.49],
                        ["v57.0", 0.32],
                        ["v56.0", 0.29],
                        ["v55.0", 0.79],
                        ["v54.0", 0.18],
                        ["v51.0", 0.13],
                        ["v49.0", 2.16],
                        ["v48.0", 0.13],
                        ["v47.0", 0.11],
                        ["v43.0", 0.17],
                        ["v29.0", 0.26]
                    ]
                },
                {
                    name: "Firefox",
                    id: "Firefox",
                    data: [
                        ["v58.0", 1.02],
                        ["v57.0", 7.36],
                        ["v56.0", 0.35],
                        ["v55.0", 0.11],
                        ["v54.0", 0.1],
                        ["v52.0", 0.95],
                        ["v51.0", 0.15],
                        ["v50.0", 0.1],
                        ["v48.0", 0.31],
                        ["v47.0", 0.12]
                    ]
                },
                {
                    name: "Internet Explorer",
                    id: "Internet Explorer",
                    data: [
                        ["v11.0", 6.2],
                        ["v10.0", 0.29],
                        ["v9.0", 0.27],
                        ["v8.0", 0.47]
                    ]
                },
                {
                    name: "Safari",
                    id: "Safari",
                    data: [
                        ["v11.0", 3.39],
                        ["v10.1", 0.96],
                        ["v10.0", 0.36],
                        ["v9.1", 0.54],
                        ["v9.0", 0.13],
                        ["v5.1", 0.2]
                    ]
                },
                {
                    name: "Edge",
                    id: "Edge",
                    data: [
                        ["v16", 2.6],
                        ["v15", 0.92],
                        ["v14", 0.4],
                        ["v13", 0.1]
                    ]
                },
                {
                    name: "Opera",
                    id: "Opera",
                    data: [
                        ["v50.0", 0.96],
                        ["v49.0", 0.82],
                        ["v12.1", 0.14]
                    ]
                }
            ]
        }
    });


</script>









@endsection