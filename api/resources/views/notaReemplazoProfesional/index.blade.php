
@extends ('layouts.admin')





@section('contenido')

    
    {{--  {{ dd($currentUserRole);}} --}}

    {{-- @if ($currentUserRole === "supervisor")
    {{ dd($notasReemplazoProfesional);}}
    @endif --}}




    {{-- {{dd($notasReemplazoProfesional);}} --}}
    {{-- {{dd($datosExtraNRP[0]['profesional_entrante_NombreCompleto']);}} --}}


    <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12" >


        <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12" >

            <h2 class="col-lg-6 col-md-6 col-sm-6 col-xs-6"><strong>Notas de Reemplazo Profesional</strong>
                @can('Completar Formulario de Nota de Reemplazo Profesional')
            </h2>  

            <br>
            <a  href="" data-target="#modal-Create" data-toggle="modal" class="btn btn-success col-lg-2 col-md-2 col-sm-2 col-xs-2" style="font-weight: bold; font-size: 130%; color: black">
                NUEVO
            </a> 
    
            @include('notaReemplazoProfesional.modalCreate')  

            @endcan

        </div>

            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" >
                {{--  @include('notaReemplazoProfesional.search') --}}
        
                {{-- <x-inputs.search searchText={{$searchText}} url="reemplazo-profesional" /> --}}
            </div>


        
    </div>



    <div class="row" style="min-height: 75vh;">


        <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
            
            @if (session('error'))
                <div class="alert alert-danger" role="alert">
                    {{session('error')}}
                </div>
            @endif

            @if (count($errors)>0)    
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li> {{$error}} </li>
                        @endforeach
                    
                    </ul>
                </div>
            @endif   
        </div>




        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="font-size: 115%"> {{-- filtros --}}

            {!! Form::open(array('url'=>'reemplazo_profesional/search','method'=>'POST','autocomplete'=>'off', 'style'=>'display:inline')) !!}
            {{ Form::token() }}
            {{csrf_field()}}      <br>



            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">

                <label for="profesional">Profesional</label>
                <select name="profesional" id="profesionales" class="form-control">

                    <option value="" selected disabled>--Seleccione--</option>
                    @foreach ($profesionales as $profesional)
                        <option value="{{$profesional->id}}">{{$profesional->profesional_nombres}} {{$profesional->profesional_apellidos}} ({{$profesional->profesional_cuit}})</option>
                    @endforeach

                </select>


            </div>



            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">


                <br>
                <div class="form-group">
                        {{-- {{ Form::submit('Filtrar', ["class" => "btn btn-primary btn-block"])}} --}}
                        <button type="submit" class="btn btn-primary btn-block">
                            Filtrar
                        </button>
                </div>

            </div>


            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                <br>
                <button type="reset" class="btn btn-secondary btn-block" id="limpiar">
                    Limpiar 
                </button>


            </div>

            {!!Form::close()!!}

        </div>      {{-- col filtros --}}






        <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10"> <br> <br>

            <div class="table-responsive">

                <table class="table table-striped table-bordered table-hover" style="font-size: 120%">

                    <thead>
                        {{-- <th>Id</th> --}}
                        <th class="bg-primary text-left">PROFESIONAL SALIENTE</th>
                        @can('Ver columna de Profesional Entrante en indice de Reemplazo Profesional')
                        <th class="bg-primary text-left">PROFESIONAL ENTRANTE</th>
                        @endcan
                        <th class="bg-primary text-right">Nº EXPEDIENTE AFECTADO</th>
                        {{-- <th>Decisión</th> --}}
                        <th class="bg-primary text-left">CONDICION</th>
                        {{-- <th>Alta</th> --}}
                        <th class="bg-primary text-left">OPCIONES</th>

                    </thead>

                    @php
                        $i = 0;
                        
                    @endphp

                    @foreach ($notasReemplazoProfesional as $notaRP)
                        <tr>

                            <td>{{$datosExtraNRP[$i]['profesional_saliente_NombreCompleto']}}</td>  
                                            <!-- profesional saliente -->
                            @can('Ver columna de Profesional Entrante en indice de Reemplazo Profesional')
                                <td>{{$datosExtraNRP[$i]['profesional_entrante_NombreCompleto']}}</td>              <!-- profesional entrante -->
                            @endcan
                            
                            <td class="text-right">{{$datosExtraNRP[$i]['expediente_afectado_numero']}}</td>    
                            <td>{{$datosExtraNRP[$i]['condicion_nrp']}}</td>   

                            <td>        {{-- Opciones --}}

                                <a href="{{route('reemplazo-profesional.show', ['id' => $notaRP->nota_reemplazo_profesional_id])}}">
                                    <button class="btn btn-success">
                                        <i class="fa fa-eye"></i>
                                    </button>
                                </a>


                                @can('Subir archivo de Nota de Reemplazo Profesional firmada')
                                    @if ($notaRP->condicion_nrp_id == config('app.nrp_faltan_firmas'))
                                        <a href="" data-target="#modal-SubirArchivoNotaFirmada-{{$notaRP->nota_reemplazo_profesional_id}}" data-toggle="modal">
                                            <button class="btn btn-info">Añadir Archivo Firmado</button>
                                        </a>
                                    @endif

                                    @include('notaReemplazoProfesional.modal-SubirArchivoNotaFirmada')
                                @endcan




                                @can('Valorar Solicitud Reemplazo Profesional')
                                    @if ($notaRP->condicion_nrp_id == config('app.nrp_para_revision'))
                                        <a href="" data-target="#modal-VerPruebasAvance-{{$notaRP->nota_reemplazo_profesional_id}}" data-toggle="modal">
                                            <button class="btn btn-info">Ver Pruebas de Avance</button>
                                        </a>
                                    @endif

                                    @include('notaReemplazoProfesional.modal-VerPruebasAvance')
                            
                                @endcan
                                




                                @can('Valorar Solicitud Reemplazo Profesional')
                                    @if ($notaRP->condicion_nrp_id == config('app.nrp_para_revision'))
                                        <a href="" data-target="#modal-ValorarNotaReemplazoProfesional-{{$notaRP->nota_reemplazo_profesional_id}}" data-toggle="modal">
                                            <button class="btn btn-info">Valorar</button>
                                        </a>
                                    @endif
                                
                                @endcan
                                
                                
                                {{-- <a href="" data-target="#modal-delete-{{$prop->propietario_id}}" data-toggle="modal"><button class="btn btn-danger">Eliminar</button></a> --}}
                            </td>    
                        </tr>

                        @php
                            $i++;
                            
                        @endphp
                    
                        @include('notaReemplazoProfesional.modal-ValorarNotaReemplazoProfesional')
                        {{--   @include('propietario.modalDestroy')  --}}


                    @endforeach
                </table>

            </div> {{-- table-responsive --}}

            

        </div> {{-- col --}}

    </div> {{-- row --}}



    {{-- @if($notasReemplazoProfesional !== null) --}}

    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" > <!-- para paginar -->
                
        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
    
            <h3> {{$notasReemplazoProfesional->render()}} </h3>

        </div>


        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 text-center">

            @if ($notasReemplazoProfesional->total() < $notasReemplazoProfesional->PerPage())

                <h3> Mostrando {{$notasReemplazoProfesional->total()}} de {{$notasReemplazoProfesional->total()}} registros </h3>

            @else 

                <h3> Mostrando {{$notasReemplazoProfesional->perPage()}} de {{$notasReemplazoProfesional->total()}} registros </h3>

            @endif

        </div>

    </div>

    {{-- @endif --}}

    
    
@endSection










<!-- SELECT2 -->


@push('scripts')



<script>
    $('#profesionales').select2();         
</script>




@endpush


