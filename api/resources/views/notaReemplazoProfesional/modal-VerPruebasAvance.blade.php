

    <div class="modal fade modal-slide-in-right" aria-hidden="true"
    role="dialog" tabindex="-1" id="modal-VerPruebasAvance-{{$notaRP->nota_reemplazo_profesional_id}}">
    
    {{-- {{dd($notaRP);}} --}}
    


        {{-- {{dd($profesionales);}} --}}



        @if (count($errors)>0)    
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li> {{$error}} </li>
                    @endforeach
                
                </ul>
            </div>
        @endif    
            
            

        <div class="modal-dialog" style="width:80%; heigth: 70%;">
            <div class="modal-content">
    
    
                <div class="modal-header"> 
                    <h2 class="modal-title text-center">Pruebas Avance Nota Reemplazo Profesional {{$notaRP->nota_reemplazo_profesional_id}}
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true"> x </span>    
                        </button>
                    </h2>   
                </div>
                    
                <div class="modal-body col-lg-12 col-sm-12 col-md-12 col-xs-12">
                    <div class="container col-lg-12 col-sm-12 col-md-12 col-xs-12">


                        {{-- <div id="carouselExampleFade" class="carousel slide carousel-fade" data-bs-ride="carousel">

                            <div class="carousel-inner"> --}}

                                {{-- <div class="carousel-item active"> --}}

                        {!! Form::open(array('url'=>'reemplazo-profesional','method'=>'POST','autocomplete'=>'off', 'files'=>true)) !!}
                        {{ Form::token() }}
                            
                            <form method="POST" enctype="multipart/form-data" action="{{route('reemplazo-profesional.descargarPruebaAvance', [
                                'id' => $notaRP->nota_reemplazo_profesional_id,
                                'prueba_avance' => "1"])}}">


                                <div class="form-group" >

                                    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10"></div>

                                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 float-right">

                                        <button class="btn btn-success btn-block" type="submit" formaction="{{url('reemplazo-profesional/descargarPruebaAvance', [
                                            'id' => $notaRP->nota_reemplazo_profesional_id,
                                            'prueba_avance' => "1"])}}">
                                            <span aria-hidden="true">Descargar</span>  
                                        
                                        </button> 

                                    </div>
                                    
                                    <img src="/storage/PruebasAvance/{{$notaRP->prueba_avance_1}}" class="d-block w-100" style="width: 100%; height:100%" alt="...">
                                </div>

                                {{-- <div class="carousel-item"> --}}


                                    <div class="form-group">


                                        <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10"></div>

                                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 float-right">


                                            <button class="btn btn-success btn-block" type="submit" formaction="{{url('reemplazo-profesional/descargarPruebaAvance', [
                                                'id' => $notaRP->nota_reemplazo_profesional_id,
                                                'prueba_avance' => "2"])}}">
                                                <span aria-hidden="true">Descargar</span>  
                                            </button> 
                                            
                                        </div>

                                        <img src="/storage/PruebasAvance/{{$notaRP->prueba_avance_2}}" class="d-block w-100" style="width: 100%; height: 100%;" alt="...">
                                    </div>
                                
                            {{-- </div> --}}

                            {{-- CAROUSEL NO FUNCIONA YA QUE NO COINCIDE CON LA VERSION DE BOOTSTRAP IMPLEMENTADA EN EL PROYECTO
                            <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>


                            <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a> --}}

                        {{-- </div> --}}


                        </form>
                    {!!Form::close()!!}

                    </div> <!--.container -->

                </div> <!--.modal-body -->


                <div class="modal-footer">
                
                </div>  <!--.modal-footer -->

            </div>    <!--  .modal-content -->
        </div>   <!--  .modal-dialog -->
            
    

    </div> <!--.modal -->



