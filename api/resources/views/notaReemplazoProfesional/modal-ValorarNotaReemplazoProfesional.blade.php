


    <div class="modal fade modal-slide-in-right" aria-hidden="true"
     role="dialog" tabindex="-1" id="modal-ValorarNotaReemplazoProfesional-{{$notaRP->nota_reemplazo_profesional_id}}">
    
    
 

        



            @if (count($errors)>0)    
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li> {{$error}} </li>
                        @endforeach
                    
                    </ul>
                </div>
            @endif    
            
            

            
            


        
            <div class="modal-dialog">

                <div class="modal-content">
    
    
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true"> X </span>    
                        </button>
                        <h3 class="modal-title text-center">Solicitud de Reemplazo Profesional en Dirección de Obra correspondiente a Expediente con número {{ $notaRP->expediente_afectado_numero}} </h3>
                        
                    </div>  {{-- modal-header --}}


                    <div class="modal-body">

                        {!! Form::open(array('url'=>'reemplazo-profesional','method'=>'POST','autocomplete'=>'off',)) !!}
                        {{ Form::token() }}
                    

    
                        {{-- <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> checkbox

                            <input class="form-check-input text " type="checkbox" name="PlanosODocumentosCheckbox" id="PlanosODocumentosCheckbox"
                                onchange="toggleCheckboxs()">

                            <label class="form-check-label" for="PlanosODocumentosCheckbox"  
                                name="PlanosODocumentosLabel" id="PlanosODocumentosLabel" title="Falta corregir algo del formulario">
                                Planos o documentos entregados pendientes de cambio(s)
                            </label>
                                
                        </div> 
                        
                        <br> --}}





                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> {{-- checkbox --}}
                            <input class="form-check-input" type="checkbox" data-group="decision" onchange="toggleCheckboxs()"
                                name="aprobadoCheckbox" id="aprobadoCheckbox"   title="Si hay encontrado todo correcto">
                            <label class="form-check-label" for="aprobadoCheckbox">
                                Aprobar Solicitud
                            </label>
                        </div>
                            
                            
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> {{-- checkbox --}}
                                
                            <input class="form-check-input" type="checkbox" data-group="decision" 
                                name="desaprobadoCheckbox" id="desaprobadoCheckbox" onchange="toggleCheckboxs()">
                            <label class="form-check-label" for="desaprobadoCheckbox" title="Por favor especifique el motivo por el que desaprueba el formulario">
                                Desaprobar Solicitud
                            </label>
                        </div>

                        

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> <!-- ComboBox de Motivos -->
                            <br>
                            <label for="motivo">Indique motivo(s) u observacion(es)</label> <br>
                            
                            <x-combo-box.motivos  size=12 page="notaReemplazoProfesional"/>
    
                            <input type="text" name="otroMotivo" class="form-control" placeholder="Otro: " id="motivoInput"> 
                                
        
                        </div> 


                    </div>  {{-- modal-body --}}

                    
                    <br><br><br> <br><br><br> <br><br>


                    <div class="modal-footer">  <!-- Botones --> 
                        
                        <button type="submit" class="btn btn-success col-lg-6 col-md-6 col-sm-6 col-xs-6"
                            formaction="{{route('reemplazo-profesional.valorarNotaReemplazoProfesional', ['id' => $notaRP->nota_reemplazo_profesional_id])}}">
                            Confirmar
                        </button>

                        <button type="button" class="btn btn-danger col-lg-5 col-md-5 col-sm-5 col-xs-5" data-dismiss="modal">Cancelar</button>
            
                    </div> {{-- modal-footer --}}
        

    

                    {!!Form::close()!!}

                </div> {{-- modal-content --}}

            </div> {{-- modal-dialog --}}
    



            <script type="text/javascript" src="{{asset('js/notaReemplazoProfesional/valorarNotaReemplazoProfesional.js')}}"></script>

    </div>  {{-- modal --}}
