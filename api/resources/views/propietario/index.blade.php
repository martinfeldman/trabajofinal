
@extends ('layouts.admin')




    {{-- {{dd($propietarios);}}  --}}

{{--     
    
        VISTA DE PROPIETARIOS INDEX


        -> VARIABLES DE ENTRADA: 

    $propietarios, con todos los propietarios con alta = 1 
    y filtrados según el rol del usuario actual


    Dado que es necesario que 
    -> cada profesional vea únicamente SUS expedientes;
    -> administrativos / supervisores ven TODOS los expedientes;

    
    
--}}





@section('contenido')


    @php
        use App\Models\Propietario;
    @endphp



    <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12">

        @if (count($errors)>0)    
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li> {{$error}} </li>
                    @endforeach
                
                </ul>
            </div>
        @endif   

        <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12">

            <h2 class="col-lg-4 col-md-4 col-sm-4 col-xs-4"><strong>Propietarios</strong></h2> 

            <br>

            @can('Ver boton Nuevo Propietario en indice de Propietarios')
                <a href="" data-target="#modal-Create" data-toggle="modal"  class="btn btn-success col-lg-2 col-md-2 col-sm-2 col-xs-2" style="font-weight: bold; font-size: 130%; color: black" title="Registrar Nuevo Propietario">
                    NUEVO
                </a> 

                @include('propietario.modal-Create')
            @endcan


            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1"></div>


            @if(!Auth()->user()->hasRole('propietario'))


                @can('Ver boton Registrarme como Propietario en indice de Propietarios',)
                    {!! Form::open(array('url'=>'propietarios/registrarse_como_propietario','method'=>'post','autocomplete'=>'off', 'style'=>'display:inline')) !!}
                    {{ Form::token() }}
                    {{csrf_field()}}
                        <a href="{{route('propietarios.registrarseComoPropietario')}}" title="Registrarse como Propietario" class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                            <button type="submit" class="btn btn-success" formaction="{{url('/propietarios/registrarse_como_propietario')}}" style="font-weight: bold; font-size: 130%; color: black" >
                                REGISTRARME COMO PROPIETARIO
                            </button>
                        </a> 
                    {!!Form::close()!!}
                @endcan

            @endif




        </div> {{-- row --}}
        
    </div> {{-- row --}}



    <div class="row col-lg-10 col-md-10 col-sm-10 col-xs-10">

        <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
            
            @if (session('error'))
                <div class="alert alert-danger" role="alert">
                    {{session('error')}}
                </div>
            @endif

        </div> {{-- col --}}


        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"  style="font-size: 115%"> {{-- filtros --}}

            {!! Form::open(array('url'=>'propietarios/search','method'=>'post','autocomplete'=>'off', 'style'=>'display:inline')) !!}
            {{ Form::token() }}
            {{csrf_field()}}      <br>



            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">

                <label for="">Propietario</label>
                <select name="propietario" id="propietarios" class="form-control">

                    <option value="" selected disabled>--Seleccione--</option>
                    @foreach ($listaPropietarios as $propietario)
                        <option value="{{$propietario->id}}">{{$propietario->propietario_nombres}} {{$propietario->propietario_apellidos}} ({{$propietario->propietario_cuit}})</option>
                    @endforeach

                </select>


            </div>



            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">


                <br>
                <div class="form-group">
                        {{-- {{ Form::submit('Filtrar', ["class" => "btn btn-primary btn-block"])}} --}}
                        <button type="submit" class="btn btn-primary btn-block">Filtrar</button>
                </div>

            </div>


            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                <br>
                <button type="reset" class="btn btn-secondary btn-block" id="limpiar">
                    Limpiar 
                </button>


            </div>

            {!!Form::close()!!}

        </div>



        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="min-height: 60vh;"> <br>


            <div class="table-responsive">

                <table class="table table-striped table-bordered table-hover" style="font-size: 120%">

                    <thead>
                        {{-- <th>Id</th> --}}
                        <th class="bg-primary text-left">NOMBRES</th>
                        <th class="bg-primary text-left">APELLIDOS</th>
                        <th class="bg-primary text-right">CUIT</th>
                        <th class="bg-primary text-left">EMAIL</th>
                        
                        @if($currentUserRoles->contains('supervisor') or $currentUserRoles->contains('administrativo'))
                            <th class="bg-primary text-left">ALTA</th>
                            <th class="bg-primary text-left">PROFESIONALES</th>
                        @endif

                        <th class="bg-primary text-left">OPCIONES</th>
                    </thead>

                    @forelse ($propietarios as $prop)

                    {{-- {{dd($prop);}} --}}

                        <tr>
                            {{--  <td>{{$prop->propietario_id}}</td> --}}
                            <td>{{$prop->propietario_nombres}}</td>
                            <td>{{$prop->propietario_apellidos}}</td>
                            <td class="text-right">{{$prop->propietario_cuit}}</td>
                            <td>{{isset($prop->propietario_email) ? $prop->propietario_email : "No posee" }}</td>

                            @if($currentUserRoles->contains('supervisor') or $currentUserRoles->contains('administrativo'))
                                <td>
                                    @if ($prop->alta == 1)
                                        Alta
                                    @else
                                        Baja
                                    @endif
                                </td>
                                
                                <td>
                                    @foreach ((Propietario::findOrFail($prop->id))->profesionales as $profesional)
                                        {{$profesional->getFullName()}} <br>
                                    @endforeach

                                </td>

                            @endif


                            {{-- <td>{{$prop->alta}}</td> --}}
                            <td>

                                {{-- <a href="{{route('propietario.edit', ['id' => $prop->propietario_id])}}"><button class="btn btn-info">Editar</button></a> --}}
                                <a href="" data-target="#modal-Edit-{{$prop->id}}" data-toggle="modal">
                                    <button class="btn btn-info"> 
                                        <i class="fa fa-pencil"></i>
                                    </button>
                                </a>

                                <a href="" data-target="#modal-delete-{{$prop->id}}" data-toggle="modal">
                                    <button class="btn btn-danger">
                                        <i class="fa fa-trash-o"></i>
                                    </button>
                                </a>

                            </td>   
                        
                            @include('propietario.modalEdit')
                            @include('propietario.modalDestroy')

                        </tr>
                    

                    @empty

                        @switch(true)

                            @case ($currentUserRoles->contains('administrativo'))
                            @case ($currentUserRoles->contains('administrativoHibrido'))
                            @case ($currentUserRoles->contains('supervisor'))

                                <h3><strong>No se han registrado propietarios aún</strong></h3>
                                @break

                            @case ($currentUserRoles->contains('profesional'))
                                
                                <h3><strong>No ha registrado propietarios aún</strong></h3>
                                @break
                                
                        @endswitch
                            
                    @endforelse

                </table>

            </div> {{-- table-responsive --}}

        </div> {{-- col --}}

    </div> {{-- row --}}



    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> <!-- para paginar -->
                
        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
    
            {{-- <h3> {{$propietarios->render()}} </h3> --}}

            @php
                $link_limit = 10;
            @endphp

            @if ($propietarios->lastPage() > 1)
                <ul class="pagination">
                    <li class="{{ ($propietarios->currentPage() == 1) ? ' disabled' : '' }}">
                        <a href="{{ $propietarios->url(1) }}">Primero</a>
                    </li>
                    @for ($i = 1; $i <= $propietarios->lastPage(); $i++)
                        <?php
                        $half_total_links = floor($link_limit / 2);
                        $from = $propietarios->currentPage() - $half_total_links;
                        $to = $propietarios->currentPage() + $half_total_links;
                        if ($propietarios->currentPage() < $half_total_links) {
                            $to += $half_total_links - $propietarios->currentPage();
                        }
                        if ($propietarios->lastPage() - $propietarios->currentPage() < $half_total_links) {
                            $from -= $half_total_links - ($propietarios->lastPage() - $propietarios->currentPage()) - 1;
                        }
                        ?>
                        @if ($from < $i && $i < $to)
                            <li class="{{ ($propietarios->currentPage() == $i) ? ' active' : '' }}">
                                <a href="{{ $propietarios->url($i) }}">{{ $i }}</a>
                            </li>
                        @endif
                    @endfor
                    <li class="{{ ($propietarios->currentPage() == $propietarios->lastPage()) ? ' disabled' : '' }}">
                        <a href="{{ $propietarios->url($propietarios->lastPage()) }}">Último</a>
                    </li>
                </ul>
            @endif

        </div>


        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 text-center">

            @if ($propietarios->total() < $propietarios->PerPage())
                <h3> Mostrando {{$propietarios->total()}} de {{$propietarios->total()}} registros </h3>

            @else 

                <h3> Mostrando {{$propietarios->perPage()}} de {{$propietarios->total()}} registros </h3>

            @endif

            

        </div>

    </div>


@endSection




<!-- SELECT2 -->


@push('scripts')












<script>
    $('#propietarios').select2();        
</script>






@endpush


