
@extends ('layouts.admin')


    {{-- {{dd($user);}}  --}}

{{--     
    
    /**
    /*
    /*
        VISTA DE USUARIOS INDEX

    Todas las rutas de /USUARIOS pueden ser accesidas únicamente por supervisores
    

    -- Agregar más detalles sobre lo que interesa de usuarios, 
        qué tipo de acciones se puede realizar en index




        -> VARIABLES DE ENTRADA: 

    $usuarios, con todos los usuarios con alta = 1 
    */
    
    --}}


    



@section('contenido')

    <div class="container col-lg-12 col-md-12 col-sm-12 col-xs-12">

        <div class="col-lg-6">
                <h2><strong>Usuarios</strong></h2>  
        </div>

        <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12" style="min-height: 75vh;"> 

            <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12" name="filtros" style="font-size: 115%">

                {!! Form::open(array('url'=>'usuarios/search/','method'=>'post','autocomplete'=>'off'/*,  'style'=>'display:inline' */)) !!}
                {{ Form::token() }}
                {{csrf_field()}}

                <br>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                    <label for="">Usuario</label>
                    <select name="user_id" id="user" class="form-control">

                        <option value="" selected disabled>--Seleccione--</option>
                        @foreach ($users as $userObject)
                            <option value="{{$userObject->id}}">{{$userObject->name." ".$userObject->last_name }}</option>
                        @endforeach

                    </select>
                </div>




                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">

                    <label for="">Roles</label>
                    <select name="rol" id="rol" class="form-control">
                        <option value="" selected disabled>--Seleccione--</option>
                        <option value="{{config('app.por_administrativo')}}">              Administrativo</option>
                        <option value="{{config('app.por_administrativo_hibrido')}}">      Administrativo Hibrido</option>
                        <option value="{{config('app.por_supervisor')}}">                  Supervisor</option>
                        <option value="{{config('app.por_profesional')}}">                 Profesional</option>
                        <option value="{{config('app.por_propietario')}}">                 Propietario</option>
                    </select>
    
                </div>




                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                    <br>
                    <div class="form-group">
                        {{ Form::submit('Filtrar', ["class" => "btn btn-primary btn-block"])}}
                    </div>
                </div>
            
            
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                    <br>
                    <button type="button" class="btn btn-secondary btn-block" id="limpiar">
                        Limpiar 
                    </button>
                </div>

                {!! Form::close() !!}


            </div>                  {{-- row - filtros --}}



            <div class="row" >       {{-- errores div --}}

                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                
                    @if(session('error'))
                        <div class="alert alert-danger" role="alert">
                            <span id="mensaje_flash_data_error">{{session('error')}}</span>
                        </div>
                    @endif
                        
                </div>
            </div>                  {{-- errores div --}}



            <div class="row col-lg-9 col-md-12 col-sm-12 col-xs-12">  

                <br>
                <table id="datatable" class="table table-head-fixed text-nowrap dataTable dtr-inline table-striped" style="font-size: 115%">

                    <thead>
                        <tr>
                            <th class="text-left">USUARIO</th>
                            <th class="text-left">ROLES</th>
                            <th class="text-left">OPCIONES</th>
                        </tr>
                    </thead>

                

                    <tbody>

                        @if($user)

                            <tr>
                                <td>{{ $user->name ." ". $user->last_name }} </td>
                                <td>
                                    @foreach ($user->roles as $rol)
                                        <span class="badge" style="background-color: black"; font-size: 120%>
                                            {{ $rol->name }}
                                        </span>
                                    @endforeach
                                </td>

                                <td>

                                    @can('Ver roles y permisos de usuarios')

                                        <a href="" data-target="#modal-Ver_roles_permisos_Usuario-{{$user->id}}" data-toggle="modal" title="Editar" id="edit-button">
                                            <button class="btn btn-info"> 
                                                <i class="fa fa-eye"></i>   
                                            </button>
                                        </a>
                                        @include('usuario.modal-Ver_roles_permisos_Usuario')
                                        
                                    @endcan
                                    

                                    @can('Ver y editar roles y permisos de usuarios')

                                        <a href="" data-target="#modal-Editar_roles_permisos_Usuario-{{$user->id}}" data-toggle="modal" title="Editar" id="edit-button">
                                            <button class="btn btn-info"> 
                                                <i class="fa fa-pencil"></i>
                                            </button>
                                        </a>
                                        @include('usuario.modal-Editar_roles_permisos_Usuario')

                                    @endcan

                                </td>
                            </tr>
                        @else
                            @foreach ($users as $user)

                                <tr>
                                    <td>{{ $user->name ." ". $user->last_name }} </td>
                                    <td>
                                        @foreach ($user->roles as $rol)
                                            <span class="badge" style="background-color: black"; font-size: 120%>
                                                {{ $rol->name }}
                                            </span>
                                        @endforeach

                                    </td>


                                    <td>

                                        @can('Ver roles y permisos de usuarios')

                                            <a href="" data-target="#modal-Ver_roles_permisos_Usuario-{{$user->id}}" data-toggle="modal" title="Editar" id="edit-button">
                                                
                                                <button class="btn btn-info"> 
                                                    <i class="fa fa-eye"></i>   
                                                </button>
                                            </a>
                                            @include('usuario.modal-Ver_roles_permisos_Usuario')

                                        @endcan
                                    

                                        @can('Ver y editar roles y permisos de usuarios')

                                            <a href="" data-target="#modal-Editar_roles_permisos_Usuario-{{$user->id}}" data-toggle="modal" title="Editar" id="edit-button">
                                                <button class="btn btn-info"> 
                                                    <i class="fa fa-pencil"></i>
                                                </button>
                                            </a>
                                            @include('usuario.modal-Editar_roles_permisos_Usuario')
                                            
                                        @endcan

                                    </td>
                                </tr>
                                
                                

                                {{-- <script type="text/javascript"> 

                                    //intento resetear modal al cerrar

                                    $('#modal-editarUsuario-{{$user->id}}').on('hidden.bs.modal', function () {
                                        $(this).find('form').trigger('reset');
                                    })
                                    
                                </script> --}}
                                
                            @endforeach
                            
                            
                        @endif
                        
                    </tbody>
                </table>


            </div> {{-- col --}}
        <!-- para paginar -->
        {{-- {{$users->render()}}  --}}
        
        </div> {{-- row principal --}}



    </div> {{-- container --}}        







@endSection








<!-- SELECT2 -->


@push('scripts')

<script>
    $('#user').select2();               
    $('#rol').select2();               
</script>


@endpush








