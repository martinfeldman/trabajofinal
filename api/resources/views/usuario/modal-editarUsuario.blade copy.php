    
    {{-- {{dd($expediente)}} --}}


    {{-- {{dd($users, $userSelected, $roles)}} --}}
    
    {{-- {{dd($permisosAdministrativo, $permisosUnicosSupervisor, $permisosProfesional)}} --}}





    
    <div class="modal fade modal-slide-in-right" aria-hidden="true" role="dialog" tabindex="-1" 
        id="modal-editarUsuario-{{$user->id}}">



        

    

        <div class="modal-dialog" role="document" style="width: 70%;">

            <div class="modal-content">


                <div class="modal-header text-center">

                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"> x </span>    
                    </button>

                    <h4 class="modal-title">Editar Roles y Permisos de usuario {{$user->name ." ". $user->last_name}}</h4> 
            
                </div> {{-- modal-header --}}



                <div class="modal-body" {{-- style="min-height:70vh" --}}>
                    

                    {{ Form::open(array('action'=> array('App\Http\Controllers\UsuarioController@update', $user->id), 'method'=>'patch' )) }}
                    {{ Form::token() }}
                    {{-- {{csrf_field()}} --}}



                        @livewire('editar-usuario-tabla-permisos', compact(
                                'user',
                                'roles',
                                'permisosAdministrativo',
                                'permisosUnicosSupervisor',
                                'permisosProfesional',
                            ), 
                        )

                        <br> <br>  <br> <br><br> <br>  <br> <br><br> <br>  <br> <br><br> <br>  <br> <br>
                                
                    {{-- </div> --}}
                    

                </div> {{-- modal-body --}}

                <br><br><br><br><br><br><br><br><br><br><br><br>


                <div class="modal-footer">
                    <br> <br>
                    

                        <button type="submit" class="btn btn-success col-lg-6 col-md-6 col-sm-6 col-xs-6">
                            Confirmar
                        </button>

                    {{ Form::Close() }}
                    
                        <button type="button" class="btn btn-danger col-lg-5 col-md-5 col-sm-5 col-xs-5" data-dismiss="modal">
                        Cancelar
                        </button>



                </div>  {{-- modal-footer --}}

                

            </div>      {{-- modal-content --}}

        </div>          {{-- modal-dialog --}}


        


    <script type="text/javascript" src="{{asset('js/usuarios/index.js')}}"></script>

    </div> {{-- modal --}}
    
    




