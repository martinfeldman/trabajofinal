


    <div class="modal fade modal-slide-in-right" aria-hidden="true"
        role="dialog" tabindex="-1" id="modal-DesestimarComprobantePago-{{$expediente->expediente_id}}">
    
    
 

           {{--  {{dd($expediente);}} --}}



            @if (count($errors)>0)    
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li> {{$error}} </li>
                        @endforeach
                    
                    </ul>
                </div>
            @endif    
            
            

           

          
            <div class="modal-dialog">

                <div class="modal-content">
    
    
                    <div class="modal-header text-center" >
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true"> X </span>    
                        </button>
                        <h3 class="modal-title">Desestimar Comprobante de Pago</h3>
                        
                    
                        <div class="modal-body">

                            

              
                           


                           
                            {!! Form::open(array('url'=>'expedientes','method'=>'POST','autocomplete'=>'off')) !!}
                            {{ Form::token() }}
                            @csrf

                            <form method="POST" enctype="multipart/form-data" action="{{route('expedientes.desestimarComprobantePago', ['id' => $expediente->expediente_id])}}">

                               {{--  <div class="form-group"> --}}

                                    {{-- //AGREGAR CHECKBOX --}}
                                            
                                    

                                    <div class="form-group">

                                        <br>
                                        <!-- Entrada de Motivo -->        
                                          <label for="motivo">Indique el motivo por el que desestima Comprobante</label> <br>
                                          <input type="text" name="motivo" class="form-control" placeholder=""> 
                
                                    </div> 



                                    {{-- <div class="row">
                                        <input class="form-check-input" type="radio" name="liquidarCheckbox">
                                        <label class="form-check-label" for="liquidarCheckbox">
                                            Liquidar Expediente
                                        </label> --}}


                                        {{-- <input class="form-check-input" type="text" name="desaprobadoCheckbox">
                                        <label class="form-check-label" for="desaprobadoCheckbox">
                                            Motivo
                                          </label> --}}

                                   {{--  </div> --}}


                             {{--    </div>     --}}




                        
                                

                        </div>
                        
                        <div class="modal-footer">   <!-- Botones --> 
                
                           
                            <a href="{{route('expedientes.desestimarComprobantePago', ['id' => $expediente->expediente_id])}}">
                                <button type="submit"  
                                    formaction="{{url('/expedientes/desestimarComprobantePago', ['id' => $expediente->expediente_id])}}"
                                    class="col-lg-6 col-md-6 col-sm-6 col-xs-6 btn btn-success" id="botonAprobarExpediente" >
                                        Confirmar
                                </button>
                            </a>

                        {!!Form::close()!!}
                        </form>

                            <button class="col-lg-6 col-md-6 col-sm-6 col-xs-6 btn btn-danger" type="button" data-dismiss="modal">Cancelar</button>



                        </div>
        
                        
    

                    </div>

                </div>

            </div>
    



        

</div>






@section('jsScripts')





 @endSection