    
    {{-- {{dd($expediente)}} --}}


    @php


    /** 
     * INTEGRACION CON https://developers.viumi.com.ar/page/checkout/requirements API
    */

        // $curl = curl_init();

        // curl_setopt_array($curl, array(
        //     CURLOPT_URL => 'https://auth.geopagos.com/oauth/token',
        //     CURLOPT_CUSTOMREQUEST => 'POST',
        //     CURLOPT_POSTFIELDS =>'{
        // "grant_type": "client_credentials",
        // "client_id": "XXXXXXX-XXX-XXXX-XXX-XXXXXXXXXXXX",
        // "client_secret": "XXXXXXXXXX",
        // "scope": "*"
        // }',
        //     CURLOPT_HTTPHEADER => array(
        //     'Content-Type: application/json'
        //     ),
        // ));

        // // dd();

        // $response = curl_exec($curl);

        // curl_close($curl);

        

    @endphp





    <div class="modal fade modal-slide-in-right" aria-hidden="true"
        role="dialog" tabindex="-1" id="modal-VerInfoLiquidacion-{{$expediente->expediente_id}}">

        {{-- {{ Form::open(array('url'=>'expedientes', 'method'=>'GET','autocomplete'=>'off' )) }} --}}

    

            <div class="modal-dialog">

                <div class="modal-content">


                    <div class="modal-header">

                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true"> x </span>    
                        </button>

                        <h4 class="modal-title text-center">Información para abonar la liquidación del Expediente Nº {{$expediente->expediente_numero}}</h4> 
                    
                    </div> {{-- modal-header --}}



                    <div class="modal-body">
                        
                        <p>MONTO A ABONAR: ${{$expediente->liquidacion}}</p> <br>
                        <p>CBU: {{config('app.cbu_banco')}}</p>

                    </div> {{-- modal-body --}}

                    

                    <div class="modal-footer">

                        <button type="button" class="btn btn-default" data-dismiss="modal">
                            Salir
                        </button>
                        
                    </div>  {{-- modal-footer --}}




                </div> {{-- modal-content --}}
            </div> {{-- modal-dialog --}}


        {{-- {{ Form::Close() }} --}}


    </div> {{-- modal --}}
    
    











    
@section('jsScripts')


    <script type="text/javascript">
    
    
        function cerrarInfo() {
            $('#modal-VerInfoLiquidacion-{{$expediente->expediente_id}}').modal('toggle');
        };
     
    </script>
    
    
    
@endSection