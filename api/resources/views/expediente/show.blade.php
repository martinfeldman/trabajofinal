@extends ('layouts.admin')





{{-- @php
    dd($expediente->comprobante_pago_id);

    if ($expediente->comprobante_pago_id != null) {
        echo ('diferente');
    } else {
        echo ('null');
    }
@endphp
 --}}



{{--
    
        VISTA DE EXPEDIENTES SHOW

        PANTALLA IMPORTANTE QUE EXPONE LA INFORMACIÓN DE UN EXPEDIENTE EN PARTICULAR, Y
        EN LA CUAL APARECEN BOTONES PARA QUE LOS USUARIOS DE TODOS LOS ROLES PUEDAN LLEVAR A CABO LOS CASOS DE USO
        DE ACUERDO A LA CONDICIÓN DEL EXPEDIENTE
        
        La condición es el estado en los ciclos de vida que puede tener éste, tanto en el ideal (habitual)
        como en el que extiende a otros casos de uso (menos habitual).

        -> VARIABLES DE ENTRADA: 
    

    Para poder mostrar un expediente, esta vista recibe 
    
    - un expediente,
    - datosExtraExpediente
    


    Los profesionales pueden abrir expedientes exclusivamente para sí mismos



        -> SALIDA: 


        Expediente 



        -> HISTORIAS DE USUARIO RELACIONADAS: 


        - Crear Expediente

    
--}}
    





    

    {{-- {{dd($expediente);}} --}}
    {{-- {{dd($condiciones)}} --}}
    {{-- {{dd($historialCondicionesExpediente)}} --}}
    {{-- {{dd($dataExtraExpediente[0]->comprobantePago_nombreArchivo)}} --}}
    {{-- {{dd($dataExtraExpediente[0]);}} --}}
    {{-- {{dd($dataExtraExpediente);}} --}}
    {{-- {{ $test = $data[0]}} --}}
    {{-- {{dd(config('app.esperando_comprobante_1'));}} --}}
    {{-- {{dd($motivosFormularioIncompleto)}} --}}



@section('contenido')
    



    {{-- ACA COMIENZA MI NUEVO DISEÑO --}}

    <div class="container col-lg-9 col-md-9 col-sm-9 col-xs-9" name="title">


        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
        <h2>
        
            @if ($expediente->estado_id == config('app.para_revisar')
            || $expediente->condicion_id == config('app.formulario_desaprobado'))
                <strong>Visualizando Formulario de Expediente</strong>

            @else 
                <strong>Visualizando Expediente </strong>    
            @endif
        </h2>
        {{-- <br> --}}

        </div>

        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
            
            
                @switch($expediente->estado_id)

                    @case(config('app.tramite_inconcluso'))

                        <h2 class="text-center" style="border-style:solid; color:red">
                            <strong style="">TRÁMITE INCONCLUSO</strong>   
                        </h2> 
                        @break


                    @case(config('app.formulario_a_revisar'))

                        <h2 class="text-center" style="border-style:solid; color:pink">
                            <strong>PARA REVISIÓN</strong>
                        </h2>   
                        @break

                    @case(config('app.estado_abierto'))
                        
                        <h2 class="text-center" style="border-style:solid; color:green">
                            <strong>ABIERTO</strong>    
                        </h2>   
                        @break

                    @case(config('app.estado_cerrado'))

                        <h2 class="text-center" style="border-style:solid; color:blue">
                            <strong>CERRADO</strong> 
                        </h2>      
                        @break

                @endswitch 
            
            
            {{-- <br> --}}
    
        </div>


        @if (count($errors)>0)    
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li> {{$error}} </li>
                    @endforeach
                
                </ul>
            </div>
        @endif    
        


    </div> <!-- .row  name="title"-->


    <div class="container col-lg-9 col-md-9 col-sm-9 col-xs-9">
        @switch(true)

            {{-- @case(session('success'))
                <div class="alert alert-success" role="alert">
                    <span id="mensaje_flash_data_success">{{session('success')}}</span>
                </div>
                @break --}}

            @case (session('error'))
                <div class="alert alert-danger" role="alert">
                    <span id="mensaje/_flash_data_error">{{session('error')}}</span>
                </div>
                @break

            {{-- @case (session('status'))
                <div class="alert alert-info" role="alert">
                    <span id="mensaje_flash_data_status">{{session('status')}}</span>
                </div>
                @break --}}

        @endswitch
    </div>

    

    

    {!! Form::open(array('url'=>'expediente','method'=>'POST','autocomplete'=>'off', 'files'=>'true')) !!}
    {{ Form::token() }}




    <div class="container col-lg-12 col-md-12 col-sm-12 col-xs-12" style="min-height: 75vh;">

        <br>

        <div class="container col-lg-9 col-md-9 col-sm-9 col-xs-9 table-responsive" name="columna-izquierda">

            <table class="table table-bordered table-striped text-center"  style="font-size: 115%">

                <thead>
                
                </thead>

                <tbody>

                    <tr>
                        <td colspan="30" id="td">NUMERO: {{$expediente->expediente_numero}} </td>
                        <td colspan="30" id="td">FECHA INICIO: {{\Carbon\Carbon::createFromTimestamp(strtotime($expediente->fecha_inicio))->format('d-m-Y  H:i:s')}}</td>
                    
                    </tr>

                    <tr>
                        <td colspan="30">PROFESIONAL: 
                            {{$dataExtraExpediente[0]->profesional_nombres}}
                            {{$dataExtraExpediente[0]->profesional_apellidos}}
                        </td>
                        <td colspan="30">CUIT: {{$dataExtraExpediente[0]->profesional_numero_matricula}}</td>

                    </tr>

                    <tr>
                        <td colspan="30">PROPIETARIO:
                            {{$dataExtraExpediente[0]->propietario_nombres}}
                            {{$dataExtraExpediente[0]->propietario_apellidos}}
                        </td>
                        <td colspan="30">CUIT: {{$dataExtraExpediente[0]->propietario_cuit}}</td>
                    </tr>

                    <tr>
                        <td colspan="60" class="text-center bg-primary" id="boldText">TAREAS Y DOCUMENTACION</td>
                    </tr>

                    <tr>
                        <td colspan="20">TAREA: {{$dataExtraExpediente[0]->tipo_tarea}}</td>
                        <td colspan="30">
                        
                            ARCHIVO: {{isset($dataExtraExpediente[0]->nombre_archivo) ? $dataExtraExpediente[0]->nombre_archivo : ""}}
                                
                        </td>



                        <td colspan="10">

                            {!! Form::open(array('url'=>'expedientes','method'=>'post','autocomplete'=>'off', 'files'=>true)) !!}
                            {{ Form::token() }}
                            
                            {{csrf_field()}}


                        
                            
                            <form method="post" enctype="multipart/form-data" action="{{route('expedientes.descargarPlanoODocumento', 
                            ['id' => $expediente->expediente_id, 'nombre_archivo' =>$dataExtraExpediente[0]->nombre_archivo])}}">
                                @csrf

                                <a href="{{route('expedientes.descargarPlanoODocumento', ['id' => $expediente->expediente_id, 'nombre_archivo' =>$dataExtraExpediente[0]->nombre_archivo])}}">
                                    <button class="btn btn-info" type="submit" style="font-size: 105%"
                                    formaction="{{url('/expedientes/descargarPlanoODocumento', ['id' => $expediente->expediente_id, 'nombre_archivo' =>$dataExtraExpediente[0]->nombre_archivo])}}">
                                        Descargar Archivo Entregado
                                    </button>
                                </a>


                                <div class="">
                                
                                    <br>
                                    @if($expediente->condicion_id == config('app.formulario_incompleto'))

                                    
                                        @can('Cargar Archivo de Tarea')


                                        {!! Form::open(array('url'=>'expedientes','method'=>'post','autocomplete'=>'off', 'files'=>true)) !!}
                                        {{ Form::token() }}

                                                <form method="post" enctype="multipart/form-data" 
                                                    action="{{route('expedientes.cargarArchivoDeTarea', [
                                                        'id' => $expediente->expediente_id,
                                                        'tarea_id' =>$expediente->tarea_id,
                                                    ])}}">
                                                    @csrf
                                                
                                                    <!-- Entrada de Archivo  -->       
                                                    <input type="file" name="archivoSubido" class="form-control-file" title="Seleccione un archivo" > 
                                                

                                                    <a href="">
                                                        <button type="submit"  class="btn btn-success" id="botonCargarArchivoDeTarea" style="font-size: 105%"
                                                            formaction="{{url('/expedientes/cargarArchivoDeTarea', [
                                                                'id' => $expediente->expediente_id,
                                                                'tarea_id' =>$expediente->tarea_id,])}}">
                                                            Subir Nuevo Archivo
                                                        </button>
                                                    </a>

                                                </form>

                                        {!!Form::close()!!}       

                                        @endcan

                                    @endif

                                </div>

                            </form>
                            

                            {!!Form::close()!!}    


                        </td>

                    </tr>



                    <tr>
                        <td colspan="60" class="text-center bg-primary" id="boldText">OBRA</td>
                    </tr>

                    <tr>
                        <td colspan="30">OBJETO: {{$dataExtraExpediente[0]->objeto}} </td>
                        <td colspan="30">TIPOLOGIA: {{$dataExtraExpediente[0]->tipologia}} </td>
                    </tr>

                    <tr>
                        <td colspan="60" class="text-center bg-primary" id="boldText">UBICACION</td>
                    </tr>

                    <tr>
                        <td colspan="15">LOCALIDAD: {{$dataExtraExpediente[0]->localidad}} </td>
                        <td colspan="15">CALLE: {{$dataExtraExpediente[0]->calle}} </td>
                        <td colspan="15">NUMERO: {{$dataExtraExpediente[0]->numero}} </td>
                        <td colspan="15">BARRIO: {{$dataExtraExpediente[0]->barrio}} </td>                    
                    </tr>

                    <tr>
                        <td colspan="60" class="text-center bg-primary" id="boldText">DATOS CATASTRALES</td>
                    </tr>

                    <tr>
                        <td colspan="12">SECCION: {{$dataExtraExpediente[0]->seccion}} </td>
                        <td colspan="12">MANZANA: {{$dataExtraExpediente[0]->manzana}} </td>
                        <td colspan="12">CHACRA: {{$dataExtraExpediente[0]->chacra}} </td>
                        <td colspan="12">PARCELA: {{$dataExtraExpediente[0]->parcela}} </td>
                        <td colspan="12">PARTIDA INMOBILIARIA: {{$dataExtraExpediente[0]->partida_inmobiliaria}} </td>
                    </tr>

                    <tr>
                        <td colspan="60" class="text-center bg-primary" id="boldText">SUPERFICIE EN M²</td>
                    </tr>

                    <tr>

                    
                        <td colspan="20">A CONSTRUIR: 
                            @if ($expediente->superficie_a_construir == null) 
                                -
                            @else
                                {{$expediente->superficie_a_construir}}
                            @endif
                        </td>

                        <td colspan="20">CON PERMISO:
                            @if ($expediente->superficie_con_permiso == null) 
                                -
                            @else
                                {{$expediente->superficie_con_permiso}}
                            @endif
                        </td>

                        <td colspan="20">SIN PERMISO:
                            @if ($expediente->superficie_sin_permiso == null) 
                                -
                            @else
                                {{$expediente->superficie_sin_permiso}}
                            @endif
                        </td>

                    

                    </tr>

                </tbody>

            </table>


            <div class="row col-lg-12 col-sm-12 col-md-12 col-xs-12">

                @if ($expediente->nota_reemplazo_profesional_id != null)
                    <h4>Expediente vinculado a <a href="/reemplazo-profesional/{{$expediente->nota_reemplazo_profesional_id}}">Reemplazo Profesional</a></h4>
                @endif

            </div>

        </div> {{-- COLUMNA IZQUIERDA --}}

        {{-- <div class="container col-lg-1 col-md-1 col-sm-1 col-xs-1" name="columna-intermedia">

        </div> --}}


        <div class="container col-lg-3 col-md-3 col-sm-3 col-xs-3" name="columna-derecha"  style="font-size: 120%">

            <!-- Botones -->       
                    
            <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
        

                <p  style="font-size: 140%"><strong>CONDICIÓN</strong></p>

                <p  style="font-size: 140%">
                    {{$dataExtraExpediente[0]->condicion}}. <br>

                    @if ($motivosFormularioIncompleto)
                        Motivos u Observaciones: <br>{{$motivosFormularioIncompleto}}     
                    @endif

                </p>

                <br>

                <a href="" data-target="#modal-VerHistorialCondiciones-{{$expediente->expediente_id}}" data-toggle="modal">
                    <button class="btn btn-info" style="font-size: 105%">
                        Ver Historial de Condiciones
                    </button>
                </a>

                @include('expediente.modal-VerHistorialCondiciones')


                <br> <br>



                <p  style="font-size: 140%"><strong>ACCIONES</strong></p>


                @can('Valorar formulario de expediente')

                
                    @if ($expediente->condicion_id == config('app.formulario_a_revisar') 
                    || $expediente->condicion_id == config('app.formulario_incompleto'))

                    <a href="{{route('expedientes.valorarFormularioExpediente', ['id' => $expediente->expediente_id])}}" data-target="#modal-ValorarFormularioExpediente-{{$expediente->expediente_id}}" data-toggle="modal">
                        <button class="btn btn-success" id="botonValorarFormularioExpediente" style="font-size: 105%">
                            Valorar Formulario de Expediente
                        </button>
                    </a>

                    @include('expediente.modal-ValorarFormularioExpediente')

                    <br><br>

                    @endif

                
                    
                @endcan

            


                


            
                @if($expediente->condicion_id == config('app.formulario_incompleto') || $expediente->condicion_id == config('app.para_liquidar'))


                    @can('Liquidar expediente')

                        <a href="{{route('expedientes.liquidarExpediente', ['id' => $expediente->expediente_id])}}" 
                            data-target="#modal-LiquidarExpediente-{{$expediente->expediente_id}}" data-toggle="modal">
                                <button class="btn btn-info" style="font-size: 105%">Liquidar Expediente</button>
                        </a>

                        <br><br>

                        @include('expediente.modal-LiquidarExpediente')

                    @endcan    



                    
                @elseif($expediente->condicion_id >= config('app.esperando_comprobante_1') 
                    && $expediente->condicion_id <= config('app.para_cerrar')
                    && $expediente->estado_id == config('app.estado_abierto'))                                   
                

                    <a href="" data-target="#modal-VerInfoLiquidacion-{{$expediente->expediente_id}}" data-toggle="modal">
                        <button class="btn btn-info" style="font-size: 105%">
                            Ver Info de Liquidación
                        </button>
                    </a>

                    <br> <br>
                    @include('expediente.modal-VerInfoLiquidacion')


                    @can('Cargar Comprobante de Pago')
                    

                        {!! Form::open(array('url'=>'expedientes','method'=>'post','autocomplete'=>'off', 'files'=>true)) !!}
                        {{ Form::token() }}
                        
                        {{csrf_field()}}

                            <div class="form-group row col-lg-12 col-sm-12 col-md-12 col-xs-12">
                            

                                {{-- <form method="post" enctype="multipart/form-data" action="{{route('expedientesUseCases.store', ['id' => $expediente->expediente_id])}}"> --}}
                                <form method="post" enctype="multipart/form-data" action="{{route('expedientes.cargarComprobantePago', ['id' => $expediente->expediente_id])}}">
                                    @csrf

                                        {{-- <label for="comprobantePago">Cargue el archivo aquí</label> <br> --}}


                                        
                                        {{-- si expediente tiene comprobante_pago_id != null --}} 
                                        @if ($expediente->comprobante_pago_id != null)

                                            <div style="border; border-style: dashed; border-color: lightblue ; font-size: 115%">
                                                <p > <strong>Aún puede cambiar el Comprobante de Pago.</strong>
                                                    <br><br>
                                                    Subido actualmente: <br>
                                                    {{$dataExtraExpediente[0]->comprobantePago_nombreArchivo}}
                                                </p>
                                            </div>
                                                <br>

                                            <!-- Entrada de Comprobante de Pago -->       
                                            <input type="file" name="comprobantePago" class="form-control-file" title="Seleccione un archivo" > 



                                        @else
                                            
                                            <input type="file" name="comprobantePago" class="form-control-file" title="Seleccione un archivo" > 

                                                
                                        @endif


                                    <br>

                                    <div class="form-group row col-lg-12 col-sm-12 col-md-12 col-xs-12">

                                        <a href="{{route('expedientes.cargarComprobantePago', ['id' => $expediente->expediente_id])}}">
                                            <button type="submit"  class="btn btn-success" id="botonAprobarExpediente" style="font-size: 105%"
                                            formaction="{{url('/expedientes/cargarComprobantePago', ['id' => $expediente->expediente_id])}}">
                                                Cargar Comprobante de Pago
                                            </button>
                                        </a>


                                    </div>

                                </form>
                                {!!Form::close()!!}            

                            </div> <!-- .form-group row -->     


                            @if ($expediente->condicion_id == config('app.esperando_comprobante_1'))
                                
                                {!! Form::open(array('url'=>'expedientes','method'=>'post','autocomplete'=>'off', 'files'=>true)) !!}
                                {{ Form::token() }}


                                <div class="form-group row col-lg-12 col-sm-12 col-md-12 col-xs-12">


                                    <p style="font-size: 120%">Nos interesa saber si ha provisto al propietario de los datos para realizar el pago</p>  

                                    <a href="{{route('expedientes.propietarioNotificadoLiquidacion', ['id' => $expediente->expediente_id])}}">
                                        <button type="submit"  class="btn btn-success" id="botonAprobarExpediente" style="font-size: 105%"
                                        formaction="{{url('/expedientes/propietarioNotificadoLiquidacion', ['id' => $expediente->expediente_id])}}">
                                            Sí, he notificado al propietario
                                        </button>
                                    </a>


                                </div>

                            @endif

                    

                        <br><br>
                    @endcan

            

                @endif





                
                @if ($expediente->condicion_id >= config('app.esperando_comprobante_1') && $expediente->comprobante_pago_id != NULL && $expediente->estado_id != config('app.tramite_inconcluso'))


                    {!! Form::open(array('url'=>'expedientes','method'=>'post','autocomplete'=>'off', 'files'=>true)) !!}
                    {{ Form::token() }}
                    {{-- @method('GET') --}}
                    {{csrf_field()}}

                        <div class="form-group row col-lg-12 col-sm-12 col-md-12 col-xs-12">
                            <form method="post" enctype="multipart/form-data" action="{{route('expedientes.descargarComprobantePago', ['id' => $expediente->expediente_id])}}">
                            @csrf

                            <a href="{{route('expedientes.descargarComprobantePago', ['id' => $expediente->expediente_id])}}" {{-- data-target="#modal-DescargarComprobantePago{{$expediente->expediente_id}}" data-toggle="modal" --}}>
                                <button  type="submit" class="btn btn-info" formaction="{{url('/expedientes/descargarComprobantePago', ['id' => $expediente->expediente_id])}}" style="font-size: 105%">
                                    Descargar Comprobante de Pago
                                </button>
                            </a>

                            <br><br>

                            </form>
                        </div> <!-- .form-group row -->     

                    {!!Form::close()!!}            



                
                    @if ($expediente->condicion_id == config('app.para_cerrar') )



                        @can('Cerrar expediente')  
                            <a href="" data-target="#modal-CerrarExpediente-{{$expediente->expediente_id}}" data-toggle="modal">
                                <button class="btn btn-info" id="CerrarExpedienteButton" style="font-size: 105%">Cerrar Expediente</button>
                            </a>

                            <br><br>

                            @include('expediente.modal-CerrarExpediente')

                        @endcan 





                        @can('Desestimar Comprobante de Pago')
                            <a href="{{-- {{route('expedientes.desestimarComprobantePago', ['id' => $expediente->expediente_id])}} --}}" 
                                data-target="#modal-DesestimarComprobantePago-{{$expediente->expediente_id}}" data-toggle="modal">
                                    <button class="btn btn-danger" style="font-size: 105%">Desestimar Comprobante de Pago</button>
                            </a>

                            <br><br>

                            @include('expediente.modal-DesestimarComprobantePago')
                            
                        @endcan


                    
                        

                    
                    @endif

                    
                @endif
            
            

                @can('Renunciar a Direccion de Obra')
                    
                    @if ($dataExtraExpediente[0]->tipo_tarea_id == config('app.direccion_obra_tarea_id') && $expediente->estado_id == config('app.estado_abierto') )

                        <a href="{{-- {{route('expedientes.renunciarDireccionObra', ['id' => $expediente->expediente_id])}} --}}" 
                        data-target="#modal-RenunciarDireccionObra-{{$expediente->expediente_id}}" data-toggle="modal">
                            <button class="btn btn-danger" style="font-size: 105%">Renunciar a Dirección de Obra</button>
                        </a>

                        <br><br>


                        @include('expediente.modal-RenunciarDireccionObra')

                    @endif

                @endcan


                
                @if($expediente->condicion_id > config('app.para_cerrar') && $expediente->condicion_id < config('app.cerrado_por_falta_tareas_reemplazo_profesional') )

                    <h3><strong>CERTIFICADO</strong></h3>

                    {!! Form::open(array('url'=>'expedientes','method'=>'post','autocomplete'=>'off', 'files'=>true, 'style'=>'display:inline')) !!}
                    {{ Form::token() }}

                    {{csrf_field()}}

                        <form method="post" enctype="multipart/form-data" 
                        action="{{route('expedientes.descargarCertificado', ['id' => $expediente->expediente_id])}}" class="d-inline">
                            @csrf
                        
                            <a href="{{route('expedientes.descargarCertificado', ['id' => $expediente->expediente_id])}}" {{-- data-target="#modal-DescargarComprobantePago{{$expediente->expediente_id}}" data-toggle="modal" --}}>
                                <button type="submit" class="btn btn-success" formaction="{{url('/expedientes/descargarCertificado', ['id' => $expediente->expediente_id])}}" style="font-size: 105%">
                                    Descargar Certificado
                                </button>
                            </a>
                            
                            <br><br>

                        
                        </form>

                    {!!Form::close()!!}

                    

                @endif



                    







            </div>  {{-- col --}}


        </div> {{-- container . columna-derecha --}} 















        {!!Form::close()!!}

    </div> {{-- CONTAINER MAIN --}}















@endSection

