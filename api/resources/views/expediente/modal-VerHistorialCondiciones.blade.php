    
    {{-- {{dd($expediente)}} --}}








    
    <div class="modal fade modal-slide-in-right" aria-hidden="true" role="dialog" tabindex="-1" 
        id="modal-VerHistorialCondiciones-{{$expediente->expediente_id}}">

        {{--  {{ Form::open(array('url'=>'expedientes', 'method'=>'GET','autocomplete'=>'off' )) }} --}}

    

            <div class="modal-dialog">

                <div class="modal-content">


                    <div class="modal-header text-center">

                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true"> x </span>    
                        </button>

                        <h4 class="modal-title">Historial de Condiciones de Expediente</h4> 
                    
                    </div> {{-- modal-header --}}



                    <div class="modal-body">
                        
                        <div class="table-responsive">

                            <table class="table table-striped table-bordered table-hover">

                                <thead>

                                    <th class="col-lg-6 col-md-6 col-sm-6 col-xs-6 bg-primary text-center">CONDICION</th>
                                    <th class="col-lg-6 col-md-6 col-sm-6 col-xs-6 bg-primary text-center">FECHA DE CAMBIO</th>

                                </thead>

                            
                                @forelse ($condicionesActuales as $condicionActual)

                                    <tr>

                                        <td class="text-center">{{$condicionActual->condicion}}</td>
                                        <td class="text-center">{{\Carbon\Carbon::createFromTimestamp(strtotime($condicionActual->fecha))->format('d-m-Y  H:i:s')}}</td>
                                        
                                    </tr>

                                @empty 

                                    <h3><strong>Actualmente no hay registros de cambios</strong></h3>

                                @endforelse


                            </table>        

                        </div> {{-- table-responsive --}}

                    </div> {{-- modal-body --}}


                    
                    <div class="modal-footer">


                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6"></div>

                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">

                            <button type="button" class="btn btn-default col-lg-12 col-md-12 col-sm-12 col-xs-12 float-right" data-dismiss="modal">
                                Salir
                            </button>

                        </div>

                    </div> {{-- modal-footer --}}




                </div> {{-- modal-content --}}

            </div> {{-- modal-dialog --}}


        {{-- {{ Form::Close() }} --}}


    </div> {{-- modal --}}
    
    




