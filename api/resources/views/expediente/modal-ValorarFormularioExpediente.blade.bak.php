


    <div class="modal fade modal-slide-in-right" aria-hidden="true"
        role="dialog" tabindex="-1" id="modal-ValorarFormularioExpediente-{{$expediente->expediente_id}}">
    
    
 

           {{--  {{dd($expediente);}} --}}



            @if (count($errors)>0)    
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li> {{$error}} </li>
                        @endforeach
                    
                    </ul>
                </div>
            @endif    
            
            

           

          
            <div class="modal-dialog">

                <div class="modal-content">
    
    
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true"> X </span>    
                        </button>
                        <h3 class="modal-title">Valorar Formulario de Expediente</h3>
                        
                    
                        <div class="modal-body">

                            <br>

                            @php
                                use Illuminate\Support\Facades\Redirect;

                                use \App\Events\ValoracionFormularioExpedienteEvent;

                                use \App\Services\ExpedienteService;

                                $expedienteService = new ExpedienteService;

                            @endphp
                           
                                <div class="form-group">

                                    <!-- Botones --> 
                                    <button onclick="document.write('<?php event($expedienteService->cambiarCondicionExptValidarFormularioExpediente($expediente));?>');"
                                         class="col-lg-7 col-md-7 col-sm-7 col-xs-7 btn btn-success" id="botonAprobarExpediente" >
                                         Aprobar
                                    </button>

                                    <button class="col-lg-5 col-md-5 col-sm-5 col-xs-5 btn btn-danger"  id="botonDesaprobarExpediente">Desaprobar</button>

                                </div>    


                        

                        </div>
                        
                        <div class="modal-footer">
                
                        </div>
        

       

                    </div>

                </div>

            </div>
    

   

         {{-- <script type="text/javascript" src="{{asset('js/expediente/useCases/valorarFormularioExpediente.js')}}"></script> --}}

</div>
