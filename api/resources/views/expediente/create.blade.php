

@extends ('layouts.admin')


{{--
    /**
    /*
    /*
        VISTA DE EXPEDIENTES CREATE


        -> VARIABLES DE ENTRADA: 
    

    Para poder crear un expediente, esta vista recibe la info de las posibles 
        localidades,
        objetos,
        profesionales,
        propietarios,
        tipos_tarea,
        tipologias,


    Los profesionales pueden abrir expedientes exclusivamente para sí mismos



        -> SALIDA: 


        Expediente 



        -> HISTORIAS DE USUARIO RELACIONADAS: 


        - Crear Expediente

    */ 
--}}
    








    {{-- {{ dd($propietarios);  }} --}}
    {{-- {{ dd($ultimoNumeroMatricula);  }} --}}

@section('contenido')

    

    <div class="row" id="main?">     
    
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <h2><strong>Formulario para Nuevo Expediente </strong></h2>
            <br>
        </div>  
            
            


        @if (count($errors)>0)    
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li> {{$error}} </li>
                    @endforeach
                
                </ul>
            </div>
        @endif    
            
            
            
            
            
        {!! Form::open(array('url'=>'expedientes','method'=>'POST','autocomplete'=>'off', 'files'=>true)) !!}
        {{ Form::token() }}
        @csrf





        <div class="container col-lg-11 col-md-11 col-sm-11 col-xs-11" >

                
            {{-- Dependiendo del rol del usuario actual del sistema, 
            se dispone o no del elemento comboBox Profesionales.
            
            roles supervisor y administrativo  -> sí, disponen.
            
            rol profesional -> no. En cambio, el sistema selecciona el profesional
            a partir del Auth()->user()->profesional_id que seteamos como atritubuto del User Model. --}}
                
            
            <div class="row" name="primerFila" id="props/profs">
                
    
                @can('Ver comboBox de Profesionales en Completar formulario de expediente y Editar formulario de Expediente')

                    <div class="col-lg-6 col-sm-6 col-md-6 col-xs-6">
                    
                        <h4><strong>PROFESIONAL</strong></h4>   
                    
                        <div class="form-group">
                            <x-combo-box.profesionales :content="$profesionales" size=10 />
                        </div> 

                    </div> 
                @endcan       
                
                
                


                <div class="col-lg-6 col-sm-6 col-md-6 col-xs-6">

                    
                    <h4><strong>PROPIETARIO</strong></h4> 
                    
                    <div class="form-group">                            
                        <x-combo-box.propietarios :content="$propietarios" size=10 />
                    </div>

                </div>
                

            </div>  <!-- .row   name="primerFila" id="props/profs" -->



            <br>




            <div class="row" name="segundaFila" id="expediente" >
                
                <!-- del Expediente -->
                <div class="col-lg-6 col-sm-6 col-md-6 col-xs-6" name="columna1">
                
                    <h4><strong>TAREA</strong></h4>   
                
                    {{-- 'columna1' y 'columna2' terminan dentro de componente formulario.segundaFila --}}
                    <x-formulario.segundaFila :tareaid="null" :tipostareas="$tipos_tareas"/>
                    
            </div> <!-- .row name="segundaFila" id="expediente" -->   
                
                
            





            <div class="row" name="tercerFila" id="obra">   
                
                <!-- de la Obra -->
                
                <h4 class="col-lg-12 col-sm-12 col-md-12 col-xs-12" ><strong>OBRA</strong></h4>  

                <div class="col-lg-6 col-sm-6 col-md-6 col-xs-6" name="columna1">                        
                
                    <div class="form-group" name="objeto">
                        
                        <label for="objeto">OBJETO</label> <br>
                        
                        <!-- ComboBox de Objetos -->
                        <x-combo-box.objetos :content="$objetos" size=9/>
                    
                    </div>
                
                </div>



            

                <div class="col-lg-6 col-sm-6 col-md-6 col-xs-6 " name="columna2">

                    <div class="form-group" name="tipologia">
                    
                        <label for="tipologias">TIPOLOGÍA</label> <br>

                        <x-combo-box.tipologias :content="$tipologias" size=9/>
                    
                    </div>
                
                </div> 


            </div> <!-- .row name="tercerFila" id="obra" -->






            <br>

            <div class="row" name="cuartaFila" id="ubicacion">    
                
                
                <h4 class="col-lg-12 col-sm-12 col-md-12 col-xs-12"><strong>UBICACIÓN</strong></h4>         

                <div class="col-lg-3 col-sm-3 col-md-3 col-xs-3">
                    
                    <div class="form-group" name="ubicación">
                    
                        <label for="localidad">LOCALIDAD</label>    <br>           

                        <x-combo-box.localidades :content="$localidades" size=12 />

                    </div>       

                </div>




                <div class="col-lg-3 col-sm-3 col-md-3 col-xs-3">

                    <div class="form-group" name="calle">
                        
                        <label for="calle">CALLE</label> <br>

                        <input type="text" name="calle" class="form-control" placeholder="Calle" value="{{ old('calle') }}">   

                    </div>       

                </div>




                <div class="col-lg-3 col-sm-3 col-md-3 col-xs-3">

                    <div class="form-group" name="numero">

                        <label for="numero">NÚMERO</label> <br>

                        <input type="text" name="numero" class="form-control" placeholder="Número" value="{{ old('numero') }}"> 

                    </div>       

                </div>
                    



                <div class="col-lg-3 col-sm-3 col-md-3 col-xs-3">

                    <div class="form-group" name="barrio">

                        <label for="barrio">BARRIO</label> <br>

                        <input type="text" name="barrio" class="form-control" placeholder="Barrio" value="{{ old('barrio') }}">     
                    
                    </div>
                
                </div>

            </div> <!-- div.row  name="cuartaFila" id="ubicacion"-->







            <br>

            <div class="row" name="quintaFila" id="datosCatastrales">   
                
                
                <h4 class='col-lg-8 col-sm-8 col-md-8 col-xs-8'><strong>DATOS CATASTRALES</strong></h4>  
                <h4 class='col-lg-4 col-sm-4 col-md-4 col-xs-4'><strong>PARTIDA INMOBILIARIA</strong></h4> 

                <div class="col-lg-2 col-sm-2 col-md-2 col-xs-2">
                
                    <div class="form-group" name="seccion">  
                    
                        <label for="seccion">SECCIÓN</label> <br>

                        <input type="text" name="seccion" class="form-control" placeholder="ejemplo: 70" value="{{ old('seccion') }}">   

                    </div>
                
                </div>



                <div class="col-lg-2 col-sm-2 col-md-2 col-xs-2">

                    <div class="form-group" name="manzana">  
                        
                        <label for="manzana">MANZANA</label> <br>

                        <input type="text" name="manzana" class="form-control" placeholder="ejemplo: 70" value="{{ old('manzana') }}"> 
                    
                    </div>
                
                </div>
                    


                <div class="col-lg-2 col-sm-2 col-md-2 col-xs-2">

                    <div class="form-group" name="chacra"> 
                                                
                        <label for="chacra">CHACRA</label> <br>

                        <input type="text" name="chacra" class="form-control" placeholder="ejemplo: 70" value="{{ old('chacra') }}">  
                        
                    </div>
                
                </div>




                <div class="col-lg-2 col-sm-2 col-md-2 col-xs-2">
                
                    <div class="form-group" name="parcela">     
                    
                        <label for="parcela">PARCELA</label> <br>
                    
                        <input type="text" name="parcela" class="form-control" placeholder="ejemplo: 70" value="{{ old('parcela') }}">     
                    
                    </div>
                
                </div>


    
                
                
                <div class="col-lg-2 col-sm-2 col-md-2 col-xs-2">
                    
                    <div class="form-group" name="partida_inmobiliaria">     
            
                        <label for="partida_inmobiliaria">PARTIDA</label> <br>

                        <input type="text" name="partida_inmobiliaria_numero" class="form-control" placeholder="ejemplo: 70" value="{{ old('partida_inmobiliaria_numero') }}"> 

                    </div>
                
                </div>
                    

                    
                    <!--  FALTA AGREGAR BOTON PARA AGREGAR OTRA PARTIDA INMBOLIARIA    -->
                    
                    
                <div class="col-lg-2 col-sm-2 col-md-2 col-xs-2">
                    {{-- vacio, para ocupar el espacio sobrante --}}
                </div>

            
            </div>  <!-- .row  name="quintaFila" id="datosCatastrales"-->

            
                
                        




            <br>

            <div class="row" name="sextaFila" id="superficies">

                <h4 class="col-lg-12 col-sm-12 col-md-12 col-xs-12"><strong>SUPERFICIE (en m²)</strong></h4>   


                <div class="col-lg-4 col-sm-4 col-md-4 col-xs-4">

                    <div class="form-group" name="superficie_a_construir">                
                        
                        <label for="nombre">A CONSTRUIR</label> <br>

                        <input type="text" name="superficie_a_construir" class="form-control"
                        placeholder="ejemplo: 70" id="inputSuperficieAConstruir" value="{{ old('superficie_a_construir') }}">  

                    </div>
                
                </div> 




                <div class="col-lg-4 col-sm-4 col-md-4 col-xs-4">

                    <div class="form-group" name="superficie_con_permiso">
                                                
                        <label for="nombre">CON PERMISO</label> <br>

                        <input type="text" name="superficie_con_permiso" class="form-control"
                        placeholder="ejemplo: 70" id="inputSuperficieConPermiso" value="{{ old('superficie_con_permiso') }}">  

                    </div>
                
                </div> 




                <div class="col-lg-4 col-sm-4 col-md-4 col-xs-4">

                    <div class="form-group" name="superficie_sin_permiso">
                                                
                        <label for="nombre">SIN PERMISO</label> <br>

                        <input type="text" name="superficie_sin_permiso" class="form-control" 
                        placeholder="ejemplo: 70" id="inputSuperficieSinPermiso" value="{{ old('superficie_sin_permiso') }}"> 
                    
                    </div>

                </div> 

                
            </div> <!-- div.row   name="sextaFila" id="superficies"-->
                
            <br>
                
            

        




            <div class="row" name="septimaFila" id="aviso">

                <h4 class="text-center">
                    <strong>
                        Puede editar el Formulario después de confirmar hasta que sea verificado y valuado <br>
                        por la Administración del Colegio de Arquitectos.
                    </strong>
                </h4>

            </div>   <!-- .row   name="septimaFila" id="aviso"-->
            

            <br>
            

        


    
            <div class="row" name="octavaFila" id="botones">     <!-- Botones -->     
                
                <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">

                    <div class="form-group">

                        <button type="submit" class="btn btn-primary col-lg-6 col-sm-6 col-md-6 col-xs-6"><strong>CONFIRMAR</strong></button>

                        <button type="reset" class="btn btn-default col-lg-6 col-sm-6 col-md-6 col-xs-6"><strong>CANCELAR</strong></button>

                    </div>

                </div>



            </div>   <!-- div.row name="octavaFila" id="botones"-->  
            

            
            

        {!!Form::close()!!}

        </div> <!-- div.container-->     

    


    
    </div> <!-- div.row id="principal?"-->     


@endSection










@section('jsScripts')


<script> var DIRECCION_OBRA_TAREA_ID = "<?= config('app.direccion_obra_tarea_id') ?>";</script>
<script> var OBJETO_NUEVO_ID = "<?= config('app.objeto_nuevo') ?>";</script>

<script type="text/javascript" src="{{asset('js/expediente/create.js')}}"></script>


@endsection





<!-- SELECT2 -->


@push('scripts')

<script>
    $('#localidades').select2();        
    $('#objetos').select2();        
    $('#profesionales').select2();        
    $('#propietarios').select2();        
    $('#tipologias').select2();        
    $('#tareas').select2();        
</script>


@endpush

