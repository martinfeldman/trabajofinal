
@extends ('layouts.admin')



























@section('contenido')


    {{-- {{ dd($expedientes);  }} --}}
    {{--  {{ dd($currentUserRole);  }} --}}

    {{-- {{ dd($expedientesProfesional);  }} --}}


   {{--  <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12"> --}}


        {{-- {{QrCode::format('svg')->size(150)->generate('test');}} --}}


        <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <h2 class="col-lg-10 col-md-10 col-sm-10 col-xs-10"><strong>Expedientes</strong> 
            
            
            </h2>
            
            <br>
            <a href={{route('expedientes.create')}}>
                <button class="btn btn-success col-lg-2 col-md-2 col-sm-2 col-xs-2" title="Registrar Nuevo Expediente">Nuevo</button>
            </a>
        </div>
        
        {{--  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
            <x-inputs.search searchText={{$searchText}} url="expedientes" />
        </div> --}}
    {{-- </div> --}}
    

    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" name="filtros">

        {{--  <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12" name="filtros"> --}}

            {!! Form::open(array('url'=>'auditoria','method'=>'post','autocomplete'=>'off', 'style'=>'display:inline')) !!}
            {{ Form::token() }}
            {{csrf_field()}}

            <br> 
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">

                <label for="">Número Expediente</label>
                <select name="expediente_numero" id="expedientesNumeros" class="form-control">
                    <option value="" selected disabled>--Seleccione--</option>
                    @foreach ($expedientesNumeros as $expedienteNumero)
                        <option value="{{$expedienteNumero->expediente_numero}}">{{$expedienteNumero->expediente_numero}}</option>
                    @endforeach

                </select>

            </div>






            @can('administrativo.&.supervisor.columnaProfesional.expediente.index')
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">

                <label for="">Profesional</label>
                <select name="profesional" id="profesionales" class="form-control">

                    <option value="" selected disabled>--Seleccione--</option>
                    @foreach ($profesionales as $profesional)
                        <option value="{{$profesional->id}}">{{$profesional->profesional_nombres}} {{$profesional->profesional_apellidos}} ({{$profesional->profesional_numero_matricula}})</option>
                    @endforeach

                </select>


            </div>
            @endcan








            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">

                <label for="">Propietario</label>
                <select name="propietario" id="propietarios" class="form-control">

                    <option value="" selected disabled>--Seleccione--</option>
                    @foreach ($propietarios as $propietario)
                        <option value="{{$propietario->id}}">{{$propietario->propietario_nombres}} {{$propietario->propietario_apellidos}} ({{$propietario->propietario_cuit}})</option>
                    @endforeach

                </select>


            </div>


            @can('profesional.expediente.index')

            {{-- para tapar el espacio vacío que tiene el index del profesional --}}
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4"></div>

            <br> <br> 

            @endcan



            <br> <br> 


            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">

                <label for="">Tipologia</label>
                <select name="tipologia" id="tipologias" class="form-control">
                    <option value="" selected disabled>--Seleccione--</option>
                    @foreach ($tipologias as $tipologia)
                        <option value="{{$tipologia->tipologia_id}}">{{$tipologia->tipologia}}</option>
                    @endforeach

                </select>

            </div>






            
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">

                <label for="">Tarea Profesional</label>
                <select name="tipo_tarea" id="tipos_tareas" class="form-control">

                    <option value="" selected disabled>--Seleccione--</option>
                    @foreach ($tipos_tareas as $tipo_tarea)
                        <option value="{{$tipo_tarea->tipo_tarea_id}}">{{$tipo_tarea->tipo_tarea}}</option>
                    @endforeach

                </select>


            </div>
            



            


            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">

                <label for="">Localidad</label>
                <select name="localidad" id="localidades" class="form-control">

                    <option value="" selected disabled>--Seleccione--</option>
                    @foreach ($localidades as $localidad)
                        <option value="{{$localidad->localidad_id}}">{{$localidad->localidad}}</option>
                    @endforeach

                </select>


            </div>

            








    </div>




    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">

            <br> 
        
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">

                <label for="">Estado</label>
                <select name="estado" id="estados" class="form-control">

                    <option value="" selected disabled>--Seleccione--</option>
                    @foreach ($estados as $estado)
                        <option value="{{$estado->estado_id}}">{{$estado->estado}}</option>
                    @endforeach

                </select>


            </div>




            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">

                <label for="">Condicion</label>
                <select name="condicion" id="condiciones" class="form-control">

                    <option value="" selected disabled>--Seleccione--</option>
                    @foreach ($condiciones as $condicion)
                        <option value="{{$condicion->condicion_id}}">{{$condicion->condicion}}</option>
                    @endforeach

                </select>


            </div>



            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">

                <label for="">Objeto</label>
                <select name="objeto" id="objetos" class="form-control">

                    <option value="" selected disabled>--Seleccione--</option>
                    @foreach ($objetos as $objeto)
                        <option value="{{$objeto->objeto_id}}">{{$objeto->objeto}}</option>
                    @endforeach

                </select>

                <br><br>

            </div>







            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">

                <div class="form-group">
                    <label>Desde</label>
                    <input type="date" id="min" name="fecha1" value="" class="form-control">
                </div>

            </div>


            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                <div class="form-group">
                    <label>Hasta</label>
                    <input type="date" id="max" name="fecha2" value="" class="form-control">
                </div>
            </div>



            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">

                <br>

                <button type="button" class="btn btn-secondary " id="limpiar">
                    Limpiar 
                </button>

            

                <a href="{{route('expedientes')}}">
                    <button type="submit" class="btn btn-primary " id="filtrar" formaction="{{url('/expedientes/search', [
                        'expediente_numero' => $expediente_numero,
                        'profesional' => $profesional,
                        'profesional' => $profesional,
                        'propietario' => $propietario,
                        'tipologia' => $tipologia,
                        'tarea_profesional' => $tipo_tarea,
                        'localidad' => $localidad,
                        'estado' => $estado,
                        'condicion' => $condicion,
                        'objeto' => $objeto,
                        'fecha1' => $fecha1,
                        'fecha2' => $fecha2,
                    ]
                    )}}">
                        Filtrar 
                    </button>
                </a>



                {!!Form::close()!!}     


            </div>

    </div>

    <div class="row col-lg-6 col-md-6 col-sm-6 col-xs-6" name="filtros">

            <br>

            {{-- <div class="row col-lg-6 col-md-6 col-sm-6 col-xs-6" name="filtros"> --}}

            {{-- </div> --}}
            
        {{-- </div> --}}
    

    
    </div> {{-- row - filtros --}}


    

    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

            <div class="row">
                @if (session('success'))
                    <div class="alert alert-success" role="alert">
                        {{session('success')}}
                    </div>
                    
                @elseif (session('error'))
                    <div class="alert alert-danger" role="alert">
                        {{session('error')}}
                    </div>

                @else 
                @endif
            </div>

            <br> 

            <div class="table-responsive">
                <table class="table table-striped table-bordered  table-hover">
                    <thead>
                        {{-- <th>ID</th> --}}
                        <th class="col-lg-1 col-md-1 col-sm-1 col-xs-1 bg-primary text-center">NÚMERO <br> EXPEDIENTE</th>

                        @can('administrativo.&.supervisor.columnaProfesional.expediente.index')
                            <th class="col-lg-2 col-md-2 col-sm-2 col-xs-2 bg-primary text-center">PROFESIONAL</th>
                        @endcan

                        <th class="col-lg-1 col-md-1 col-sm-1 col-xs-1 bg-primary text-center">PROPIETARIO</th>
                        <th class="col-lg-1 col-md-1 col-sm-1 col-xs-1 bg-primary text-center">FECHA INICIO</th>
                        <th class="col-lg-1 col-md-1 col-sm-1 col-xs-1 bg-primary text-center">TIPOLOGÍA</th>
                        <th class="col-lg-1 col-md-1 col-sm-1 col-xs-1 bg-primary text-center">TAREAS</th>
                        <th class="col-lg-1 col-md-1 col-sm-1 col-xs-1 bg-primary text-center">PARTIDA/S INMOBILIARIA/S</th>
                        <th class="col-lg-2 col-md-2 col-sm-2 col-xs-2 bg-primary text-center">OPCIONES</th>
                        <th class="col-lg-2 col-md-2 col-sm-2 col-xs-2 bg-primary text-center">CONDICIÓN</th>
                        

                    </thead>


                    

                    @if ($currentUserRole === "administrativo" or $currentUserRole === "supervisor")

                        @forelse ($expedientes as $expt)

                            <tr>

                                {{-- <td>{{$expt->expediente_id}}</td> --}}
                                <td>{{$expt->expediente_numero}}</td>


                                @can('administrativo.&.supervisor.columnaProfesional.expediente.index')
                                    <td>{{$expt->profesional_nombres}} {{$expt->profesional_apellidos}}</td>
                                @endcan


                                <td>{{$expt->propietario_nombres}} {{$expt->propietario_apellidos}}</td>


                                <td>{{\Carbon\Carbon::createFromTimestamp(strtotime($expt->fecha_inicio))->format('d-m-Y  H:m:s')}}</td>

                                <td>{{$expt->tipologia}}</td>


                                <td>{{$expt->tipo_tarea}}</td>

                                <td>{{$expt->partida_inmobiliaria}}</td>


                                {{-- Botones --}}
                                <td>
                                    <a href="{{route('expedientes.show', ['id' => $expt->expediente_id])}}">
                                        <button  class="btn btn-success"> <i class="fa fa-eye"></i></button>
                                    </a>

                                    @if ($expt->condicion_id == config('app.formulario_a_revisar'))
                                        <a href="{{route('expedientes.edit', ['id' => $expt->expediente_id])}}">
                                            <button class="btn btn-info"> <i class="fa fa-pencil"></i></button>
                                        </a>    
                                    @endif
                                    

                                    @if ($expt->condicion_id >= config('app.cerrado'))
                                        {!! Form::open(array('url'=>'expedientes','method'=>'post','autocomplete'=>'off', 'files'=>true, 'style'=>'display:inline')) !!}
                                        {{ Form::token() }}
                                        {{-- @method('GET') --}}
                                        {{csrf_field()}}

                                            {{-- <div class="form-group row col-lg-12 col-sm-12 col-md-12 col-xs-12"> --}}
                                            <form method="post" enctype="multipart/form-data" 
                                            action="{{route('expedientes.descargarCertificado', ['id' => $expt->expediente_id])}}" class="d-inline">
                                                @csrf
                                            
                                                <a href="{{route('expedientes.descargarCertificado', ['id' => $expt->expediente_id])}}" {{-- data-target="#modal-DescargarComprobantePago{{$expediente->expediente_id}}" data-toggle="modal" --}}>
                                                    <button  type="submit" class="btn btn-danger d-inline" formaction="{{url('/expedientes/descargarCertificado', ['id' => $expt->expediente_id])}}">
                                                        <i class="fa fa-file-pdf-o"></i>
                                                    </button>
                                                </a>

                                                {{-- <br><br> --}}
                                            
                                            </form>
                                            {{-- </div> <!-- .form-group row -->      --}}
                                        
                                        {!!Form::close()!!}   
                                    @endif


                                








                                    {{-- <a href="" data-target="#modal-delete-{{$expt->expediente_id}}" data-toggle="modal"><button class="btn btn-danger">Eliminar</button></a> --}}
                                </td>  


                                <td>{{$expt->condicion_short}}</td>

                            <tr>


                        @empty 
                        
                            <h3><strong>Sin expedientes actualmente</strong></h3>

                        @endforelse




                    @else   {{-- currentUserRole == 'profesional' -> --}}


                        @forelse ($expedientesProfesional as $expt)
                                
                        <tr>
                            {{-- <td>{{$expt->expediente_id}}</td> --}}
                            <td>{{$expt->expediente_numero}}</td>


                            <td>{{$expt->propietario_nombres}} {{$expt->propietario_apellidos}}</td>


                            <td>{{$expt->fecha_inicio}}</td>


                            <td>{{$expt->tipologia}}</td>


                            <td>{{$expt->tipo_tarea}}</td>


                            <td>{{$expt->partida_inmobiliaria}}</td>


                            {{-- Botones --}}
                            <td>
                                <a href="{{route('expedientes.show', ['id' => $expt->expediente_id])}}">
                                    <button class="btn btn-success" title="Ver Expediente"> <i class="fa fa-eye"></i></button>
                                </a>


                                @if ($expt->condicion_id == config('app.formulario_a_revisar'))
                                    <a href="{{route('expedientes.edit', ['id' => $expt->expediente_id])}}">
                                        <button class="btn btn-info" title="Editar Expediente"> <i class="fa fa-pencil"></i></button>
                                    </a>     
                                @endif   
                                

                                @if ($expt->condicion_id >= config('app.cerrado') && $expt->condicion_id <= config('app.cerrado_con_dir_obra_finalizado'))

                                    {!! Form::open(array('url'=>'expedientes','method'=>'post','autocomplete'=>'off', 'files'=>true, 'style'=>'display:inline')) !!}
                                    {{ Form::token() }}
                                    {{-- @method('GET') --}}
                                    {{csrf_field()}}
                                    
                                        {{-- <div class="form-group row col-lg-12 col-sm-12 col-md-12 col-xs-12"> --}}
                                        <form method="post" enctype="multipart/form-data" action="{{route('expedientes.descargarCertificado', ['id' => $expt->expediente_id])}}">
                                            @csrf
                                            
                                            
                                            <a href="{{route('expedientes.descargarCertificado', ['id' => $expt->expediente_id])}}" {{-- data-target="#modal-DescargarComprobantePago{{$expediente->expediente_id}}" data-toggle="modal" --}}>
                                                <button  type="submit" class="btn btn-danger" formaction="{{url('/expedientes/descargarCertificado', ['id' => $expt->expediente_id])}}">
                                                    <i class="fa fa-file-pdf-o"></i>
                                                </button>
                                            </a>

                                            {{-- <br><br> --}}
                                        
                                        </form>
                                        {{-- </div> <!-- .form-group row -->      --}}
                                    
                                    {!!Form::close()!!}     

                                @endif   

                                
                                {{-- <a href="" data-target="#modal-delete-{{$expt->expediente_id}}" data-toggle="modal"><button class="btn btn-danger">Eliminar</button></a> --}}
                            </td>  

                            <td>{{$expt->condicion_short}}</td>

                        <tr>



                        @empty 
                        
                        <h3><strong>Usted no posee expedientes actualmente</strong></h3>


                        @endforelse


                    @endif


                            

                    </tr>
                    

                        {{-- @include('expediente.modal') --}}


                    
                </table>
            </div> {{-- table --}}


            {{-- <!-- para paginar --> --}}
            
            @if ($currentUserRole === "administrativo" or $currentUserRole === "supervisor")

                {{$expedientes->render()}} 

            @else

                {{$expedientesProfesional->render()}} 
            
            @endif


        </div> {{-- col --}}
    </div> {{-- row --}}


    




@endSection





@section('jsScripts')

{{-- <script>
            Swal.fire({
            title: 'Error!',
            text: 'Do you want to continue',
            icon: 'error',
            confirmButtonText: 'Cool'
        })


</script> --}}


@endSection








<!-- SELECT2 -->


@push('scripts')

<script>
    $('#expedientesNumeros').select2();        
    $('#profesionales').select2();        
    $('#propietarios').select2();        
    $('#tipologias').select2();        
    $('#tipos_tareas').select2();        
    $('#estados').select2();        
    $('#condiciones').select2();        
    $('#objetos').select2();        
    $('#localidades').select2();        
</script>


@endpush


