@extends ('layouts.admin')





{{-- @php
    dd($expediente->comprobante_pago_id);

    if ($expediente->comprobante_pago_id != null) {
        echo ('diferente');
    } else {
        echo ('null');
    }
@endphp
 --}}



{{--
    
        VISTA DE EXPEDIENTES SHOW

        PANTALLA IMPORTANTE QUE EXPONE LA INFORMACIÓN DE UN EXPEDIENTE EN PARTICULAR, Y
        EN LA CUAL APARECEN BOTONES PARA QUE LOS USUARIOS DE TODOS LOS ROLES PUEDAN LLEVAR A CABO LOS CASOS DE USO
        DE ACUERDO A LA CONDICIÓN DEL EXPEDIENTE
        
        La condición es el estado en los ciclos de vida que puede tener éste, tanto en el ideal (habitual)
        como en el que extiende a otros casos de uso (menos habitual).

        -> VARIABLES DE ENTRADA: 
    

    Para poder mostrar un expediente, esta vista recibe 
    
    - un expediente,
    - datosExtraExpediente
    


    Los profesionales pueden abrir expedientes exclusivamente para sí mismos



        -> SALIDA: 


        Expediente 



        -> HISTORIAS DE USUARIO RELACIONADAS: 


        - Crear Expediente

    
--}}
    





    

    {{-- {{dd($expediente);}} --}}
    {{-- {{dd($condiciones)}} --}}
    {{-- {{dd($historialCondicionesExpediente)}} --}}
    {{-- {{dd($dataExtraExpediente[0]->comprobantePago_nombreArchivo)}} --}}
    {{-- {{dd($dataExtraExpediente[0]);}} --}}
    {{-- {{dd($dataExtraExpediente);}} --}}
    {{-- {{ $test = $data[0]}} --}}
    {{-- {{dd(config('app.esperando_comprobante_1'));}} --}}
    {{-- {{dd($motivosFormularioIncompleto)}} --}}



@section('contenido')
    

    <link href="{{ asset('css/views/expedientes/show.css') }}" rel="stylesheet">

    {{-- {{dd("YEAH")}} --}}

    @livewire('dynamic-show', [ compact(
        'expediente', 
        'condicionesActuales',
        'motivosFormularioIncompleto', 
    ),
    
        ["dataExtraExpediente"  => $dataExtraExpediente/* [0] */, ]
        // 'user' => $user
    ])















@endSection











{{-- <script> 
    window.onload = asignarEstadoBotonValorarExpediente();
</script> --}} 
