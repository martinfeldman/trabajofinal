
@extends ('layouts.admin')















@section('contenido')




    <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12">
    
        @if (count($errors)>0)    
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li> {{$error}} </li>
                    @endforeach
                
                </ul>
            </div>
        @endif   
    
        <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12">
        
            <h2 class="col-lg-5 col-md-5 col-sm-5 col-xs-5"><strong>Informes</strong></h2> 
        
            <br>

        </div>


        <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12">

        {!! Form::open(array('url'=>'informes/generar','method'=>'post','autocomplete'=>'off', 'style'=>'display:inline')) !!}
        {{ Form::token() }}
        {{csrf_field()}}


            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="font-size: 115%">
            
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                    <br>

                    <label for="">Tipologia</label>
                    <select name="tipologia" id="tipologias" class="form-control">
                        <option value="" selected disabled>--Seleccione--</option>
                        @foreach ($tipologias as $tipologia)
                            <option value="{{$tipologia->tipologia_id}}">{{$tipologia->tipologia}}</option>
                        @endforeach

                    </select>

                </div>





                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">

                    <label class="text-center" for="">Condicion (Exclusivo de Informe Profesional con Más Expedientes)</label>
                    <select name="condicion" id="condiciones" class="form-control">

                        <option value="" selected disabled>--Seleccione--</option>
                        @foreach ($condiciones as $condicion)
                            <option value="{{$condicion->condicion_id}}">{{$condicion->condicion}}</option>
                        @endforeach

                    </select>


                </div>

            </div> {{-- col izquieda --}}


            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="font-size: 115%">

                <br>
                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">

                    <div class="form-group">
                        <label>Desde</label>
                        <input type="date" id="min" name="fecha1" value="" class="form-control">
                    </div>

                </div>


                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
                    <div class="form-group">
                        <label>Hasta</label>
                        <input type="date" id="max" name="fecha2" value="" class="form-control">
                    </div>
                </div>


                {{-- no necesita boton filtrar  --}}


                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                    <br>
                    <button type="button" class="btn btn-secondary btn-block" id="limpiar">
                        Limpiar 
                    </button>


                </div>


            </div> {{-- col derecha --}}

        </div> {{-- row --}}

    </div>


    <div class="row" style="min-height: 80vh;">

        <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <br> <br>

            <h3 class="col-lg-6 col-md-6 col-sm-6 col-xs-6">Generar Informe sobre Expedientes con Trámites inconclusos</h3>  <br>

        


                {{-- // la siguiente ruta está funcionando actualmente (27-02-2022) --}}
                <a href="{{route('informes.generarInformeExpedientesTramitesInconclusos')}}" style="font-size: 1.25em">
                    <button type="submit" class="btn btn-success col-lg-2 col-md-2 col-sm-2 col-xs-2" formaction="{{url('/informes/generarInformeExpedientesTramitesInconclusos')}}" style="font-size: 1.1em"> 
                        Generar
                    </button>
                </a> 

        
        </div>


        <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12">


            <h3 class="col-lg-6 col-md-6 col-sm-6 col-xs-6">Generar Informe sobre Profesionales con más Expedientes generados</h3> <br>

            <div>
                <a href="{{route('informes.generarInformeProfesionalesMasExpedientes')}}" style="font-size: 1.25em">
                    <button type="submit" class="btn btn-success col-lg-2 col-md-2 col-sm-2 col-xs-2" formaction="{{url('/informes/generarInformeProfesionalesMasExpedientes')}}"style="font-size: 1.1em"> 
                        Generar
                    </button>
                </a> 
            </div>

            <br> <br> <br> <br> <br> <br> <br> <br> {{-- new lines (br) para bajar la linea horizontal que sale en algunos modals actualmente: bootstrap v3.3.4  --}}


        </div>


        <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
            <h4 class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><strong> Estas acciones pueden requerir unos segundos para devolver el archivo</strong></h4>
        </div>



    </div>

@endsection









<!-- SELECT2 -->


@push('scripts')












<script>
    $('#tipologias').select2();        
    $('#condiciones').select2();        
</script>






@endpush