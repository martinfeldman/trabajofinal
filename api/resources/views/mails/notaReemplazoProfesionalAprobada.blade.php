<!DOCTYPE html>
<html lang="en">

<head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Document</title>
</head>


    {{-- {{dd($expediente)}} --}}

<body>

    <h1> Su nota de Reemplazo Profesional ha sido aprobada</h1> <br><br>

    <p>
        Estimado profesional,<br> <br>

        @if($nota->pendiente_cambios == 1) 

            Con efecto de la aprobación de la Nota de Reemplazo Profesional que presentó el día
            {{\Carbon\Carbon::createFromTimestamp(strtotime($nota->fecha))->format('d-m-Y')}},<br>
            y dado que al crear la nota marcó que piensa subir planos conformes a obra,<br>
            se ha creado el formulario Nº {{$expediente->expediente_numero}} con la condición de Incompleto,<br>
            el cual sigue el proceso habitual de los formularios y expedientes.<br><br><br>
        
        @else

            Con efecto de la aprobación de la Nota de Reemplazo Profesional que presentó el día
            {{\Carbon\Carbon::createFromTimestamp(strtotime($nota->fecha))->format('d-m-Y')}},<br>
            se ha creado un nuevo expediente para registrar la tarea correspondiente,<br>
            el expediente Nº {{$expediente->expediente_numero}}.<br>
            Dado que no marcó que piensa subir planos conformes a obra, el expediente está listo para liquidación.<br><br><br>
            
        @endif

    
        Gracias
        <br><br><br>
        
        
        <strong>Colegio de Arquitectos de la Provincia de Misiones</strong> <br> <br>

        Mensaje enviado de forma automatizada con el sistema de Expedientes del Colegio.
    
    </p>



</body>
</html>