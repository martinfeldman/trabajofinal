
@extends ('layouts.admin')
















    {{-- {{dd($auditorias)}} --}}
    {{-- {{dd($auditorias->first())}} --}}




@php

    use App\Models\User;
    use App\Models\Expediente;
    use App\Services\AuditoriaService;

    // esto lo hago aca porque con la implementacion actual (31-01 hacia atras), 
    // se esta recibiendo recien acá, en el index, la colección de auditorias.
    $auditorias = AuditoriaService::staticFormatearColumnas($auditorias);


@endphp





@section('contenido')





    <div class="container col-lg-12 col-md-12 col-sm-12 col-xs-12">


        <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12">

            <h2 class="col-lg-5 col-md-5 col-sm-5 col-xs-5"><strong>Auditorías</strong></h2>

        </div>


        <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12" name="filtros" style="font-size: 115%">

            {!! Form::open(array('url'=>'auditoria/search','method'=>'post','autocomplete'=>'off', 'style'=>'display:inline')) !!}
            {{ Form::token() }}
            {{csrf_field()}}

            <br> 
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">

                <label for="">Tabla</label>
                <select name="tabla" id="tabla" class="form-control">
                    <option value="" selected disabled>--Seleccione--</option>
                    <option value="User">Usuarios</option>
                    <option value="Expediente">Expedientes</option>
                    <option value="Profesional">Profesionales</option>
                    <option value="Propietario">Propietarios</option>
                    <option value="NotaReemplazoProfesional">Nota Reemplazo Profesional</option>
                    <option value="Tarea">Tarea</option>
                    <option value="ComprobantePago">Comprobante Pago</option>

                </select>

            </div>





            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">

                <label for="">Evento</label>
                <select name="evento" id="eventos" class="form-control">

                    <option value="" selected disabled>--Seleccione--</option>
                        <option value="created">Creación</option>
                        <option value="updated">Actualización</option>

                </select>


            </div>






            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">

                <label for="">Usuario</label>
                <select name="user_id" id="user" class="form-control">

                    <option value="" selected disabled>--Seleccione--</option>
                    @foreach ($users as $user)
                        <option value="{{$user->id}}">{{$user->name}}</option>
                    @endforeach

                </select>


            </div>



            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">

                <div class="form-group">
                    <label>Desde</label>
                    <input type="date" id="min" name="fecha1" value="" class="form-control">
                </div>

            </div>


            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                <div class="form-group">
                    <label>Hasta</label>
                    <input type="date" id="max" name="fecha2" value="" class="form-control">
                </div>
            </div>






            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">

                <div class="form-group">
                        {{ Form::submit('Filtrar', ["class" => "btn btn-primary btn-block"])}}
                </div>
            </div>


            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">

                <button type="button" class="btn btn-secondary btn-block" id="limpiar">
                    Limpiar 
                </button>
            </div>



        
        </div> {{-- row - filtros --}}






        <div class="row">

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            
                    @if (session('success'))

                        {{-- <div class="alert alert-success" role="alert">
                            {{session('success')}}
                        </div> --}}

                    @elseif (session('error'))

                        <div class="alert alert-danger" role="alert">
                            {{session('error')}}
                        </div>

                    @endif
                    
                <br> <br>
            </div>
        </div>



        <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12" name="tabla" style="min-height: 55vh;">

            <div class="table-responsive col-lg-10 col-md-10 col-sm-10 col-xs-10">

                <table class="table table-striped table-bordered table-hover" style="font-size: 120%">

                    <thead>

                        {{-- <th>ID</th> --}}


                        <th class="text-left">OBJETO</th>
                        <th class="text-left">EVENTO</th>
                        <th class="text-left">RESPONSABLE</th>
                        <th class="text-left">FECHA</th>
                        <th class="text-left">VER MÁS</th>



                    </thead>

                    @forelse ($auditorias as $auditoria)

                        <tr>

                            <td>{{substr($auditoria->auditable_type, 11)}}
                            
                                @if($auditoria->auditable_type === 'App\Models\Expediente')
                                    <span> Nº {{Expediente::findOrFail($auditoria->auditable_id)->expediente_numero}}</span> 
                                @endif
                            </td>

                            <td>

                                    @switch ($auditoria->event) 
                                        @case ('created')
                                            Creación
                                            @break;


                                        @case ('updated')
                                            Actualización
                                            @break;

                                    @endswitch
                            
                            </td>

                            <td>{{$auditoria->usuario}}</td>
                            <td>{{\Carbon\Carbon::createFromTimestamp(strtotime($auditoria->created_at))->format('d-m-Y  H:m:s')}}</td>


                            <td>
                                <a data-target="#modal-VerAuditoriaData-{{$auditoria->id}}" data-toggle="modal">
                                    <button  class="btn btn-success" title="Ver Info"> 
                                        <i class="fa fa-eye"></i>
                                    </button>
                                </a>

                                @include('auditoria.modal-VerAuditoriaData')
                            </td>



                        </tr>




                    @empty 

                        <h3><strong>Actualmente no hay registros de auditoría o no coinciden con los filtros especificados</strong></h3>

                    @endforelse 

                </table>

            </div> {{-- table --}}

        </div> {{-- row - tabla --}}


        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
            
                {{-- <h3> {{$auditorias->appends(Request::all())->links()}} </h3> --}}
            @php
                $link_limit = 10;
            @endphp

            @if ($auditorias->lastPage() > 1)
                <ul class="pagination">
                    <li class="{{ ($auditorias->currentPage() == 1) ? ' disabled' : '' }}">
                        <a href="{{ $auditorias->url(1) }}">Primero</a>
                    </li>
                    @for ($i = 1; $i <= $auditorias->lastPage(); $i++)
                        <?php
                        $half_total_links = floor($link_limit / 2);
                        $from = $auditorias->currentPage() - $half_total_links;
                        $to = $auditorias->currentPage() + $half_total_links;
                        if ($auditorias->currentPage() < $half_total_links) {
                            $to += $half_total_links - $auditorias->currentPage();
                        }
                        if ($auditorias->lastPage() - $auditorias->currentPage() < $half_total_links) {
                            $from -= $half_total_links - ($auditorias->lastPage() - $auditorias->currentPage()) - 1;
                        }
                        ?>
                        @if ($from < $i && $i < $to)
                            <li class="{{ ($auditorias->currentPage() == $i) ? ' active' : '' }}">
                                <a href="{{ $auditorias->url($i) }}">{{ $i }}</a>
                            </li>
                        @endif
                    @endfor
                    <li class="{{ ($auditorias->currentPage() == $auditorias->lastPage()) ? ' disabled' : '' }}">
                        <a href="{{ $auditorias->url($auditorias->lastPage()) }}">Último</a>
                    </li>
                </ul>
            @endif

            </div>


            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 text-center">

                @if ($auditorias->total() !== 0)
                    <h3> Mostrando {{$auditorias->perPage()}} de {{$auditorias->total()}} registros </h3>
                @else 
                    <h3> Mostrando 0 de 0 registros </h3>
                @endif
            </div>

        </div>




    </div> {{-- container --}}




    
@endsection









<!-- SELECT2 -->


@push('scripts')












<script>
    $('#tabla').select2();        
    $('#eventos').select2();        
    $('#user').select2();        
    $('#fecha1').select2();        
    $('#fecha2').select2();             
</script>






@endpush