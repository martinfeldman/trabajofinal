<?php

namespace App\Providers;

use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;


/* EVENTOS */
use App\Events\TestEvent;
use App\Events\CertificadoGeneradoEvent;
use App\Events\ComprobantePagoCargadoEvent;
use App\Events\ComprobantePagoDesestimadoEvent;
use App\Events\DocumentacionFormularioActualizadaEvent;
use App\Events\ExpedienteCerradoEvent;
use App\Events\ExpedienteLiquidadoEvent;
use App\Events\finalProcesoAutomatizado_cerrarExpedienteEvent;
use App\Events\NuevoFormularioExpedienteEvent;
use App\Events\PrimerRecordatorio_Profesional_subirComprobantePagoEvent;
use App\Events\RenunciaDireccionObraEvent;
use App\Events\SegundoRecordatorio_Profesional_subirComprobantePagoEvent;
use App\Events\SolicitudReemplazoProfesionalEvent;
use App\Events\ValoracionFormularioExpedienteEvent;
use App\Events\ValoracionNotaReemplazoProfesionalEvent;



/* LISTENERS */
use App\Listeners\TestListener;
use App\Listeners\CertificadoGeneradoListener;
use App\Listeners\ComprobantePagoCargadoListener;
use App\Listeners\ComprobantePagoDesestimadoListener;
use App\Listeners\DocumentacionFormularioActualizadaListener;
use App\Listeners\EmailVerificadoListener;
use App\Listeners\ExpedienteCerradoListener;
use App\Listeners\ExpedienteLiquidadoListener;
use App\Listeners\finalProcesoAutomatizado_cerrarExpedienteListener;
use App\Listeners\NuevoFormularioExpedienteListener;
use App\Listeners\PrimerRecordatorio_Profesional_subirComprobantePagoListener;
use App\Listeners\RenunciaDireccionObraListener;
use App\Listeners\SegundoRecordatorio_Profesional_subirComprobantePagoListener;
use App\Listeners\SolicitudReemplazoProfesionalListener;
use App\Listeners\ValoracionFormularioExpedienteListener;
use App\Listeners\ValoracionNotaReemplazoProfesionalListener;
use Illuminate\Auth\Events\Verified;

class EventServiceProvider extends ServiceProvider {

        /**
     * The event listener mappings for the application.
     *
     * @var array
     */

    protected $listen = [



        Registered::class => [
            SendEmailVerificationNotification::class,
        ],


        TestEvent::class => [
            TestListener::class,
        ],

        CertificadoGeneradoEvent::class => [
            CertificadoGeneradoListener::class,
        ],


        ComprobantePagoCargadoEvent::class => [
            ComprobantePagoCargadoListener::class,
        ],


        ComprobantePagoDesestimadoEvent::class => [
            ComprobantePagoDesestimadoListener::class,
        ],


        DocumentacionFormularioActualizadaEvent::class => [
            DocumentacionFormularioActualizadaListener::class,
        ],


        ExpedienteCerradoEvent::class => [
            ExpedienteCerradoListener::class,
        ],


        ExpedienteLiquidadoEvent::class => [
            ExpedienteLiquidadoListener::class,
        ],


        NuevoFormularioExpedienteEvent::class => [
            NuevoFormularioExpedienteListener::class,
        ],


        Registered::class => [
            SendEmailVerificationNotification::class,
        ],


        RenunciaDireccionObraEvent::class => [
            RenunciaDireccionObraListener::class,
        ],


        SolicitudReemplazoProfesionalEvent::class => [
            SolicitudReemplazoProfesionalListener::class,
        ],


        ValoracionFormularioExpedienteEvent::class => [
            ValoracionFormularioExpedienteListener::class,
        ],


        ValoracionNotaReemplazoProfesionalEvent::class => [
            ValoracionNotaReemplazoProfesionalListener::class,
        ],


        PrimerRecordatorio_Profesional_subirComprobantePagoEvent::class => [
            PrimerRecordatorio_Profesional_subirComprobantePagoListener::class,
        ],

        
        SegundoRecordatorio_Profesional_subirComprobantePagoEvent::class => [
            SegundoRecordatorio_Profesional_subirComprobantePagoListener::class,
        ],


        finalProcesoAutomatizado_cerrarExpedienteEvent::class => [
            finalProcesoAutomatizado_cerrarExpedienteListener::class,
        ],


        Verified::class => [
            EmailVerificadoListener::class,
        ],


    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
