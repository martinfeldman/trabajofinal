<?php

namespace App\Jobs;

use App\Events\finalProcesoAutomatizado_cerrarExpedienteEvent;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;


use App\Models\Expediente;
use App\Models\HistorialCondicion;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;









class finalProcesoAutomatizado_cerrarExpediente implements ShouldQueue {
    
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $expediente;



    /**
     * Create a new job instance.
     *
     * @return void
     */

    public function __construct(Expediente $expediente,) {

        $this->expediente = $expediente;

    }






    
    /**
    * Execute the job.
    *
    * @return void
    */

    public function handle() {


        $expediente = $this->expediente;

        if ($expediente->condicion_id >= config('app.esperando_comprobante_1') && $expediente->condicion_id <= config('app.esperando_comprobante_3')){


            try {
            
                DB::beginTransaction();


                $expediente->update([
                    'estado_id'                 => config('app.tramite_inconcluso'),
                    'condicion_id'              => config('app.cerrado_por_falta_comprobante_pago'),
                    'prioridad_administracion'  => 5,
                    'prioridad_profesional'     => 5,
                ]);

                
                
                // código similar a $expedienteService->actualizarHistorialCondicionExpediente($expediente);
                
                $fecha = Carbon::now()->setTimeZone('America/Argentina/Buenos_Aires')->format('Y-m-d H:i:s');

                $nuevoRegistroHistorialCondicion = HistorialCondicion::create([

                    'expediente_id' => $expediente->expediente_id,
                    'condicion_actual_id' => config('app.cerrado_por_falta_comprobante_pago'),
                    'fecha' => $fecha,
    
                ]);

    

                // PARA NOTIFICAR

                $evento = event (new finalProcesoAutomatizado_cerrarExpedienteEvent($expediente));
        


                DB::commit();

                return 0 ;
    
    
            } catch (\Exception $e) {
                DB::rollback();
                throw $e;
            } catch (\Throwable $e) {
                DB::rollback();
                throw $e;
            }
        

            
        } else {
            return ("Trabajo para Cerrar Expediente por falta de Comprobante de Pago Cancelado");
        }
    }



}
