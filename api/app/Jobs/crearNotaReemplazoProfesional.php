<?php

namespace App\Jobs;

use App\Http\Requests\NotaReemplazoProfesionalFormRequest;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;


use App\Models\Expediente;
use App\Models\NotaReemplazoProfesional;
use App\Repositories\NRPQueries;
use App\Repositories\PropietarioQueries;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

use Barryvdh\DomPDF\Facade as PDF;



class crearNotaReemplazoProfesional implements ShouldQueue {

    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;


    protected $request;
    protected $propietarioQueries;
    protected $nrpQueries;



    /**
     * Create a new job instance.
     *
     * @return void
     */

    public function __construct($request) {

        $this->request = $request;
        $this->propietarioQueries = new PropietarioQueries();
        $this->nrpQueries = new NRPQueries($this->propietarioQueries);
        
    }




    
    
    /**
     * Execute the job.
     *
     * @return void
     */

    public function handle() {
        
        $request = $this->request;
            
        /* dd($request); */

        $expediente_afectado_id = $this->nrpQueries->getExpedienteAfectadoID($request);

        /* dd($expediente_afectado_id); */


        if ($expediente_afectado_id->isEmpty()) {
            
            return Redirect()->back()->with('error', 'Los datos ingresados no coincidieron con los de algún expediente');
        }
        

        

        $expediente_afectado_numero = (Expediente::findOrFail($expediente_afectado_id[0]->expediente_id))->expediente_numero;
        


        if ($request->input('pendiente_cambios_checkbox') === 'on'){
            $pendiente_cambios = true;
        } else {
            $pendiente_cambios = false;
        }


        try{

            DB::beginTransaction();
            
            $fecha = Carbon::now()->setTimeZone('America/Argentina/Buenos_Aires')->format('Y-m-d H:i:s');
            


            $nota = NotaReemplazoProfesional::create ([

                'profesional_entrante_id'       => Auth()->user()->profesional_id,
                'profesional_saliente_id'       => $request->input('profesional_id'),
                'expediente_afectado_id'        => $expediente_afectado_id[0]->expediente_id,
                'expediente_afectado_numero'    => $expediente_afectado_numero,
                'nota_aprobada'                 => 0,
                'condicion_nrp_id'              => config('app.nrp_faltan_firmas'),
                'fecha'                         => $fecha,
                'observaciones'                 => "",
                'avance_obra'                   => $request->input('avance_obra'),
                'pendiente_cambios'             => $pendiente_cambios,
                'alta'                          => 1
            ]);




            $rutaDestino = 'public/PruebasAvance';


            $archivo1 = $request->file('pruebaAvance1');
            $archivo2 = $request->file('pruebaAvance2');

            $nombreArchivo1 = "Prueba Avance 1 NRP Nº " .  $nota->nota_reemplazo_profesional_id . " - " . $archivo1->getClientOriginalName();
            $nombreArchivo2 = "Prueba Avance 2 NRP Nº " .  $nota->nota_reemplazo_profesional_id . " - " . $archivo2->getClientOriginalName();

            $ruta = $archivo1->storeAs($rutaDestino, $nombreArchivo1);
            $ruta = $archivo2->storeAs($rutaDestino, $nombreArchivo2);



            $nota->update([
                'prueba_avance_1' => $nombreArchivo1, 
                'prueba_avance_2' => $nombreArchivo2, 
            ]);




            $dataExtraNota = $this->nrpQueries->getDataExtraNRP($nota, $request);



            $pdfNota = PDF::loadView('pdfs.notaReemplazoProfesional', ([

                'profesional_entrante_nombreCompleto'       => $dataExtraNota['profesional_entrante_nombreCompleto'],
                'profesional_entrante_numero_matricula'     => $dataExtraNota['profesional_entrante_numero_matricula'],
                'profesional_saliente_nombreCompleto'       => $dataExtraNota['profesional_saliente_nombreCompleto'],
                'profesional_saliente_numero_matricula'     => $dataExtraNota['profesional_saliente_numero_matricula'],
                'propietario_nombreCompleto'                => $dataExtraNota['propietario_nombreCompleto'],
                'propietario_dni'                           => $dataExtraNota['propietario_dni'],
                'obra'                                      => $dataExtraNota['obra']

            ]),

                compact ('nota', 'request' /* , 'logo' */));




            //ACA GUARDO EN STORAGE EL PDFNOTA Y AGREGO AL OBJETO NOTA

            
            
            $nombrePdfNota = "Nota Reemplazo Profesional Nº " .  $nota->nota_reemplazo_profesional_id . ".pdf";
            
            /* dd($nombrePdfNota); */

            $nota->fill(array (
                /* 'nota_reemplazo_profesional'    => $pdfNota,  */
                'nombre_archivo'                => $nombrePdfNota,
            ));
            

            $nota->save();

            Storage::put('public/NotasReemplazoProfesional/' . $nombrePdfNota, $pdfNota->output());
            
            /* return $pdfNota->stream(); */
            
            /* dd("todo okey"); */
            


            DB::commit();



        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        } catch (\Throwable $e) {
            DB::rollback();
            throw $e;
        }
    
    

        return Redirect()->back()->with('success', 'Nota de Reemplazo Profesional ha sido creada. Puede observar que la condición ha cambiado');




    }   
}