<?php

namespace App\Jobs;


use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;


use App\Models\Expediente;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;





class borrarJobsRecordatorios implements ShouldQueue {

    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;


    protected $expediente;



    /**
     * Create a new job instance.
     *
     * @return void
     */

    public function __construct(Expediente $expediente) {

        $this->expediente = $expediente;
        
    }




    
    
    /**
     * Execute the job.
     *
     * @return void
     */

    public function handle() {
        

        $expediente = $this->expediente;


        $jobs = DB::table('jobs')->get();



        if($jobs){

            foreach ($jobs as $job) {

                $jsonpayload = json_decode($job->payload);

                if ($jsonpayload->displayName === 'App\Jobs\enviarPrimerRecordatorio_Profesional_subirComprobantePagoMail'
                    || $jsonpayload->displayName === 'App\Jobs\enviarSegundoRecordatorio_Profesional_subirComprobantePagoMail'){

                    $command = addslashes($jsonpayload->data->command);

                    $newCommandString = str_replace('\"', '', $command);

                    if(Str::contains($newCommandString, ("expt_id;i:".$expediente->expediente_id))){

                        
                        DB::table('jobs')->delete($job->id);

                    }


                }


                if ($jsonpayload->displayName === 'App\Jobs\finalProcesoAutomatizado_cerrarExpediente'){

                    $command = addslashes($jsonpayload->data->command);

                    $newCommandString = str_replace('\"', '', $command);

                    if(Str::contains($newCommandString, ("id;i:".$expediente->expediente_id))){

                        
                        DB::table('jobs')->delete($job->id);

                    }


                }

            }

        }


        


    }




}
