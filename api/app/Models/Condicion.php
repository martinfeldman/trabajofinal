<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;













class Condicion extends Model implements Auditable {

    use HasFactory;
    use \OwenIt\Auditing\Auditable;


    protected $table='condiciones';

    protected $primaryKey='condicion_id';

    public $timestamps=false;







    protected $filelable=[
        'condicion',
        'condicion_short',
        'fecha',

        // fks
        'expediente_id',
    ];





    protected $guarded = [
        
    ];





    



    // Relaciones:


    public function expedientes(){
        return $this->hasMany('App\Models\Expediente', 'expediente_id', 'expediente_id');
    }


}
 


