<?php

namespace App\Models;



use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use OwenIt\Auditing\Contracts\Auditable;




class Expediente extends Model implements Auditable {

    use HasFactory;
    use \OwenIt\Auditing\Auditable;

    protected $table='expedientes';

    protected $primaryKey='expediente_id';

    public $timestamps=false;




    protected $filelable=[
        'expediente_numero', // 1
        'profesional_id',    // 1
        'estado_id',         // 1

        'tarea_id',          //pueden ser muchas
        'obra_id',           // 1
        'tipologia_id',      // 1
        'objeto_id',         // 1
        

        'superficie_a_construir',   // 1
        'superficie_con_permiso',   // 1 
        'superficie_sin_permiso',   // 1


        'fecha_inicio',             
        'fecha_cierre',   

        'liquidacion',              // 1
        'comprobante_pago_id',      // 1
        
        'condicion_id'              // 1
    ];

    protected $guarded = [
        
    ];







    // Relaciones:



    public function profesional(){
        return $this->belongsTo('App\Models\Profesional');
    }



    public function obra(){
        return $this->hasOne('App\Models\Obra', 'obra_id', 'obra_id');
    }



    public function estado(){
        return $this->hasOne('App\Models\Estado', 'estado_id', 'estado_id');
    }



    public function objeto(){
        return $this->hasOne('App\Models\Objeto', 'objeto_id', 'objeto_id');
    }



    public function tipologia(){
        return $this->hasOne('App\Models\Tipologia', 'tipologia_id', 'tipologia_id');
    }



    public function tareas(){
        return $this->hasMany('App\Models\Tarea', 'tarea_id', 'tarea_id');
    }



    public function condicion(){
        return $this->belongsTo('App\Models\Condicion', 'condicion_id', 'condicion_id');
    }



    public function historial_condiciones(){
        // return $this->hasMany('App\Models\HistorialCondicion',  'id', 'historial_condicion_id'); original que no anda
        return $this->hasMany('App\Models\HistorialCondicion',  'id'/* , 'historial_condicion_id' */);
    }



    public function comprabantePago(){
        return $this->hasOne('App\Models\ComprobantePago', 'comprobante_pago_id', 'comprobante_pago_id');
    }



    public function certificado(){
        return $this->belongsTo('App\Models\Certificado', 'certificado_id', 'certificado_id');
    }



    public function notaReemplazoProfesional(){
        return $this->belongsTo('App\Models\NotaReemplazoProfesional', 'nota_reemplazo_profesional_id', 'nota_reemplazo_profesional_id');
    }



 




}
 


