<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use OwenIt\Auditing\Contracts\Auditable;

use Illuminate\Support\Collection;

use Illuminate\Support\Facades\DB;












class Profesional extends Model implements Auditable {

    use HasFactory, Notifiable;
    use \OwenIt\Auditing\Auditable;

    private Collection $propietarios;

    
    


    protected $table='profesionales';

    protected $primaryKey='id';

    public $timestamps=false;





    protected $filelable=[
        'profesional_nombre',
        'profesional_apellido',
        'profesional_numero_matricula',
        'profesional_cuit',
        'fecha_registro',
        'alta',

        // fks
        'expediente_id',
        'propietario_id',
        

    ];




    protected $guarded = [
        
    ];





    

    // Relaciones:

    public function propietarios(){
        return $this->belongsToMany('App\Models\Propietario', 'profesional_propietario', 'profesional_id', 'propietario_id')/* ->withDefault() */;
    }




    public function expedientes(){
        return $this->hasMany('App\Models\Expediente');
    }


    

    public function nota_reemplazo_profesional(){
        return $this->hasMany('App\Models\NotaReemplazoProfesional', 'nota_reemplazo_profesional_id', 'profesional_entrante_id');
    }






    // Métodos propios
    
    public function agregarPropietario($id){

        // NO SE UTILIZA ACTUALMENTE : (02/01/2022) 

        DB::beginTransaction();

        try {

            $propietario = Propietario::findOrFail($id);

            /* $propietario->obras; */

            $this->propietarios()->add($propietario);

            $this->save();

            DB::commit();

        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        } catch (\Throwable $e) {
            DB::rollback();
            throw $e;

        }




    }




    public function quitarPropietario($id){

        $propietario = Propietario::findOrFail($id);
        $this->propietarios()->add($propietario);

    }






    public function getPropietarios() {
        return $this->propietarios;
    }




    // HASTA ACÁ



    public function getFullName(){
        return $this->profesional_nombres . " " . $this->profesional_apellidos;
    }



}
 


