<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;













class Certificado extends Model implements Auditable {
    

    use HasFactory;
    use \OwenIt\Auditing\Auditable;


    protected $table = 'certificados';

    protected $primaryKey = 'certificado_id';

    public $timestamps = false;





    protected $filelable = [
        'certificado',
        'expediente_id',
        'codigo_qr',
        'random_number',
        'fecha',

        // fks
        'expediente_id',
    ];




    protected $guarded = [];






    // Relaciones: 

    public function expediente()
    {
        return $this->hasOne('App\Models\Expediente', 'expediente_id', 'expediente_id');
    }
}
