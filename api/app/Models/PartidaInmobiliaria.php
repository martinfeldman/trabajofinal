<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;















class PartidaInmobiliaria extends Model implements Auditable {


    use HasFactory;
    use \OwenIt\Auditing\Auditable;


    protected $table='partidas_inmobiliarias';

    protected $primaryKey='partida_inmobiliaria_id';

    public $timestamps=false;







    protected $filelable=[
        'partida_inmobiliaria_numero',

        // fks
        'obra_id',
    ];






    protected $guarded = [
        
    ];






    // Relaciones:


    public function obra(){
        return $this->belongsTo('App\Models\Obra');
    }


}
