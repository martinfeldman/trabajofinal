<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
















class CondicionNRP extends Model implements Auditable {
    
    
    use HasFactory;
    use \OwenIt\Auditing\Auditable;


    protected $table='condiciones_nota_reemplazo_profesional';

    protected $primaryKey='condicion_nota_reemplazo_profesional_id';

    public $timestamps=false;






    protected $filelable=[
        
        'condicion_nota_reemplazo_profesional',

        // fks
        'nota_reemplazo_profesional_id',
    ];





    protected $guarded = [
        
    ];





    // Relaciones:


    public function notasReemplazoProfesional(){
        return $this->belongsToMany('App\Models\NotaReemplazoProfesional');
    }
}
 


