<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

















class ComprobantePago extends Model implements Auditable {
    
    
    use HasFactory;
    use \OwenIt\Auditing\Auditable;


    protected $table='comprobantes_pago';

    protected $primaryKey='comprobante_pago_id';

    public $timestamps=false;






    protected $filelable=[
        'comprabante_pago',
        'nombre_archivo',
        'fecha',

        // fks
        'expediente_id',
    ];



    


    protected $guarded = [
        
    ];





    // Relaciones:


    public function expediente(){
        return $this->belongsTo('App\Models\Expediente', 'expediente_id', 'expediente_id');
    }


}
