<?php


namespace App\Models;


use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

use Spatie\Permission\Traits\HasRoles;

use OwenIt\Auditing\Contracts\Auditable;







class User extends Authenticatable implements Auditable, MustVerifyEmail {

    use HasApiTokens, HasFactory, Notifiable, HasRoles;
    use \OwenIt\Auditing\Auditable;

    protected $table = 'users';
    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */

    protected $fillable = [
        'name',
        'last_name',
        'email',
        'password',
        'cuit',
        'numero_matricula',
        'profesional_id',
        'propietario_id',

    ];




    
    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */

    protected $hidden = [
        'password',
        'remember_token',
    ];





    /**
     * The attributes that should be cast.
     *
     * @var array
     */

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];





    protected $auditExclude = [
        'remember_token',
    ];


/* 
    // Relaciones:


    PONER ESTO ME DIO UN ERROR POR VARIOS DIAS 

    public function notifications(){
        return $this->hasMany('App\Models\Notification', 'id', 'notification_id');
    }


    BORRAR MENSAJE DOS COMMITS ADELANTE 26-01

 */


}
