<?php

namespace App\Listeners;

use App\Events\CertificadoGeneradoEvent;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\Notification;
use App\Events\ExpedienteCerradoEvent;
use App\Jobs\generarCertificado;
use App\Models\User;
use App\Models\Expediente;
use App\Models\Estado;
use App\Notifications\ExpedienteCerradoNotification;

use App\Services\ExpedienteService;
use Illuminate\Database\Eloquent\Collection;









class ExpedienteCerradoListener {

    use Dispatchable, InteractsWithSockets, SerializesModels;


    public $expediente;
    public $dataExtraExpediente;
    public string $userName;
    public $expedienteService; 




    
    /** 
     * Create the event listener.
     *
     * @return void
     */

    public function __construct(Expediente $expediente, Collection $dataExtraExpediente, ExpedienteService $expedienteService) {
        
        $this->expediente = $expediente;
        $this->dataExtraExpediente = $dataExtraExpediente;
        /* $this->userName = $userName; */
        $this->expedienteService = $expedienteService;
    
    }




    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */

    public function handle(ExpedienteCerradoEvent $event){
        
        
        $expediente = Expediente::findOrFail($event->expediente->expediente_id);
        
        if ($expediente->condicion_id == config('app.para_cerrar')){

            
            try {
            
                DB::beginTransaction();
    
                
                
                // ACA FALTA UN IF QUE CONDICION DE EXPEDIENTE CERRADO DEBE ASIGNARSE
                
                
                $expediente->update([
                    'estado_id'                 => config('app.estado_cerrado'),
                    'condicion_id'              => config('app.cerrado'),
                    'prioridad_administracion'  => 4,
                    'prioridad_profesional'     => 4,
                ]);


                $this->expedienteService->actualizarHistorialCondicionExpediente($event->expediente);




        

                $certificadoRetorno = generarCertificado::dispatch($expediente, $event->dataExtraExpediente);



                // NOTIFICACION

                $user = User::where('profesional_id', "=", $expediente->profesional_id)->get();

                $emisor = Auth()->user()->name;

                Notification::send($user, new ExpedienteCerradoNotification($event->expediente, $emisor));
    


                DB::commit();

                return 0;


            } catch (\Exception $e) {
                DB::rollback();
                throw $e;
            } catch (\Throwable $e) {
                DB::rollback();
                throw $e;
            }


        } 
        
    }


}
