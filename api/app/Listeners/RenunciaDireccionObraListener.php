<?php

namespace App\Listeners;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;



use Illuminate\Support\Facades\Notification;
use App\Events\RenunciaDireccionObraEvent;
use App\Models\User;
use App\Notifications\RenunciaDireccionObraNotification;






class RenunciaDireccionObraListener {

    public $expediente;

    use Dispatchable, InteractsWithSockets, SerializesModels;





    /** 
     * Create the event listener.
     *
     * @return void
     */

    public function __construct() {
        //
    }




    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */

    public function handle(RenunciaDireccionObraEvent $event){
        

        $users = User::role('supervisor')->get();

        $emisor = Auth()->user()->name;

        Notification::send($users, new RenunciaDireccionObraNotification($event->expediente, $emisor));
            
    }


}
