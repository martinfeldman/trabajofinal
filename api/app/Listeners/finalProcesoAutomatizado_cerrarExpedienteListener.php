<?php

namespace App\Listeners;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;


use App\Events\finalProcesoAutomatizado_cerrarExpedienteEvent;
use App\Notifications\finalProcesoAutomatizado_cerrarExpedienteNotification;
use Illuminate\Support\Facades\Notification;
use App\Models\User;








class finalProcesoAutomatizado_cerrarExpedienteListener {

    use Dispatchable, InteractsWithSockets, SerializesModels;


    /**
     * Create a new event instance.
     *
     * @return void
     */

    public function __construct(){
        //
    }




    
    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */

    public function handle(finalProcesoAutomatizado_cerrarExpedienteEvent $event){


        // NOTIFICACION 

        $user = User::where('profesional_id', "=", $event->expediente->profesional_id)->get()->first();

        Notification::send($user, new finalProcesoAutomatizado_cerrarExpedienteNotification($event->expediente));

    }

}
