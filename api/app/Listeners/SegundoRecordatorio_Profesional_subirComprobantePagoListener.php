<?php

namespace App\Listeners;

/* use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast; */
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;


use Illuminate\Support\Facades\Notification;
use App\Events\SegundoRecordatorio_Profesional_subirComprobantePagoEvent;
use App\Models\User;
use App\Notifications\SegundoRecordatorio_Profesional_subirComprobantePagoNotification;








class SegundoRecordatorio_Profesional_subirComprobantePagoListener {

    use Dispatchable, InteractsWithSockets, SerializesModels;


    /**
     * Create a new event instance.
     *
     * @return void
     */

    public function __construct(){
        //
    }




    
    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */

    public function handle(SegundoRecordatorio_Profesional_subirComprobantePagoEvent $event){


        // NOTIFICACION 

        $user = User::where('profesional_id', "=", $event->expediente->profesional_id)->get()->first();

        Notification::send($user, new SegundoRecordatorio_Profesional_subirComprobantePagoNotification($event->expediente));

    }

}
