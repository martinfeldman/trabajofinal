<?php

namespace App\Listeners;

/* use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast; */
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;


use Illuminate\Support\Facades\Notification;
use App\Notifications\PrimerRecordatorio_Profesional_subirComprobantePagoNotification;
use App\Events\PrimerRecordatorio_Profesional_subirComprobantePagoEvent;
use App\Models\User;










class PrimerRecordatorio_Profesional_subirComprobantePagoListener {

    use Dispatchable, InteractsWithSockets, SerializesModels;


    


    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(){
        //
    }




    
    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */

    public function handle(PrimerRecordatorio_Profesional_subirComprobantePagoEvent $event){
        

        // NOTIFICACION AL USUARIO

        $user = User::where('profesional_id', "=", $event->expediente->profesional_id)->get()->first();
        
        Notification::send($user, new PrimerRecordatorio_Profesional_subirComprobantePagoNotification($event->expediente));


    }

}
