<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;


use Illuminate\Notifications\Notification;
use App\Models\Expediente;
use App\Models\Profesional;
use Carbon\Carbon;




class NuevoUsuarioNotification extends Notification {

    use Queueable;

    public $user;




    /**
     * Create a new notification instance.
     *
     * @return void
     */

    public function __construct($user)
    {
        $this->user = $user;
    }




    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */

    public function via($notifiable){
        return ['database'];
    }





    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */

    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }






    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */

    public function toArray($notifiable){

        $fecha = Carbon::now()->setTimeZone('America/Argentina/Buenos_Aires')->format('d-m-Y H:i:s');

        return [

            'emisor'   => $this->user->name." ".$this->user->last_name,
            'usuario' => $this->user,
            'asunto' => "Nuevo usuario ha completado el formulario de Registro",
            'fecha' => $fecha,
            
        ];
    }










}
