<?php

namespace App\Notifications;

use App\Models\Configuracion;
use App\Models\Expediente;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;


use Carbon\Carbon;







class SegundoRecordatorio_Profesional_subirComprobantePagoNotification extends Notification implements ShouldQueue {

    use Queueable;

    public $expediente;




    /**
     * Create a new notification instance.
     *
     * @return void
     */

    public function __construct(Expediente $expediente){

        $this->expediente = $expediente;
    }




    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */

    public function via($notifiable) {
        return ['database'];
    }




    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->line('The introduction to the notification.')
            ->action('Notification Action', url('/'))
            ->line('Thank you for using our application!');
    }





    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */

    public function toArray($notifiable)
    {

        $fecha = Carbon::now()->setTimeZone('America/Argentina/Buenos_Aires')->format('d-m-Y H:i:s');
        $emisor = Configuracion::get()->first()->nombre_institucion;

        return [

            'emisor' => $emisor,
            'expediente_id' => $this->expediente->expediente_id,
            'expediente_numero' => $this->expediente->expediente_numero,
            'asunto' => "Le recordamos que su Expediente Nº " . $this->expediente->expediente_numero .
                " adeuda el Comprobante de Pago de la Liquidación para proceder a ser cerrado",
            'fecha' => $fecha,

        ];
    }
}
