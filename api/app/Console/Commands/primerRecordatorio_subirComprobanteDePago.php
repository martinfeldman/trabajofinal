<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class primerRecordatorio_subirComprobanteDePago extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'recordatorio:primerRecordatorio_subirComprobanteDePago';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Primer recordatorio enviado a profesional de expediente
     de subir Comprobante de Pago';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        return Command::SUCCESS;
    }
}
