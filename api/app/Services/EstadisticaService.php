<?php


namespace App\Services;

use App\Models\Auditoria;
use App\Models\Profesional;
use App\Models\Tarea;
use App\Repositories\EstadisticaQueries;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;








class EstadisticaService {



    public $estadisticaQueries;



    public function __construct(EstadisticaQueries $estadisticaQueries) {

        $this->estadisticaQueries = $estadisticaQueries;

    }






    public function getDataForGraficoTiposTareasMasRealizadas($request){

        // 1) CREAMOS UNA COLECCION EN LA QUE INCLUIREMOS TODA LA DATA NECESARIA, 
        // PERO TIENE PROPOSITOS DE DEBUGGING UNICAMENTE. YA QUE AL FINAL CREAMOS EL ARRAY QUE 
        // SE ENVIARA A LA LISTA     
        $data = collect();


        // 2) OBTENEMOS UNA COLECCION DE LOS TIPOS DE TAREAS
        $tipos_tareas = DB::table('tipos_tareas')
        ->select('tipos_tareas.*')
        ->orderBy('tipo_tarea_id', 'asc')
        ->get();

        /* dd($tipos_tareas); */

        $data = $data->push($tipos_tareas->toArray());

        
        
        // 3) OBTENEMOS EL TOTAL DE TAREAS REGISTRADAS EN UNA TIEMPO DETERMINADO SI SE RECIBIO UN FILTRO DE FECHA

        $localidad  = $request->input('localidad');
        $fecha1 = $request->input('fecha1');
        $fecha2 = $request->input('fecha2');


        
        $total_tareas = DB::table('tareas')

        /* ->join('expedientes', 'tareas.tarea_id', '=', 'expedientes.tarea_id')

        ->when($fecha1, function ($query, $fecha1) {
            return $query->where('expedientes.fecha_inicio', '>=' , $fecha1);
        })

        ->when($fecha2, function ($query, $fecha2) {
            return $query->where('expedientes.fecha_inicio', '<=' , $fecha2);
        }) */

        ->count();


       /*  dd($total_tareas); */


        // salimos del método en caso de que no existan tareas, porque lanza error al entrar al index
        if ($total_tareas == 0){
            return "Sin tareas actualmente";
        }


    

        /* dd($fecha1, $fecha2); */


        // 4) CREAMOS UN ARRAY PARA GUARDAR EL TOTAL DE TAREAS POR TIPO DE TAREA
        // Y OTRO ARRAY PARA GUARDAR LOS PORCENTAJES. 
        // MAS ALLÁ DE QUE SÓLO ES NECESARIO UNN ARRAY, SE IMPLEMENTAN DOS PARA FACILITAR EL DEBUGGING 

        $valores = array();
        $porcentajes = array();

        $i = 0; 

        foreach($tipos_tareas as $tipo_tarea){
            
            /* dd($tipo_tarea->tipo_tarea); */

            $total_por_tipo_tarea = DB::table('tipos_tareas')
            ->join('tareas', 'tipos_tareas.tipo_tarea_id', '=', 'tareas.tipo_tarea_id')
            ->join('expedientes', 'tareas.tarea_id', '=', 'expedientes.tarea_id')
            ->join('obras', 'expedientes.obra_id', '=', 'obras.obra_id')

            ->select('tareas.tipo_tarea_id')
            ->where('tipos_tareas.tipo_tarea_id', '=', $tipo_tarea->tipo_tarea_id)


            ->when($localidad, function ($query, $localidad) {
                return $query->where('obras.localidad_id', '>=' , $localidad);
            })

            ->when($fecha1, function ($query, $fecha1) {
                return $query->where('expedientes.fecha_inicio', '>=' , $fecha1);
            })

            ->when($fecha2, function ($query, $fecha2) {
                return $query->where('expedientes.fecha_inicio', '<=' , $fecha2);
            })

            ->orderBy('tipo_tarea_id', 'desc')
            ->count();
            
            


            $valores[$i] = $total_por_tipo_tarea;
            $porcentajes[$i] = ( $total_por_tipo_tarea * 100) / $total_tareas; 

            $i++;
        }



        /* dd($porcentajes); */



        $data = $data->push($porcentajes);
        $data = $data->push($valores);


        // 5) CREAMOS EL ARRAY QUE LE PASAREMOS A LA VISTA

        $puntos = [];
        $i= 0;

        foreach ($tipos_tareas as $tipo_tarea){

            $puntos[] = [
                'name' => $tipo_tarea->tipo_tarea,
                'y' => floatval($porcentajes[$i])

            ];

            $i++;

        }


        /* dd($puntos); */

        /* dd($valores); */

        /* dd($data); */


        return $puntos;

    }












    public function getDataForGraficoTipologiasMasRealizadas($request){

        // 1) CREAMOS UNA COLECCION EN LA QUE INCLUIREMOS TODA LA DATA NECESARIA
        $data = collect();


        // 2) OBTENEMOS UNA COLECCION DE LAS TIPOLOGIAS EXISTENTES
        $tipologias = DB::table('tipologias')
        ->select('tipologias.*')
        ->orderBy('tipologia_id', 'asc')
        ->get();

        /* dd($tipologias); */

        $data = $data->push($tipologias->toArray());

        

        
        // 3) OBTENEMOS EL TOTAL DE EXPEDIENTES REGISTRADOS

        $localidad  = $request->input('localidad');
        $fecha1 = $request->input('fecha1');
        $fecha2 = $request->input('fecha2');


        $total_expedientes = DB::table('expedientes')

        /* ->join('tareas', 'expedientes.tarea_id', '=', 'tareas.tarea_id')

        ->when($fecha1, function ($query, $fecha1) {
            return $query->where('expedientes.fecha_inicio', '>=' , $fecha1);
        })

        ->when($fecha2, function ($query, $fecha2) {
            return $query->where('expedientes.fecha_inicio', '<=' , $fecha2);
        }) */

        ->count();


        /* dd($total_expedientes); */

        // salimos del método en caso de que no existan expedientes, porque lanza error al entrar al index
        if ($total_expedientes == 0){
            return "Sin expedientes actualmente";
        }


        // 4) CREAMOS UN ARRAY PARA GUARDAR EL TOTAL DE EXPEDIENTES POR TIPOLOGIAS
        // Y OTRO ARRAY PARA GUARDAR LOS PORCENTAJES. 
        // MAS ALLÁ DE QUE SÓLO ES NECESARIO UNN ARRAY, SE IMPLEMENTAN DOS PARA FACILITAR EL DEBUGGING 

        $valores = array();
        $porcentajes = array();

        $i = 0; 

        foreach($tipologias as $tipologia){
            
            /* dd($tipo_tarea->tipo_tarea); */

            $total_por_tipologia = DB::table('expedientes')
            ->join('tipologias', 'expedientes.tipologia_id', '=', 'tipologias.tipologia_id')
            ->join('obras', 'expedientes.obra_id', '=', 'obras.obra_id')

            ->select('expedientes.tipologia_id')
            ->where('expedientes.tipologia_id', '=', $tipologia->tipologia_id)


            ->when($localidad, function ($query, $localidad) {
                return $query->where('obras.localidad_id', '>=' , $localidad);
            })

            ->when($fecha1, function ($query, $fecha1) {
                return $query->where('expedientes.fecha_inicio', '>=' , $fecha1);
            })

            ->when($fecha2, function ($query, $fecha2) {
                return $query->where('expedientes.fecha_inicio', '<=' , $fecha2);
            })

            ->orderBy('expedientes.tipologia_id', 'desc')
            ->count();
            
            
            /* dd($total_por_tipologia); */

            $valores[$i] = $total_por_tipologia;
            $porcentajes[$i] = ( $total_por_tipologia * 100) / $total_expedientes; 

            $i++;
        }



        /* dd($porcentajes); */







        // 5) CREAMOS EL ARRAY QUE LE PASAREMOS A LA VISTA

        $puntos = [];
        $i= 0;

        foreach ($tipologias as $tipologia){

            $puntos[] = [
                'name' => $tipologia->tipologia,
                'y' => floatval($porcentajes[$i])

            ];

            $i++;

        }


        /* dd($puntos); */

        return $puntos;








        $data = $data->push($porcentajes);
        $data = $data->push($valores);


        /* dd($valores); */

        /* dd($data); */
    }
























    public function getDataForGraficoCantidadMetrosCuadradosConstruidos($request){

        $localidad  = $request->input('localidad');
        $fecha1     = $request->input('fecha1');
        $fecha2     = $request->input('fecha2');


        $tipologias = DB::table('tipologias')
        ->select('tipologias.*')
        ->orderBy('tipologia_id', 'asc')
        ->get();

        $profesionales = Profesional::all();


        // TEST  para mejorar el array de profesionales

        /*  $profesionales = DB::table('profesionales')
        ->join('expedientes', 'profesionales.id','=','expedientes.profesional_id')
        ->select (
            'profesionales.id', 'profesionales.profesional_nombres', 'profesionales.profesional_apellidos'
        )
        ->where('expedientes.objeto_id', '=', config('app.objeto_nuevo'))
        ->groupBy(
            'profesionales.id', 'profesionales.profesional_nombres', 'profesionales.profesional_apellidos'
        )
        ->get();

        $profesionales = $profesionales->toArray(); */
        /* dd($profesionales); */

        $arrayExpedientesPorTipologias = array();

        $metrosCuadradosPorTipologia = array(
            floatval(0),
            floatval(0),
            floatval(0),
            floatval(0),
            floatval(0),
            floatval(0),
            floatval(0),
        );       
        
        $data_por_profesional = array(
        
            "tipologia1" => array_fill(0,$profesionales->count(),array_fill_keys(array('name', 'y'),0)),
            "tipologia2" => array_fill(0,$profesionales->count(),array_fill_keys(array('name', 'y'),0)),
            "tipologia3" => array_fill(0,$profesionales->count(),array_fill_keys(array('name', 'y'),0)),
            "tipologia4" => array_fill(0,$profesionales->count(),array_fill_keys(array('name', 'y'),0)),
            "tipologia5" => array_fill(0,$profesionales->count(),array_fill_keys(array('name', 'y'),0)),
            "tipologia6" => array_fill(0,$profesionales->count(),array_fill_keys(array('name', 'y'),0)),
            "tipologia7" => array_fill(0,$profesionales->count(),array_fill_keys(array('name', 'y'),0)),

            // TEST  para mejorar el array de profesionales
            /* "tipologia1" => array_fill(0,count($profesionales),array_fill_keys(array('name', 'y'),0)), */
            
        );

        /* dd($data_por_profesional); */

        $i = 0;

        foreach($tipologias as $tipologia){
            
            /* dd($tipologia); */

            $expedientes_por_tipologia = DB::table('expedientes')

            ->join('profesionales', 'expedientes.profesional_id', '=', 'profesionales.id' )
            ->join('tipologias', 'expedientes.tipologia_id', '=', 'tipologias.tipologia_id')
            ->join('obras', 'expedientes.obra_id', '=', 'obras.obra_id')

            ->select(
                'expedientes.superficie_a_construir',
                'expedientes.profesional_id',
                'expedientes.expediente_id',
                DB::raw("CONCAT(profesionales.profesional_nombres,' ',profesionales.profesional_apellidos) as nombreProfesional"),
                'tipologias.tipologia'
            )
            ->where('expedientes.tipologia_id', '=', $tipologia->tipologia_id)
            
            ->where('expedientes.objeto_id', '=', config('app.objeto_nuevo'))


            ->when($localidad, function ($query, $localidad) {
                return $query->where('obras.localidad_id', '>=' , $localidad);
            })

            ->when($fecha1, function ($query, $fecha1) {
                return $query->where('expedientes.fecha_inicio', '>=' , $fecha1);
            })

            ->when($fecha2, function ($query, $fecha2) {
                return $query->where('expedientes.fecha_inicio', '<=' , $fecha2);
            })

            ->orderBy('expedientes.tipologia_id', 'asc')
            ->groupBy(
                'expedientes.superficie_a_construir','expedientes.profesional_id', 'expedientes.expediente_id', 
                'profesionales.profesional_nombres', 'profesionales.profesional_apellidos',
                'tipologias.tipologia', 
            )
            ->get();

            $arrayExpedientesPorTipologias[$i] = $expedientes_por_tipologia->toArray();
            

            
            /* // DEBUGGING
            if($i == 2){

                dd($expedientes_por_tipologia);

                if(empty($expedientes_por_tipologia[$i])){
                    dd("si flaco");

                } else {

                dd($expedientes_por_tipologia);
                }
            } */


            
            if(!empty($expedientes_por_tipologia)){

                foreach ($expedientes_por_tipologia as $data_de_expediente){


                    /* // DEBUGGING
                    if($i == 2){

                        dd($expedientes_por_tipologia);

                        if(empty($expedientes_por_tipologia[$i])){
                            dd("si flaco");

                        } else {

                        dd($expedientes_por_tipologia);
                        }
                    } */


                    /* dd($data_de_expediente); */
                    /* dd($data_de_expediente->superficie_a_construir); */
                    /* dd($data_de_expediente->profesional_id); */
                    /* dd($data_de_profesional['tipologia'.$i+1][($data_de_expediente->profesional_id)-1]['y']); */

                    $metrosCuadradosPorTipologia[$i] += floatval($data_de_expediente->superficie_a_construir);

                    // TEST  para mejorar el array de profesionales

                    /* $clave = array_search($data_de_expediente->profesional_id, array_column($profesionales, 'id')); 

                    dd($clave); */
                    //TEST : Necesito el valor del array en profesionales donde esta ubicado el profesional que está en $data_de_expediente
                    //$data_por_profesional['tipologia'.$i+1][$profesionales->id-1]

                    //['name'] = $data_de_expediente->nombreProfesional;


                    $data_por_profesional['tipologia'.$i+1][$data_de_expediente->profesional_id-1]['name'] = $data_de_expediente->nombreProfesional;
                    /* dd($data_por_profesional[$data_por_expediente->profesional_id -1]['name']); */
                    $data_por_profesional['tipologia'.$i+1][($data_de_expediente->profesional_id)-1]['y'] += floatval($data_de_expediente->superficie_a_construir);
                   /*  $data_por_profesional[2]['y'] += floatval(4); */    

                    


                }

            } else {

                $metrosCuadradosPorTipologia[$i] = 0;
            }


            $i++;
        }


        /* debug($arrayExpedientesPorTipologias, $data_por_profesional, $metrosCuadradosPorTipologia); */

        
        $puntos = [];
        $i= 0;

        foreach ($tipologias as $tipologia){

            $puntos[] = [
                'name' => $tipologia->tipologia,
                'y' => floatval($metrosCuadradosPorTipologia[$i])

            ];

            $i++;

        }


        foreach($data_por_profesional as $key => $data_por_tipologia){

            foreach ($data_por_tipologia as $key2 => $profesional_stats){

                // dd($data_por_profesional[$key][$key2]['name']);
                // dd($data_por_tipologia[$key2]['y']);
                if ($data_por_tipologia[$key2]['y'] == 0 ){
                    // dd($data_por_tipologia[$profesional_stats]);
                    // unset($data_por_tipologia[$profesional_stats]);
                    // unset($data_por_tipologia[$key2]);
                    unset($data_por_profesional[$key][$key2]);
                
                } else {
                    $data_por_tipologia[$key2] = Arr::add($data_por_tipologia[$key2], 'id',$data_por_tipologia[$key2]['name']);
                    /* dd($data_por_tipologia); */
                }

                /* dd($data_por_profesional[$key]); */
               /*  arsort($data_por_tipologia[$key]); */

            }  

        }

        unset($key);
        unset($key2);

        // dd($data_por_profesional);

        foreach($data_por_profesional as $key => &$data_por_tipologia){

            foreach ($data_por_tipologia as $key2 => &$profesional_stats){

                $profesional_stats = Arr::add($data_por_tipologia[$key2], 'id',$data_por_tipologia[$key2]['name']);
                
            }  
            
            // dd($data_por_tipologia);
        }

        unset($key);
        unset($key2);

        /* dd($data_por_profesional); */

        $puntos = json_encode($puntos);
        $data_por_profesional = json_encode($data_por_profesional);

        /* debug($data_por_profesional); */

        $data = collect(array($puntos, $data_por_profesional));

        
        /* dd($data); */
        return $data;


    }







    /**
     *  Codigo Base para el grafico de metros cuadrados por tipologia
     *  
     */

    public function baseCode(){

        $tipologias = DB::table('tipologias')
        ->select('tipologias.*')
        ->orderBy('tipologia_id', 'asc')
        ->get();

        $array = array();
        $i = 0;

        foreach($tipologias as $tipologia){
            
            /* dd($tipologia); */

            $expedientes_por_tipologia = DB::table('expedientes')

            ->join('profesionales', 'expedientes.profesional_id', '=', 'profesionales.id' )
            ->join('tipologias', 'expedientes.tipologia_id', '=', 'tipologias.tipologia_id')

            ->select(
                'expedientes.superficie_a_construir',
                'expedientes.profesional_id',
                'expedientes.expediente_id',
                DB::raw("CONCAT(profesionales.profesional_nombres,' ',profesionales.profesional_apellidos) as nombreProfesional"),
                'tipologias.tipologia'
            )
            ->where('expedientes.tipologia_id', '=', $tipologia->tipologia_id)
            ->where('expedientes.objeto_id', '=', config('app.objeto_nuevo'))

            /*  ->when($fecha1, function ($query, $fecha1) {
                return $query->where('expedientes.fecha_inicio', '>=' , $fecha1);
            })

            ->when($fecha2, function ($query, $fecha2) {
                return $query->where('expedientes.fecha_inicio', '<=' , $fecha2);
            }) */

            ->orderBy('expedientes.tipologia_id', 'asc')
            ->groupBy(
                'expedientes.superficie_a_construir','expedientes.profesional_id', 'expedientes.expediente_id', 
                'profesionales.profesional_nombres', 'profesionales.profesional_apellidos',
                'tipologias.tipologia', 
            )
            ->get();

            $array[$i] = $expedientes_por_tipologia->toArray();
            
            $i++;
        }


        dd($array);

    }














}