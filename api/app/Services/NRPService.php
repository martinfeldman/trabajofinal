<?php


namespace App\Services;


use Illuminate\Support\Facades\DB;

use App\Events\SolicitudReemplazoProfesionalEvent ;
use App\Events\ValoracionNotaReemplazoProfesionalEvent;
use App\Repositories\NRPQueries;

use App\Models\NotaReemplazoProfesional;
use App\Models\Propietario;

use App\Http\Requests\NotaReemplazoProfesionalFormRequest;
use App\Jobs\crear_notaReemplazoProfesional;
use App\Jobs\procesoAutomazitado3_notaReemplazoProfesional;
use App\Mail\notaReemplazoProfesionalAprobadaMail;
use App\Models\Expediente;
use App\Models\Obra;
use App\Models\Plano;
use App\Models\Profesional;
use App\Models\Tarea;
use App\Models\User;
use App\Repositories\ExpedienteQueries;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;

use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Support\Facades\Mail;

class NRPService {



    protected $nrpQueries;
    protected $expedienteService;
    protected $expedienteQueries;




    public function __construct(NRPQueries $nrpQueries,ExpedienteService $expedienteService, ExpedienteQueries $expedienteQueries) {

        $this->nrpQueries = $nrpQueries;
        $this->expedienteService = $expedienteService;
        $this->expedienteQueries = $expedienteQueries;
    }















    public function createNRP(NotaReemplazoProfesionalFormRequest $request){

        
        /* dd($request); */

        $expediente_afectado_id = $this->nrpQueries->getExpedienteAfectadoID($request);

        /* dd($expediente_afectado_id); */


        if ($expediente_afectado_id->isEmpty()) {
            
            return Redirect()->back()->with('error', 'Los datos ingresados no coincidieron con los de algún expediente');
        }
        

        

        $expediente_afectado_numero = (Expediente::findOrFail($expediente_afectado_id[0]->expediente_id))->expediente_numero;
        


        if ($request->input('pendiente_cambios_checkbox') === 'on'){
            $pendiente_cambios = true;
        } else {
            $pendiente_cambios = false;
        }


        try{

            DB::beginTransaction();
            
            $fecha = Carbon::now()->setTimeZone('America/Argentina/Buenos_Aires')->format('Y-m-d H:i:s');
            


            $nota = NotaReemplazoProfesional::create ([

                'profesional_entrante_id'       => Auth()->user()->profesional_id,
                'profesional_saliente_id'       => $request->input('profesional_id'),
                'expediente_afectado_id'        => $expediente_afectado_id[0]->expediente_id,
                'expediente_afectado_numero'    => $expediente_afectado_numero,
                'nota_aprobada'                 => 0,
                'condicion_nrp_id'              => config('app.nrp_faltan_firmas'),
                'fecha'                         => $fecha,
                'observaciones'                 => "",
                'avance_obra'                   => $request->input('avance_obra'),
                'pendiente_cambios'             => $pendiente_cambios,
                'alta'                          => 1
            ]);




            $rutaDestino = 'public/PruebasAvance';


            $archivo1 = $request->file('pruebaAvance1');
            $archivo2 = $request->file('pruebaAvance2');

            $nombreArchivo1 = "Prueba Avance 1 NRP Nº " .  $nota->nota_reemplazo_profesional_id . " - " . $archivo1->getClientOriginalName();
            $nombreArchivo2 = "Prueba Avance 2 NRP Nº " .  $nota->nota_reemplazo_profesional_id . " - " . $archivo2->getClientOriginalName();

            $ruta = $archivo1->storeAs($rutaDestino, $nombreArchivo1);
            $ruta = $archivo2->storeAs($rutaDestino, $nombreArchivo2);



            $nota->update([
                'prueba_avance_1' => $nombreArchivo1, 
                'prueba_avance_2' => $nombreArchivo2, 
            ]);




            $dataExtraNota = $this->nrpQueries->getDataExtraNRP($nota, $request);



            $pdfNota = PDF::loadView('pdfs.notaReemplazoProfesional', ([

                'profesional_entrante_nombreCompleto'       => $dataExtraNota['profesional_entrante_nombreCompleto'],
                'profesional_entrante_numero_matricula'     => $dataExtraNota['profesional_entrante_numero_matricula'],
                'profesional_saliente_nombreCompleto'       => $dataExtraNota['profesional_saliente_nombreCompleto'],
                'profesional_saliente_numero_matricula'     => $dataExtraNota['profesional_saliente_numero_matricula'],
                'propietario_nombreCompleto'                => $dataExtraNota['propietario_nombreCompleto'],
                'propietario_dni'                           => $dataExtraNota['propietario_dni'],
                'obra'                                      => $dataExtraNota['obra']

            ]),

                compact ('nota', 'request' /* , 'logo' */));




            //ACA GUARDO EN STORAGE EL PDFNOTA Y AGREGO AL OBJETO NOTA

            
            
            $nombrePdfNota = "Nota Reemplazo Profesional Nº " .  $nota->nota_reemplazo_profesional_id . ".pdf";
            
            /* dd($nombrePdfNota); */

            $nota->fill(array (
                /* 'nota_reemplazo_profesional'    => $pdfNota,  */
                'nombre_archivo'                => $nombrePdfNota,
            ));
            

            $nota->save();

            Storage::put('public/NotasReemplazoProfesional/' . $nombrePdfNota, $pdfNota->output());
            
            /* return $pdfNota->stream(); */
            
            /* dd("todo okey"); */
            


            DB::commit();



        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        } catch (\Throwable $e) {
            DB::rollback();
            throw $e;
        }
    
    

        return Redirect()->back()->with('success', 'Nota de Reemplazo Profesional ha sido creada. Puede observar que la condición ha cambiado');

    }












    public function subirArchivoNotaFirmada($id, $request){

        $fecha = Carbon::now()->setTimeZone('America/Argentina/Buenos_Aires')->format('Y-m-d H:i:s');

        $nota = NotaReemplazoProfesional::findOrFail($id);


        $rutaDestino = 'public/NotasReemplazoProfesional';

        $archivo = $request->file('notaFirmada');

        $nombreArchivo = $archivo->getClientOriginalName();


        try {

            DB::beginTransaction();


            $nota->update([
                'nota_reemplazo_profesional'    => $archivo, 
                'nombre_archivo'                => $nombreArchivo,
                'condicion_nrp_id'              => config('app.nrp_para_revision'),
                'fecha'                         => $fecha,
            ]);


            $ruta = $archivo->storeAs($rutaDestino, $nombreArchivo);


            event (new SolicitudReemplazoProfesionalEvent($nota));




            DB::commit();

            
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        } catch (\Throwable $e) {
            DB::rollback();
            throw $e;
        }

    }


















    public function valorarNotaReemplazoProfesional($request, $id){

        // dd($request);

        $nota = NotaReemplazoProfesional::findOrFail($id);
        $expediente_afectado_id = $nota->expediente_afectado_id;

        if ($request->input('aprobadoCheckbox') === "on" ){

            $this->aprobarNotaReemplazoProfesional($nota);

        }  else {      

            $observaciones = $request->input('motivo') . ". " .  $request->input('otroMotivo');
            

            // if ($request->input('PlanosODocumentosCheckbox') === 'on') { // ESTA INCOMPLETO

            //     $nota->condicion_nrp_id = config('app.para_completar');
            //     $incompleto = true;
            //     $this->procesoAutomazitado3($nota, $expediente_afectado_id, $incompleto, $observaciones);


            // } else { // SE DESAPROBÓ

                $this->desaprobarNotaReemplazoProfesional($nota, $observaciones);

            // }
            
        } 


    }












    public function desaprobarNotaReemplazoProfesional($nota, $observaciones){


        /* dd($nota); */
        $expediente = Expediente::findOrFail($nota->expediente_afectado_id);

        try {

            DB::beginTransaction();


            $nota->update ([
                'condicion_nrp_id' => config('app.nrp_desaprobada'),
                'nota_aprobada'    => false,
                'observaciones'    => $observaciones,
            ]);


            event (new ValoracionNotaReemplazoProfesionalEvent($expediente, $nota));
            // el expediente que paso al evento no se usa en este caso de uso. deberia setearlo a null en la declaracion del evento. Pendiente.

        
            DB::commit();

        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        } catch (\Throwable $e) {
            DB::rollback();
            throw $e;
        }

    }












    public function aprobarNotaReemplazoProfesional($nota){

        /* dd($nota); */

        $expediente_afectado_id = $nota->expediente_afectado_id;
    
        $this->procesoAutomazitado3($nota, $expediente_afectado_id);

    }














    public function procesoAutomazitado3($nota, $expediente_afectado_id) {


        
        
        try {

            DB::beginTransaction();
            /* dd($nota); */

            

            $nota->update ([
                'condicion_nrp_id' => config('app.nrp_aprobada'),
            ]);
            
            
            
            $expediente_afectado = Expediente::findOrFail($expediente_afectado_id);
            // cambios en el exp 1
            
            
            // si numero de tareas = 1  {
                $expediente_afectado->update([
                    'estado_id'                     => config('app.estado_cerrado'),
                    'nota_reemplazo_profesional_id' => $nota->nota_reemplazo_profesional_id,
                    'condicion_id'                  => config('app.cerrado_por_falta_tareas_reemplazo_profesional'),
                    'prioridad_administracion'      => 5,
                    'prioridad_profesional'         => 5,
                ]);



                $this->expedienteService->actualizarHistorialCondicionExpediente($expediente_afectado);


                $tareaDireccionObra_ExpedienteAfectado = Tarea::findOrFail($expediente_afectado->tarea_id);
            
            // } else {

            //     $tareaDireccionObra_ExpedienteAfectado = encontrarTareaCorrespondienteADireccionObra();

            // }
            
            


            $tareaDireccionObra_ExpedienteAfectado->update([
                'reemplazo' => true,
            ]);
            
            
            // crear exp nuevo

            $dataExpedienteAfectado = ExpedienteQueries::getExpedienteData($expediente_afectado->expediente_id);


            /* dd($dataExpedienteAfectado); */

            $fecha_inicio = Carbon::now()->setTimeZone('America/Argentina/Buenos_Aires')->format('Y-m-d H:i:s');             
    
            $obra = Obra::create ([
                'propietario_id' => $dataExpedienteAfectado[0]->propietario_id,

                'localidad_id' => $dataExpedienteAfectado[0]->localidad_id,

                'seccion' => $dataExpedienteAfectado[0]->seccion,
                'chacra' => $dataExpedienteAfectado[0]->chacra,
                'manzana' => $dataExpedienteAfectado[0]->manzana,
                'parcela' => $dataExpedienteAfectado[0]->parcela,

                'calle' => $dataExpedienteAfectado[0]->calle,
                'numero' => $dataExpedienteAfectado[0]->numero,
                'barrio' => $dataExpedienteAfectado[0]->barrio,

                'partida_inmobiliaria' => $dataExpedienteAfectado[0]->partida_inmobiliaria,

            ]);
            
    


            $planoDeExpedienteAfectado = $this->expedienteQueries->getFileFromExpediente($expediente_afectado_id);

    
            // AGREGAR IF que controle que: 
            // SI tipo_tarea_id != los tipos de tarea que necesitan plano
            // entonces plano_id != null
    
            $tarea = Tarea::create([
                'tipo_tarea_id' => config('app.direccion_obra_tarea_id'),
                'fecha_inicio' => $fecha_inicio,
                'fecha_fin' => null,
                'plano_id' => $planoDeExpedienteAfectado[0]->plano_id,
                'reemplazo' => 0,
            ]);
        
    
    
    
            /**
             * 
             *  Para el expediente, vamos a crear unas variables previamente
             * 
             * $ultimoNumeroExpediente -> para obtener el  ultimo Numero de Expediente,
             *  al que le sumaremos 1 para el nuevo expediente
             *  o usaremos la el valor de la variable de entorno NUMERO_INICIAL_EXPEDIENTES si no existe.
             * 
             *  $fecha_inicio;
             */
    
    
            $ultimoNumeroExpediente= DB::table('expedientes')
            ->select ('expediente_numero')
            ->orderby ('expediente_id', 'DESC')
            ->limit('1')
            ->get();

        
            $numeroExpediente = $ultimoNumeroExpediente[0]->expediente_numero +1;    
    
            $superficie_a_construir = $expediente_afectado->superficie_a_construir;

            $profesional_id = $nota->profesional_entrante_id;




            if ($nota->pendiente_cambios == true){
                
                $aprobado                   = false;
                $estado_id                  = config('app.para_revisar');
                $condicion_id               = config('app.formulario_incompleto');
                $prioridad_administracion   = 3;
                $prioridad_profesional      = 2;

            } else {

                $aprobado       = true;
                $estado_id      = config('app.estado_abierto');
                $condicion_id   = config('app.para_liquidar');
                $prioridad_administracion   = 1;
                $prioridad_profesional      = 4;

            }


            $expediente= Expediente::create([

                'expediente_numero'             => $numeroExpediente,

                'profesional_id'                => $profesional_id,

                'tarea_id'                      => $tarea->tarea_id,
                'tipologia_id'                  => $expediente_afectado->tipologia_id,
                'objeto_id'                     => $expediente_afectado->objeto_id,
                'obra_id'                       => $obra->obra_id,
                'superficie_a_construir'        => $superficie_a_construir,
                'fecha_inicio'                  => $fecha_inicio,

                'condicion_id'                  => $condicion_id,
                'estado_id'                     => $estado_id,

                'aprobado'                      => $aprobado,

                'nota_reemplazo_profesional_id' => $nota->nota_reemplazo_profesional_id,
            
                'prioridad_administracion'      => $prioridad_administracion,
                'prioridad_profesional'         => $prioridad_profesional,

            ]); 
    



            $this->expedienteService->actualizarHistorialCondicionExpediente($expediente);



            event (new ValoracionNotaReemplazoProfesionalEvent($expediente, $nota));
            

            // ACA MAIL

            $profesional_email = (User::where('profesional_id', $profesional_id)->get()->first())->email;

            $correo = new notaReemplazoProfesionalAprobadaMail($nota, $expediente/* , $dataForEmail */);
            Mail::to(isset($emails) ? $emails : $profesional_email)->send($correo);


            
            DB::commit();

        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        } catch (\Throwable $e) {
            DB::rollback();
            throw $e;


        }
    }












}