<?php


namespace App\Services;


use App\Models\ComprobantePago;
use App\Models\Expediente;
use App\Models\Obra;
use App\Models\Plano;
use App\Models\Tarea;
use App\Models\Certificado;
use App\Models\HistorialCondicion;


use App\Http\Requests\ExpedienteFormRequest;


use Illuminate\Support\Facades\Storage;

use App\Events\NuevoFormularioExpedienteEvent;
use App\Events\CertificadoGeneradoEvent;
use App\Events\ComprobantePagoCargadoEvent;
use App\Events\DocumentacionExpedienteActualizadaEvent;
use App\Events\DocumentacionFormularioActualizadaEvent;
use App\Events\RenunciaDireccionObraEvent;
use App\Models\Profesional;
use App\Repositories\ExpedienteQueries;
use Carbon\Carbon;

use Illuminate\Support\Facades\DB;


use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Database\QueryException;
use SimpleSoftwareIO\QrCode\Facades\QrCode as QrCode;











class ExpedienteService {





    public function comprobarUnicidadExpediente($request){



        /**
         * 
         *  La idea es recibir el request que llega desde CreateExpediente, 
         *  armar una colección para luego proceder a comparar con las colecciones
         *  de datos de los expedientes del profesional del ultimo mes.
         *  Si alguno es igual se aborta la creacion del expediente y se devuelve un mensaje. 
         */

        $expediente = Expediente::find(2);

        $expediente = collect([$expediente->pluck('original')]);
        dd($expediente);



        $profesional = Profesional::findOrfail($expedienteNuevo->profesional_id);
        $expedientes = $expedientes = $profesional->expedientes()
        ->whereDate('fecha_inicio', '>=', Carbon::now()->subMonth())
        ->where('expediente_id', '!=', $expedienteNuevo->expediente_id)
        ->get();


        /*  si $expedientes es diferente de null ->
            foreach ($expedientes as $exp){
                traer data de $exp($exp->expediente_id): obra, objeto, tipologia, tarea->tipo_tarea, 
                compare to $expedienteNuevo
                    if es igual 
                        abort
                

            } */


        if (!$expedientes->isEmpty()){

            $expedienteNuevoData = ExpedienteQueries::getExpedienteData($expedienteNuevo);
            //$expedienteNuevoData = $expedienteNuevoData[0];
            dd($expedienteNuevoData);

            foreach($expedientes as $exp){

                $exp = ExpedienteQueries::getExpedienteData($exp->expediente_id);
                $exp = $exp[0];
                
                dd($diff = $expedienteNuevo->diff($exp));
                
                //dd($exp[0]);
                
                
                /* json_decode($exp); */

            }  
            
        }


        return $expedientes; 


    }













    public function createExpediente($request){


        /* $this->comprobarUnicidadExpediente($request); */
        
        
        try {
            
            DB::beginTransaction();

            $fecha_inicio = Carbon::now()->setTimeZone('America/Argentina/Buenos_Aires')->format('Y-m-d H:i:s');             



            $obra = Obra::create ([
                'propietario_id'        =>  trim($request->input('propietario_id')),

                'localidad_id'          =>  trim($request->input('localidad_id')),

                'seccion'               =>  trim($request->input('seccion')),
                'chacra'                =>  trim($request->input('chacra')),
                'manzana'               =>  trim($request->input('manzana')),
                'parcela'               =>  trim($request->input('parcela')),

                'calle'                 =>  trim(strtoupper($request->input('calle'))),
                'numero'                =>  trim($request->input('numero')),
                'barrio'                =>  trim(strtoupper($request->input('barrio'))),

                'partida_inmobiliaria'  =>  trim($request->input('partida_inmobiliaria_numero')),

            ]);
        

            $rutaDestino = 'public/CarpetaPlanosYDocumentos';

            $archivo = $request->file('plano');

            $nombreArchivo = "Archivo de FormExpt" .  " - " . $archivo->getClientOriginalName();



            $plano = Plano::create([
                'plano'=>$archivo,
                'nombre_archivo'=>$nombreArchivo,
                'fecha'=> $fecha_inicio,
            ]);


            $ruta = $archivo->storeAs($rutaDestino, $nombreArchivo);



            $tarea = Tarea::create([
                'tipo_tarea_id' =>  $request->input('tipo_tarea_id'),
                'fecha_inicio'  =>  $fecha_inicio,
                'fecha_fin'     =>  null,
                'plano_id'      =>  $plano->plano_id,
                'reemplazo'     =>  0,
            ]);
    


            $ultimoNumeroExpediente= DB::table('expedientes')
            ->select ('expediente_numero')
            ->orderby ('expediente_id', 'DESC')
            ->limit('1')
            ->get();


            if ($ultimoNumeroExpediente->isEmpty()) {
                $numeroExpediente = config('app.numero_inicial_expedientes');
            } else {
                $numeroExpediente = $ultimoNumeroExpediente[0]->expediente_numero +1;
            }


            if ($request->input('profesional_id') !== null){
                $profesional_id = $request->input('profesional_id');
            } else {
                $profesional_id = Auth()->user()->profesional_id;
            }

    
            $expediente = Expediente::create([

                'expediente_numero'         =>  $numeroExpediente,

                'profesional_id'            =>  $profesional_id,

                'tarea_id'                  =>  $tarea->tarea_id,
                'tipologia_id'              =>  $request->input('tipologia_id'),
                'objeto_id'                 =>  $request->input('objeto_id'),
                'obra_id'                   =>  $obra->obra_id,
                'superficie_a_construir'    =>  $request->input('superficie_a_construir'),
                'superficie_con_permiso'    =>  $request->input('superficie_con_permiso'),
                'superficie_sin_permiso'    =>  $request->input('superficie_sin_permiso'),

                'fecha_inicio'              =>  $fecha_inicio,

                'condicion_id'              =>  config('app.formulario_a_revisar'),
                'estado_id'                 =>  config('app.para_revisar'),
                'aprobado'                  =>  null,

                'prioridad_administracion'  =>  2,
                'prioridad_profesional'     =>  3,
            
            ]); 
            
            

            $this->actualizarHistorialCondicionExpediente($expediente);


            $evento = event(New NuevoFormularioExpedienteEvent($expediente));

            

            DB::commit();
            
            return $expediente->expediente_id; 
            
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        } catch (\Throwable $e) {
            DB::rollback();
            throw $e;
        }


    }














    

    public function editExpediente(ExpedienteFormRequest $request, $id){


        $expediente = Expediente::findOrFail($id);

        try {

            DB::beginTransaction();

            $obra = Obra::findOrFail($expediente->obra_id);
            $obra->update([
                'propietario_id'=>$request->input('propietario_id'),

                'localidad_id'=>$request->input('localidad_id'),

                'seccion'=>$request->input('seccion'),
                'chacra'=>$request->input('chacra'),
                'manzana'=>$request->input('manzana'),
                'parcela'=>$request->input('parcela'),

                'calle'=>$request->input('calle'),
                'numero'=>$request->input('numero'),
                'barrio'=>$request->input('barrio'),

                'partida_inmobiliaria' => $request->input('partida_inmobiliaria_numero')

            ]);

            $tarea = Tarea::findOrFail($expediente->tarea_id);


        
            $rutaDestino = 'storage/CarpetaPlanosYDocumentos/';

            $archivo = $request->file('plano');
            $nombreArchivo = $archivo->getClientOriginalName();
            /*    $nombreArchivo = $archivo . '-' time();  */

            $plano = Plano::findOrFail($tarea->plano_id);
            $plano->update([
                'plano'         => $archivo
            ]);


            $archivo->storeAs('CarpetaPlanos', $nombreArchivo);



            $tarea->update([
                'tipo_tarea_id' => $request->input('tipo_tarea_id'),
                // 'plano_id'      => $plano->plano_id,
            ]);


            $expedienteModificado = $expediente->update([

                'profesional_id'                => $expediente->profesional_id,
                'tipologia_id'                  => $request->input('tipologia_id'),
                'objeto_id'                     => $request->input('objeto_id'),
                'superficie_a_construir'        => $request->input('superficie_a_construir'),
                'superficie_con_permiso'        => $request->input('superficie_con_permiso'),
                'superficie_sin_permiso'        => $request->input('superficie_sin_permiso'),
            ]); 


            DB::commit();

            return 0;

        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        } catch (\Throwable $e) {
            DB::rollback();
            throw $e;
        }


    }









    public function actualizarHistorialCondicionExpediente($expediente){

        $ultimaAuditoria = $expediente->audits()->latest()->first();
        
        /* dd($ultimaAuditoria); */
        
        try {

            $fecha = Carbon::now()->setTimeZone('America/Argentina/Buenos_Aires')->format('Y-m-d H:i:s');


            DB::beginTransaction();

            $nuevoRegistroHistorialCondicion = HistorialCondicion::create([

                'expediente_id' => $expediente->expediente_id,
                'condicion_actual_id' => $ultimaAuditoria['new_values']['condicion_id'],
                'fecha' => $fecha,

            ]);


            DB::commit();

            return 0;

        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        } catch (\Throwable $e) {
            DB::rollback();
            throw $e;
        }

    }











    public function cargarArchivoDeTarea($request, $id, $tarea_id){

        /* dd($request, "cargarArchivoDeTarea"); */


        try {

            DB::beginTransaction();


            $expediente = Expediente::findOrFail($id);
            $tarea = Tarea::findOrFail($tarea_id);
            $plano = Plano::findOrFail($tarea->plano_id);
            /* dd($plano); */



            $rutaDestino = 'public/CarpetaPlanosYDocumentos';

            $archivo = $request->file('archivoSubido');

            /* dd($archivo); */

            $nombreArchivo = "FormExpt" .  " - " . $archivo->getClientOriginalName();



            $plano->update([
                'plano'             =>$archivo,
                'nombre_archivo'    =>$nombreArchivo,
                'fecha'             => Carbon::now()->setTimeZone('America/Argentina/Buenos_Aires')->format('Y-m-d H:i:s'),
            ]);


            $ruta = $archivo->storeAs($rutaDestino, $nombreArchivo);


            $tarea->update([
                'tipo_tarea_id'     => $tarea->tipo_tarea_id,
                'fecha_inicio'      => $tarea->fecha_inicio,
                'fecha_fin'         => $tarea->fecha_fin,
                'plano_id'          => $plano->plano_id,
                'reemplazo'         => $tarea->reemplazo,
            ]);



            // PARA NOTIFICAR A SUPERVISOR 

            event(New DocumentacionFormularioActualizadaEvent($expediente));



            DB::commit();

            return $expediente;



        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        } catch (\Throwable $e) {
            DB::rollback();
            throw $e;
        }




    }










    public function cambiarCondicionExpedientePropietarioNotificadoLiquidacion($id){

        try {
            
            DB::beginTransaction();

            $expediente = Expediente::findOrFail($id);

            $expediente->update([
                'condicion_id' => config('app.esperando_comprobante_2'),
            ]);

            $this->actualizarHistorialCondicionExpediente($expediente);

            DB::commit();

            return 0;

        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        } catch (\Throwable $e) {
            DB::rollback();
            throw $e;
        }

    } 













    public function cargarComprobantePago($request, $expediente){

        /* dd($request); */ 

        
        try {
            
            DB::beginTransaction();

            
            $time = Carbon::now()->setTimeZone('America/Argentina/Buenos_Aires');
            
            $rutaDestino = 'public/CarpetaComprobantesPagos';
            
            $archivo = $request->file('comprobantePago');

            //$nombreArchivo =  $archivo->getClientOriginalName();

            $nombreArchivo = "CP - Expet Nº " . $expediente->expediente_numero .  " - " . $archivo->getClientOriginalName();



            $ruta = $archivo->storeAs($rutaDestino, $nombreArchivo);

            $comprobantePago = ComprobantePago::create([
                'comprobante_pago'  => $archivo,
                'nombre_archivo'    => $nombreArchivo,
                'fecha'             => $time,
                'expediente_id'     => $expediente->expediente_id,
            ]);   



            $expediente->update([
                'comprobante_pago_id'       => $comprobantePago->comprobante_pago_id,
                'condicion_id'              => config('app.para_cerrar'),
                'prioridad_administracion'  => 1,
                'prioridad_profesional'     => 4,
            ]);


            /* dd($expediente); */

            $this->actualizarHistorialCondicionExpediente($expediente);


            // PARA NOTIFICAR

            event(New ComprobantePagoCargadoEvent($expediente, $comprobantePago->comprobante_pago_id));


            DB::commit();

            return $comprobantePago;

        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        } catch (\Throwable $e) {
            DB::rollback();
            throw $e;
        }

        
    }

















    public function renunciarDireccionObra($id){


        $expediente = Expediente::findOrfail($id);


        try {

        
            DB::beginTransaction();

            $expediente->update([
                'condicion_id'          => config('app.cerrado_por_falta_tareas_renuncia'),
                'estado_id'             => config('app.estado_cerrado'),
                'prioridad_administracion'  => 5,
                'prioridad_profesional'     => 5,
            ]);


            $this->actualizarHistorialCondicionExpediente($expediente);

            DB::commit();

            $evento = event(new RenunciaDireccionObraEvent($expediente));

            return 0;

        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        } catch (\Throwable $e) {
            DB::rollback();
            throw $e;       
        }


    }











    










}