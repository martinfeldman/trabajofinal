<?php


namespace App\Services;












class AuditoriaService {





    /**
     *  Este metodo quita las llaves al comienzo y final de los strings de las columnas Old y New values
     *  de los registros de auditorias.
     *  También, cambia las comas por "comas + newline" (aunque no funciona en algunos casos, no sé porque)
     * 
     *  Es estático por la implementación del filtrado. Los registros llegan filtrados recién en la vista, 
     *  no en el controlador. Por lo que se requirió llamar a este método en la vista para poder formatear 
     *  las columnas.
     */

    public static function staticFormatearColumnas($auditorias){



        foreach ($auditorias as $auditoria){
            

            if ($auditoria->event === "created"){

                $auditoria->old_values = "Vacío";

            } else {

                $auditoria->old_values = str_replace(array('{','}'),'',$auditoria->old_values);
                $auditoria->old_values = str_replace(','  ,  (','.PHP_EOL) , $auditoria->old_values);

            }


            $auditoria->new_values = str_replace(array('{','}'),'',$auditoria->new_values);
            $auditoria->new_values = str_replace(','  ,  (','.PHP_EOL) , $auditoria->new_values);

            
            // dd($auditoria);
        }

        // dd($auditorias);

        return $auditorias;

    }








}