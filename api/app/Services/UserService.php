<?php


namespace App\Services;

use App\Models\Profesional;
use App\Models\Propietario;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Spatie\Permission\Models\Role;

class UserService {






    public function updateUser($request, $id){

        // dd($request);

        $user = User::findOrFail($id);
        
        try {
            
            DB::beginTransaction();

            $huboCambios = true;

            $user->name         = $request->input('nombres');
            $user->last_name    = $request->input('apellidos');
            $user->cuit         = $request->input('cuit');
            $user->email        = $request->input('email');
            

            if($user->profesional_id){

                $profesional = Profesional::findOrFail($user->profesional_id)->update([

                    'profesional_nombres'      => $request->input('nombres'),
                    'profesional_nombres'      => $request->input('apellidos'),
                    'profesional_cuit'         => $request->input('cuit'),

                ]);
                
                // $profesionalActualizado = true;

            }


            if($user->propietario_id){

                $propietario = Propietario::findOrFail($user->propietario_id)->update([

                    'propietario_nombres'       => $request->input('nombres'),
                    'propietario_nombres'       => $request->input('apellidos'),
                    'propietario_cuit'          => $request->input('cuit'),
                    'propietario_email'         => $request->input('email'),

                ]);
            
                
                // $profesionalActualizado = true;
            }





            $huboCambios = true;
            $user->isDirty() ? /* dd("dirty") */ $user->save() : $huboCambios = false;
            // dd($huboCambios);
            DB::commit();
            

            return $huboCambios; 
            
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        } catch (\Throwable $e) {
            DB::rollback();
            throw $e;
        }
    }








    public function updateUserRolesPermisos($request, $id){

        $huboCambios = false;
        $profesionalCreado = false;
        $propietarioCreado = false;

        $user = User::findOrFail($id);

        // dd("permisos de MNF: ". $user->getPermissionNames());

        $rolesActualmente = $user->getRoleNames();

        // dd($request->administrativoCheckbox);
        // dd($request, $rolesActualmente);
        $rolesDeseados = collect();
        
        if($request->administrativoCheckbox === 'on'){
            $rolesDeseados = $rolesDeseados->push('administrativo');
        }

        if($request->supervisorCheckbox === 'on'){
            $rolesDeseados = $rolesDeseados->push('supervisor');
        }

        if($request->administrativoHibridoCheckbox === 'on'){
            $rolesDeseados = $rolesDeseados->push('administrativoHibrido');
        }

        if($request->profesionalCheckbox === 'on'){
            $rolesDeseados = $rolesDeseados->push('profesional');
        }

        if($request->propietarioCheckbox === 'on'){
            $rolesDeseados = $rolesDeseados->push('propietario');
        }

        // dd($rolesDeseados);

        // primero querremos comprobar si se le han asignado menos o nuevos roles, y cuales se mantienen 

        $nuevosRoles = $rolesDeseados->diff($rolesActualmente);

        $rolesPerdidos = $rolesActualmente->diff($rolesDeseados);

        $rolesQueSeMantienen = $rolesActualmente->diff($nuevosRoles)->diff($rolesPerdidos);

        // dd("nuevos roles: ".$nuevosRoles, "rolesPerdidos: ". $rolesPerdidos, "rolesQueSeMatienen: ". $rolesQueSeMantienen);



        // el unico rol que requiere mirar los checkbos es administrativoHibrido, hay dos posibilidades:
        try{


            DB::beginTransaction();

            if (!$nuevosRoles->isEmpty() || !$rolesPerdidos->isEmpty() ){

                // quito y asigno roles
                $user->syncRoles($rolesDeseados);
                $huboCambios = true;

            }



            if($rolesQueSeMantienen->contains('administrativoHibrido') || $nuevosRoles->contains('administrativoHibrido')){

                // aca llamo o comienza el metodo para mirar los checkboxs

                // 1. sumar los permisos de administrativo y unicosSupervisor en una coleccion

                $permisosAdministrativo = Role::findByName('administrativo')->permissions;
                $permisosSupervisor = Role::findByName('supervisor')->permissions;
                $permisosUnicosSupervisor = $permisosSupervisor->diff($permisosAdministrativo);

                // dd($permisosAdministrativo, $permisosUnicosSupervisor);

                $permisosPosiblesAdministrativoHibrido = collect();
                $permisosPosiblesAdministrativoHibrido = $permisosPosiblesAdministrativoHibrido->merge($permisosAdministrativo)->sortBy('id');
                $permisosPosiblesAdministrativoHibrido = $permisosPosiblesAdministrativoHibrido->merge($permisosUnicosSupervisor)->sortBy('id');

                // dd($permisosPosiblesAdministrativoHibrido);


                // 2. recorrer $permisosPosiblesAdministrativoHibrido y ver si está en el request. 
                // si está, lo agregamos al usuario

                $inicioString   = "permiso-";
                $finalString    = "-Checkbox";

                foreach($permisosPosiblesAdministrativoHibrido as $permiso){

                    // dd($permiso->name);

                    $newStr = str_replace(' ', '_', $permiso->name);

                    $stringCheckboxRequest = sprintf('%1$s%2$s%3$s', $inicioString, $newStr, $finalString );

                    // dd($stringCheckboxRequest);

                    if($request->input($stringCheckboxRequest) && !$user->hasPermissionTo($permiso->name) /* versionAnterior :esta ultima condicion no estaba */ ){

                        // dd("este permisso estaba en el request: ". $stringCheckboxRequest);
                        // dd("ttiene este permiso: ". $permiso->name." el usuario ? ". $user->hasPermissionTo($permiso->name));
                        // dd("entre en no tiene el permiso");
                        $user->givePermissionTo($permiso->name);
                        $huboCambios = true;

                    // } else  y era con este el else no mas {

                    //     $user->revokePermissionTo($permiso->name);

                    } elseif($request->input($stringCheckboxRequest) && $user->hasPermissionTo($permiso->name)){ 
                        // dd("tienee este permiso: ". $permiso->name." el usuario ? ". $user->hasPermissionTo($permiso->name));

                        // if($huboCambios){
                        //     $huboCambios = false;
                        // }

                    } elseif ($user->hasPermissionTo($permiso->name)){

                        $user->revokePermissionTo($permiso->name);
                        $huboCambios = true;

                        // dd("entre en no revocar el permiso");
                    }

                }


                // dd("rolesUsuario: ".$user->getRoleNames(), "permisos de Usuario: ".$user->getPermissionNames());


            } elseif ($rolesPerdidos->flip()->has('administrativoHibrido')){

                // dd("entre aca");

                $permisosDirectosUser = $user->getPermissionNames();
                // dd($permisosDirectosUser);

                foreach($permisosDirectosUser as $permisoDirecto){

                    // dd($permisoDirecto);
                    $user->revokePermissionTo($permisoDirecto);
                }

                $huboCambios = true;

            } 



            

            // si entra aca, es porque se pidio el rol de profesional para un usuario que se registró en el sistema y dió su numero de matricula. 
            // Es un profesional que todavía no le concedieron el rol de profesional

            if ($nuevosRoles->contains('profesional') && !$user->profesional_id && isset($user->numero_matricula)){
                
                $fecha = Carbon::now()->setTimeZone('America/Argentina/Buenos_Aires')->format('Y-m-d H:i:s');

                $profesional = Profesional::create([
                    'profesional_nombres'           => $user->name,
                    'profesional_apellidos'         => $user->last_name,
                    'profesional_numero_matricula'  => $user->numero_matricula,
                    'profesional_cuit'              => $user->cuit,
                    'fecha_registro'                => $fecha,
                    'alta'                          => true,
                ]);

                $profesionalCreado = true;

                $user->update([
                    'profesional_id' => $profesional->id
                ]);

                // $huboCambios  = true;   // por las dudas

            // En cambio acá, es alguien sin registro de profesional pero que debe ser personal administrativo 
            // (propietario no, porque no tenemos registro usuario con rol propietario)

            } elseif($nuevosRoles->contains('profesional') && !$user->profesional_id && !isset($user->numero_matricula) && $request->input('numero_matricula')){

                $fecha = Carbon::now()->setTimeZone('America/Argentina/Buenos_Aires')->format('Y-m-d H:i:s');

                $profesional = Profesional::create([
                    'profesional_nombres'           => $user->name,
                    'profesional_apellidos'         => $user->last_name,
                    'profesional_numero_matricula'  => $request->input('numero_matricula'),
                    'profesional_cuit'              => $user->cuit,
                    'fecha_registro'                => $fecha,
                    'alta'                          => true,
                ]);

                $profesionalCreado = true;

                $user->update([
                    'profesional_id' => $profesional->id
                ]);

                // $huboCambios  = true;   // por las dudas

            }



            if ($nuevosRoles->contains('propietario') && !$user->propietario_id){
                
                $fecha = Carbon::now()->setTimeZone('America/Argentina/Buenos_Aires')->format('Y-m-d H:i:s');

                $propietario = Propietario::create([
                    'propietario_nombres'           => $user->name,
                    'propietario_apellidos'         => $user->last_name,
                    'propietario_cuit'              => $user->cuit,
                    'propietario_email'             => $user->email,
                    'alta'                          => true,
                ]);


                $propietarioCreado = true;
                
                $user->update([
                    'propietario_id' => $propietario->id
                ]);

                // $huboCambios  = true;   // por las dudas

            }

            // dd($user->isDirty());
            // dd($huboCambios);
            // dd($resultado);
            // dd("dirty: ".$user->isDirty(). " , huboCambios: ".$huboCambios );
            
            DB::commit();

            if($profesionalCreado){
                return "Usuario modificado y registro de Profesional creado con éxito";
            }

            if($propietarioCreado){
                return "Usuario modificado y registro de Propietario creado con éxito";
            }

            
            if(!$user->isDirty() && !$huboCambios){
                // dd("entre al coso");
                return false;
            }
            

            return true; 

        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        } catch (\Throwable $e) {
            DB::rollback();
            throw $e;
        }


    }






}