<?php


namespace App\Services;

use App\Models\Configuracion;
use App\Repositories\ConfiguracionQueries;
use Illuminate\Support\Facades\DB;

class ConfiguracionService {


    public $configuracionQueries;


    
    public function __construct(ConfiguracionQueries $configuracionQueries) {
        
        $this->configuracionQueries = $configuracionQueries;

    }






    public function updateConfiguracion($request){

        $configuracion = Configuracion::get()->first();
        // dd($configuracion->tiempo_cerrar_expediente_1er_proceso_automatizado);

        try {
            
            DB::beginTransaction();

            $configuracion->nombre_institucion                                   = $request->input('nombre_institucion');
            $configuracion->email                                                = $request->input('email');
            $configuracion->localidad                                            = $request->input('localidad');
            $configuracion->calle                                                = $request->input('calle');
            $configuracion->numero                                               = $request->input('numero');
            $configuracion->tiempo_primer_recordatorio_1er_proceso_automatizado  = $request->input('tiempo1erRecordatorio');
            $configuracion->tiempo_segundo_recordatorio_1er_proceso_automatizado = $request->input('tiempo2doRecordatorio');
            $configuracion->tiempo_cerrar_expediente_1er_proceso_automatizado    = $request->input('tiempoCierreExpediente');
            
            $huboCambios = true;
            $configuracion->isDirty() ? /* dd("dirty") */ $configuracion->save() : $huboCambios = false;
                
            
            if($request->file('logo') !== null){

                $rutaDestino = 'public/LogosSistema';

                $archivo = $request->file('logo');

                $nombreArchivo = "LogoSistema - ". $archivo->getClientOriginalName();

                $ruta = $archivo->storeAs($rutaDestino, $nombreArchivo);

                $configuracion->update([
                    'logo'                  =>   $archivo,
                    'nombre_archivo_logo'   =>   $nombreArchivo,
                ]);

            } elseif($huboCambios == false) {

                return $huboCambios; 
            }


            DB::commit();
            
            return $huboCambios = true; 
            
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        } catch (\Throwable $e) {
            DB::rollback();
            throw $e;
        }

    }





}