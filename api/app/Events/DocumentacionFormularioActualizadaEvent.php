<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;





class DocumentacionFormularioActualizadaEvent {

    public $expediente;
    public $emisor;


    use Dispatchable, InteractsWithSockets, SerializesModels;




    /**
     * Create a new event instance.
     *
     * @return void
     */

    public function __construct($expediente){
        
        $this->expediente = $expediente;
        $this->emisor = Auth()->user()->name." ".Auth()->user()->last_name;        

    }






    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
