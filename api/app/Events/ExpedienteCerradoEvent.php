<?php

namespace App\Events;

use App\Models\Expediente;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Collection as SupportCollection;






class ExpedienteCerradoEvent {

    public $expediente;
    public $dataExtraExpediente;
    public $emisor;
    

    use Dispatchable, InteractsWithSockets, SerializesModels;


    


    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Expediente $expediente, $dataExtraExpediente)
    {
        $this->expediente = $expediente;
        $this->dataExtraExpediente = $dataExtraExpediente;
        $this->emisor = Auth()->user()->name." ".Auth()->user()->last_name;
        /* $this->userName = $userName; */
    }




    
    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
