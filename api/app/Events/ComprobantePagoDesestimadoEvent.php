<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;





class ComprobantePagoDesestimadoEvent {

    public $expediente;
    public $motivo;

    use Dispatchable, InteractsWithSockets, SerializesModels;




    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($expediente, $motivo){
        $this->expediente = $expediente;
        $this->motivo = $motivo;
        
    }






    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
