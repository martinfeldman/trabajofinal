<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;





class SolicitudReemplazoProfesionalEvent {

    use Dispatchable, InteractsWithSockets, SerializesModels;
    
    public $nota; 
    public $emisor; 



    


    /**
     * Create a new event instance.
     *
     * @return void
     */

    public function __construct($nota) {

        $this->nota = $nota;
        $this->emisor = Auth()->user()->name." ".Auth()->user()->last_name;
    }






    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
