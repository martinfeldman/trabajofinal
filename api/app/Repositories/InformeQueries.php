<?php

namespace App\Repositories;

use Illuminate\Support\Facades\DB;
use App\Repositories\PropietarioQueries;






class InformeQueries  {


    public $propietarioQueries;




    public function __construct (PropietarioQueries $propietarioQueries){

        $this->propietarioQueries = $propietarioQueries;
    
    }







    public function getDataForinformeExpedientesTramitesInconclusos($request){

        $tipologia = $request->input('tipologia');
        $fecha1 = $request->input('fecha1');
        $fecha2 = $request->input('fecha2');


        $expedientesConTramitesInconclusos = DB::table('expedientes')
        ->join('obras', 'expedientes.obra_id', '=', 'obras.obra_id')
        ->join('propietarios as prop', 'obras.propietario_id', '=', 'prop.id')

        //recuperar nombre completo del profesional de un expediente
        ->join('profesionales as prof', 'expedientes.profesional_id', '=', 'prof.id')


        ->join('historial_condiciones','expedientes.expediente_id','=', 'historial_condiciones.expediente_id')

        ->select(
            'expedientes.expediente_numero', 'expedientes.estado_id', 'expedientes.fecha_inicio',
            DB::raw("CONCAT(prof.profesional_nombres,' ',prof.profesional_apellidos) as nombreProfesional"),
            DB::raw("CONCAT(prop.propietario_nombres,' ',prop.propietario_apellidos) as nombrePropietario"),
            'historial_condiciones.condicion_actual_id', 'historial_condiciones.fecha', 'historial_condiciones.expediente_id',
        )
        ->where('expedientes.estado_id', '=', config('app.tramite_inconcluso'))
        ->Where('historial_condiciones.condicion_actual_id', '=', config('app.para_liquidar'))


        ->when($tipologia, function ($query, $tipologia) {
            return $query->where('expedientes.tipologia_id', $tipologia);
        })

        ->when($fecha1, function ($query, $fecha1) {
            return $query->where('expedientes.fecha_inicio', '>=' , $fecha1);
        })

        ->when($fecha2, function ($query, $fecha2) {
            return $query->where('expedientes.fecha_inicio', '<=' , $fecha2);
        })


        ->groupBy(
            'expedientes.expediente_numero', 'expedientes.estado_id', 'expedientes.fecha_inicio',
            'prof.profesional_nombres', 'prof.profesional_apellidos',
            'prop.propietario_nombres', 'prop.propietario_apellidos',
            'historial_condiciones.condicion_actual_id', 'historial_condiciones.fecha', 'historial_condiciones.expediente_id'
        )

        ->orderBy('expedientes.fecha_inicio', 'desc')

        ->get();


    

        if (isset($tipologia)){

            $tipologiaNombre = DB::table('tipologias')
            ->where('tipologia_id', '=', $tipologia)
            ->get()->first();
        }


        /* dd($expedientesNumeros); */

    
        /* dd($expedientesConTramitesInconclusos); */

        return ([
            $expedientesConTramitesInconclusos,
            (isset($tipologiaNombre) ? $tipologiaNombre : null),
            (isset($fecha1) ? $fecha1 : null),
            (isset($fecha2) ? $fecha2 : null),
        ]);

    







    }











    public function getDataForinformeProfesionalesMasExpedientes($request){


        $tipologia = $request->input('tipologia');
        $condicion = $request->input('condicion');
        $fecha1 = $request->input('fecha1');
        $fecha2 = $request->input('fecha2');


        $profesionalesConMasExpedientes = DB::table('expedientes')

        ->join('profesionales as prof', 'expedientes.profesional_id', '=', 'prof.id')
        ->join('tipologias', 'expedientes.tipologia_id', '=', 'tipologias.tipologia_id')
        ->join('condiciones', 'expedientes.condicion_id', '=', 'condiciones.condicion_id')


        ->select(
            'prof.id',
            DB::raw("(  (count(expedientes.expediente_id)  ) )  as `totalExpedientes` "),
            DB::raw("CONCAT(prof.profesional_nombres,' ',prof.profesional_apellidos) as nombreProfesional"),
        )


        ->when($tipologia, function ($query, $tipologia) {
            return $query->where('expedientes.tipologia_id', $tipologia);
        })


        ->when($condicion, function ($query, $condicion) {
            return $query->where('expedientes.condicion_id', $condicion);
        })


        ->when($fecha1, function ($query, $fecha1) {
            return $query->where('expedientes.fecha_inicio', '>=' , $fecha1);
        })

        ->when($fecha2, function ($query, $fecha2) {
            return $query->where('expedientes.fecha_inicio', '<=' , $fecha2);
        })


        ->groupBy(
            'prof.id',
            'prof.profesional_nombres', 'prof.profesional_apellidos',
        )

        ->orderBy('totalExpedientes', 'desc')
        ->limit(10)

        ->get();



        if (isset($tipologia)){

            $tipologiaNombre = DB::table('tipologias')
            ->where('tipologia_id', '=', $tipologia)
            ->get()->first();
        }


        if (isset($condicion)){

            $condicionNombre = DB::table('condiciones')
            ->where('condicion_id', '=', $condicion)
            ->get()->first();
        }



        /* dd([
            $profesionalesConMasExpedientes,
            isset($tipologiaNombre) ? $tipologiaNombre : null,
            isset($condicionNombre) ? $condicionNombre : null,
            (isset($fecha1) ? $fecha1 : null),
            (isset($fecha2) ? $fecha2 : null),
        ]); */

    


        return ([
            $profesionalesConMasExpedientes, 
            (isset($tipologiaNombre) ? $tipologiaNombre : null),
            (isset($condicionNombre) ? $condicionNombre : null),
            (isset($fecha1) ? $fecha1 : null),
            (isset($fecha2) ? $fecha2 : null),
        ]);

    }








    public function getDataForIndexFiltros(){

        $estados = DB::table('estados')
        ->orderBy('estado_id','asc')
        ->get();


        $condiciones = DB::table('condiciones')
        ->orderBy('condicion_id','asc')
        ->get();

    
        $tipos_tareas= DB::table('tipos_tareas')
        ->orderBy('tipo_tarea_id','asc')
        ->get();


        $tipologias=DB::table('tipologias')
        ->orderBy('tipologia_id','asc')
        ->get();


        return ([
            $estados,
            $condiciones,
            $tipos_tareas,
            $tipologias,
        ]);   
    

    }








}