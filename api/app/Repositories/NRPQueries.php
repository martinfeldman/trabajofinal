<?php

namespace App\Repositories;

use App\Models\Expediente;
use App\Models\Obra;
use Illuminate\Support\Facades\DB;
use App\Models\Profesional;
use App\Models\Propietario;
use Illuminate\Support\Facades\Auth;

class NRPQueries{



    protected $propietarioQueries;




    public function __construct(PropietarioQueries $propietarioQueries) {
    
        $this->propietarioQueries = $propietarioQueries;

    }







    public function getProfesionalNRP(){

        $profesional_id = Auth::user()->profesional_id;

        $profesional = Profesional::findOrFail($profesional_id);

        $profesionalNRPS = $profesional->notasReemplazoProfesional;

    }









    public function getDataForNRPIndex($request){


        $profesional = $request->input('profesional');



        $data = DB::table('notas_reemplazo_profesional as nrps')

        ->join('expedientes as expt', 'nrps.expediente_afectado_id', '=', 'expt.expediente_id')
        

        //recuperar nombre completo del propietario de una obra
        ->join('obras', 'expt.obra_id', '=', 'obras.obra_id')
        ->join('propietarios as prop', 'obras.propietario_id', '=', 'prop.id')

    
        //recuperar nombre completo del profesional de un expediente
        ->join('profesionales as prof', 'expt.profesional_id', '=', 'prof.id')


        //recuperar tipologia de expediente
        ->join('tipologias', 'expt.tipologia_id', '=', 'tipologias.tipologia_id')

        //recuperar tipo de tarea de un expediente
        ->join('tareas', 'expt.tarea_id', '=', 'tareas.tarea_id')
        ->join('tipos_tareas', 'tareas.tipo_tarea_id', '=', 'tipos_tareas.tipo_tarea_id')

        // recuperar el texto de la condicion NRP
        ->join('condiciones_nota_reemplazo_profesional as condiciones_nrp', 'nrps.condicion_nrp_id', '=', 
            'condiciones_nrp.condicion_nota_reemplazo_profesional_id')

        
        ->select (
            'nrps.*',
            'expt.expediente_numero as expediente_afectado_numero',
            'prop.propietario_nombres', 'prop.propietario_apellidos', 
            'prof.profesional_nombres', 'prof.profesional_apellidos', 
            'tipologias.tipologia', 
            'tipos_tareas.tipo_tarea',
            'condiciones_nrp.condicion_nota_reemplazo_profesional as condicion_nrp',

        )

        ->when($profesional, function ($query, $profesional) {
            return $query->where('nrps.profesional_saliente_id', $profesional);
        })


        ->when($profesional, function ($query, $profesional) {
            return $query->orWhere('nrps.profesional_entrante_id', $profesional);
        })


        // ->where('nrps.nota_reemplazo_profesional_id','LIKE','%'.$query.'%')
       /*  ->where('notas_reemplazo_profesional.alta','=','1') */
        ->orderBy('nrps.nota_reemplazo_profesional_id','desc')
        ->paginate(15);

        // dd($data);


        /**
         * 
         * En el siguiente @foreach, vamos a agregar el nombre completo del profesional entrante 
         * a cada elemento $nota del array $notasReemplazoProfesional
         * 
         * Para ello, convertimos las variables $nota y $profesional_entrante_nombreCompleto en array 
         * 
         * y lo agregamos el nombre a la nota.
         * 
         * Se resolvió de esta forma pasar el nombre Completo porque desde la query con $data
         * no se podía obtener dos profesionales diferentes, o sí? 
         */
        
        
        /*   dd(gettype($data));

        $data = (array)$data;    

        foreach ($data as $nota) {


            dd(gettype($nota->profesional_entrante_id)); */


            /* dd($nota); */
            /* dd(gettype($nota)); */

            /* $profesional_entrante = Profesional::findOrFail($nota->profesional_entrante_id);
            $profesional_entrante_nombreCompleto = $profesional_entrante->profesional_nombres . " " . $profesional_entrante->profesional_apellidos;

            $profesional_entrante_nombreCompleto = array('profesional_entrante_nombreCompleto' => $profesional_entrante_nombreCompleto); */



            /* dd($profesional_entrante_nombreCompleto); */



            /*  $nota = (array)$nota;

            $nota = array_merge($nota, $profesional_entrante_nombreCompleto); */

           /*  $nota = $nota->merge($profesional_saliente_nombreCompleto); */

        /*      dd($data);
        }

        dd($data); */
        
        return $data;

    }









    public function getDataForProfesionalNRPIndex(){



        /* ESTA FUNCION DEBE DEVOLVER LOS NRP EXCLUSIVOS DEL PROFESIONAL QUE HA LLAMADO A LA FUNCION */

        $data= DB::table('notas_reemplazo_profesional as nrps')


        ->join('expedientes as expt', 'nrps.expediente_afectado_numero', '=', 'expt.expediente_numero')

        //recuperar nombre completo del propietario de una obra
        ->join('obras', 'expt.obra_id', '=', 'obras.obra_id')
        ->join('propietarios as prop', 'obras.propietario_id', '=', 'prop.id')

        //recuperar nombre completo del profesional de un expediente
        ->join('profesionales as prof', 'expt.profesional_id', '=', 'prof.id')

        //recuperar tipologia de expediente
        ->join('tipologias', 'expt.tipologia_id', '=', 'tipologias.tipologia_id')

        //recuperar tipo de tarea de un expediente
        ->join('tareas', 'expt.tarea_id', '=', 'tareas.tarea_id')
        ->join('tipos_tareas', 'tareas.tipo_tarea_id', '=', 'tipos_tareas.tipo_tarea_id')


        // recuperar el texto de la condicion NRP
        ->join('condiciones_nota_reemplazo_profesional as condiciones_nrp', 'nrps.condicion_nrp_id', '=', 
        'condiciones_nrp.condicion_nota_reemplazo_profesional_id')

        
        

        
        ->select (
            'nrps.*',
            'expt.expediente_numero',
            'prop.propietario_nombres', 'prop.propietario_apellidos', 
            'prof.profesional_nombres', 'prof.profesional_apellidos', 
            'tipologias.tipologia', 
            'tipos_tareas.tipo_tarea',
            'condiciones_nrp.condicion_nota_reemplazo_profesional as condicion_nrp',
        )



        // ->where('nrps.nota_reemplazo_profesional_id','LIKE','%'.$query.'%')
        /* ->where('notas_reemplazo_profesional.alta','=','1') */
        ->orderBy('nrps.nota_reemplazo_profesional_id','desc')
        ->paginate(10);



        return $data;


    }










    public function getExtraDataForNRPIndex($notasReemplazoProfesional){

        $fullCollection = collect([]);

        $i = 0;



        /* dd($notasReemplazoProfesional); */
        foreach ($notasReemplazoProfesional as $nota) {

            /* dd($nota); */
            /* dd(gettype($nota)); */


            $profesional_saliente = Profesional::findOrFail($nota->profesional_saliente_id);
            $profesional_saliente_nombreCompleto = $profesional_saliente->getFullName();

            $profesional_entrante = Profesional::findOrFail($nota->profesional_entrante_id);    
            $profesional_entrante_nombreCompleto = $profesional_entrante->getFullName();


            $condicionNRP = DB::table('condiciones_nota_reemplazo_profesional')
            ->where('condicion_nota_reemplazo_profesional_id', '=', $nota->condicion_nrp_id)
            ->select('condicion_nota_reemplazo_profesional')
            ->get()->first();


            $expediente_afectado_numero = (Expediente::findOrFail($nota->expediente_afectado_id))->expediente_numero;

            ${"collection$i"} = ([
                "profesional_saliente_NombreCompleto" => $profesional_saliente_nombreCompleto,
                "profesional_entrante_NombreCompleto" => $profesional_entrante_nombreCompleto,
                "condicion_nrp" => $condicionNRP->condicion_nota_reemplazo_profesional,
                "expediente_afectado_numero" => $expediente_afectado_numero,
            ]);


            //$array = array($profesional_saliente_nombreCompleto);

            $fullCollection->push(${"collection$i"});

            $i++;

        }
        

        return $fullCollection;


    }










    public function getDataForNRPCreate($currentUserRoles){


        $localidades=DB::table('localidades')
        ->orderBy('localidad_id','asc')->get();


        $profesionales = DB::table('profesionales')
        ->where('alta','=','1')
        ->orderBy('id','desc')->get();


        $propietarios = $this->propietarioQueries->getPropietariosForUser($currentUserRoles);

        return ([
            $localidades,
            $profesionales,
            $propietarios
        ]);   
    }













    
    public function getExpedienteAfectadoID ($requestNRP){


/*          dd( $requestNRP->input('localidad_id')) */
            $expediente_ID = DB::table('expedientes as expt')


             //recuperar profesional de un expediente
            ->join('profesionales as prof', 'expt.profesional_id', '=', 'prof.id')

            //recuperar nombre completo del propietario de una obra
            ->join('obras', 'expt.obra_id', '=', 'obras.obra_id')
            ->join('propietarios as prop', 'obras.propietario_id', '=', 'prop.id')

            ->join('tareas', 'expt.tarea_id' , '=' , 'tareas.tarea_id')
            ->join('tipos_tareas', 'tareas.tipo_tarea_id' , '=' , 'tipos_tareas.tipo_tarea_id')

            //recuperar partida inmobiliaria de una obra
           /*  ->join('partidas_inmobiliarias as partidas', 'obras.partida_inmobiliaria_id', '=', 'partidas.partida_inmobiliaria_id') */

        

            ->select (
                'expt.expediente_id',
                'tipos_tareas.tipo_tarea',
            )

            ->where('obras.localidad_id','=',       $requestNRP->input('localidad_id'))
            ->where('obras.seccion'     ,'=',       $requestNRP->input('seccion'))
            ->where('obras.manzana'     ,'=',       $requestNRP->input('manzana'))
            ->where('obras.chacra'      ,'=',       $requestNRP->input('chacra'))
            ->where('obras.parcela'     ,'=',       $requestNRP->input('parcela'))
            ->where('obras.calle'       ,'=',       strtoupper(trim($requestNRP->input('calle'))))
            ->where('obras.numero'      ,'=',       $requestNRP->input('numero'))
            ->where('obras.barrio'      ,'=',       strtoupper(trim($requestNRP->input('barrio'))))
            ->where('tipos_tareas.tipo_tarea_id', '=', config('app.direccion_obra_tarea_id'))

           /*  ->where('partidas.partida_inmobiliaria_numero'      ,'=',       $requestNRP->input('partida_inmobiliaria_numero')) */
        
            ->get();



            return $expediente_ID;


    }



















    public function getDataForCreateNRPFile($nota){





        /* dd($notasReemplazoProfesional); */

            /* dd($nota); */
            /* dd(gettype($nota)); */


        $profesional_saliente = Profesional::findOrFail($nota->profesional_saliente_id);
        $profesional_saliente_nombreCompleto = $profesional_saliente->getFullName();
        $profesional_entrante = Profesional::findOrFail($nota->profesional_entrante_id);    
        $profesional_entrante_nombreCompleto = $profesional_entrante->getFullName();
    
        $expediente_afectado_numero = (Expediente::findOrFail($nota->expediente_afectado_id))->expediente_numero;

    

        $data = array(
            $profesional_saliente_nombreCompleto,
            $profesional_entrante_nombreCompleto,
            $expediente_afectado_numero,

        );
      
        
     

        return $data;


    }










    public function getDataExtraNRP($nota, $request){

        $profesional_saliente = Profesional::findOrFail($nota->profesional_saliente_id);
        $profesional_saliente_nombreCompleto = $profesional_saliente->getFullName();
        $profesional_saliente_numero_matricula = $profesional_saliente->profesional_numero_matricula;

        $profesional_entrante = Profesional::findOrFail($nota->profesional_entrante_id);    
        $profesional_entrante_nombreCompleto = $profesional_entrante->getFullName();
        $profesional_entrante_numero_matricula = $profesional_entrante->profesional_numero_matricula;

        $expediente_afectado = Expediente::findOrFail($nota->expediente_afectado_id);


        $obra = Obra::find($expediente_afectado->obra_id);


        $propietario = Propietario::findOrFail($obra->propietario_id);
        
        $propietario_nombreCompleto = $propietario->getFullName();

        $propietario_dni = substr($propietario->propietario_cuit, 3, -2);



        $localidad_id = Obra::find($expediente_afectado->obra_id)->localidad_id;

        $localidad = DB::table('localidades')
        ->select('localidades.localidad')
        ->where('localidades.localidad_id', '=', $localidad_id)
        ->get()->first();


        $partida_inmobiliaria = $obra->partida_inmobiliaria;



        $obra = array (
            'localidad'             => $localidad->localidad,  
            /* 'seccion'               => $request->input('seccion'),   COMENTË PORQUE DA IGUAL USARLO DESDE EL REQUEST EN LA VISTA DE NOTA DE REEMPLAZO
            'chacra'                => $request->input('chacra'),
            'manzana'               => $request->input('manzana'),
            'parcela'               => $request->input('parcela'),
            'calle'                 => $request->input('calle'),
            'numero'                => $request->input('numero'),
            'barrio'                => $request->input('barrio'), */
            'partida_inmobiliaria'  => $partida_inmobiliaria,
        );




        $dataExtraNota = array(
            
            compact(
                'profesional_entrante_nombreCompleto',
                'profesional_entrante_numero_matricula',
                'profesional_saliente_nombreCompleto',
                'profesional_saliente_numero_matricula',
                'propietario_nombreCompleto',
                'propietario_dni',
                'obra'
            ) 
        );


        $dataExtraNota = $dataExtraNota[0];

        // dd($dataExtraNota['obra']['localidad']);


        return $dataExtraNota;






    //     $data = DB::table('expedientes as expt')

    //     ->join('profesionales as prof', 'expt.profesional_id', '=', 'prof.id')

    //     ->join('obras', 'expt.obra_id', '=', 'obras.obra_id')
    //     ->join('propietarios as prop', 'obras.propietario_id', '=', 'prop.id')

    //     ->join('tareas', 'expt.tarea_id' , '=' , 'tareas.tarea_id')
    //     ->join('tipos_tareas', 'tareas.tipo_tarea_id' , '=' , 'tipos_tareas.tipo_tarea_id')

    //     ->join ('localidades', 'obras.localidad_id' , '=' , 'localidades.localidad_id')

    //     ->select(
    //         'obras.seccion', 'obras.chacra', 'obras.manzana', 'obras.parcela',
    //         'obras.seccion',
    //         'prof.id',
    //         'expt.expediente_numero', 'expt.expediente_id',
    //         'tipos_tareas.tipo_tarea',
    //         'localidades.localidad'
    //     )

    //     ->where('tipos_tareas.tipo_tarea_id', '=', config('app.direccion_obra_tarea_id'))
    //     ->where('expt.expediente_id', '=', $nota->expediente_afectado_id)
    //    /*  ->orWhere('prof.id', '=', $nota->profesional_entrante_id)
    //     ->orWhere('prof.id', '=', $nota->profesional_saliente_id) */

    //     ->groupBy(
    //             'obras.'
    //             'prof.id as profesional_id',
    //             'expt.expediente_numero', 'expt.expediente_id',
    //             'tipos_tareas.tipo_tarea',
    //     )
    //     ->get();


        




    }







}