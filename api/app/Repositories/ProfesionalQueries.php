<?php

namespace App\Repositories;



use App\Models\Profesional;

use Illuminate\Support\Facades\DB;





class ProfesionalQueries  {




    public function getDataForIndex ($request){


        $fecha1 = $request->input('fecha1');
        $fecha2 = $request->input('fecha2');

        $profesional = $request->input('profesional');
        $alta = $request->input('alta');




        $profesionales= DB::table('profesionales')

        ->when($profesional, function ($query, $profesional) {
            return $query->where('profesionales.id', $profesional);
        })

        ->when($alta, function ($query, $alta) {
            return $query->where('profesionales.alta',  $alta);
        })

        ->when($fecha1, function ($query, $fecha1) {
            return $query->where('profesionales.fecha_registro', '>=' , $fecha1);
        })

        ->when($fecha2, function ($query, $fecha2) {
            return $query->where('profesionales.fecha_registro', '<=' , $fecha2);
        })

        ->orderBy('id','desc')
        ->paginate(10);

        
        return $profesionales;
    
    }









    // SIN USO
    public function getDataForExpedienteCreate(){

        $localidades=DB::table('localidades')
        ->orderBy('localidad_id','asc')->get();


        $objetos=DB::table('objetos')
        ->orderBy('objeto_id','asc')->get();


        $profesionales=DB::table('profesionales')
        ->where('alta','=','1')
        ->orderBy('profesional_id','desc')->get();


        $propietarios=DB::table('propietarios')
        ->where('alta','=','1')
        ->orderBy('id','desc')->get();


        $tipos_tareas=DB::table('tipos_tareas')
        ->orderBy('tipo_tarea_id','asc')->get();


        $tipologias=DB::table('tipologias')
        ->orderBy('tipologia_id','asc')->get();

        return ([
            $localidades,
            $objetos,
            $profesionales,
            $propietarios,
            $tipos_tareas,
            $tipologias
        ]);   
       

    }






}