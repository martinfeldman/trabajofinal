<?php

namespace App\Repositories;

use Illuminate\Support\Facades\DB;





class ValidadorCertificadosQueries {





    // SI BIEN BUSCA EL CERTIFICADO, SE DEVUELVE EL EXPEDIENTE, PARA MOSTRAR SUS DATOS EN /validador_certificados/validar

    public function buscarCertificado($codigo){

        $certificado = DB::table('certificados')
        ->join('expedientes', 'certificados.expediente_id', '=', 'expedientes.expediente_id')
        ->join('profesionales', 'expedientes.profesional_id', '=', 'profesionales.id')
        ->select(
            'expedientes.*',
            'profesionales.profesional_nombres', 'profesionales.profesional_apellidos', 
            'profesionales.profesional_numero_matricula', 'profesionales.profesional_cuit', 
        )
        ->where('random_number', '=', $codigo)
        ->get()->first();

        return $certificado;



    }


}