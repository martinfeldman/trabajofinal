<?php

namespace App\Repositories;



use App\Models\Profesional;

use Illuminate\Support\Facades\DB;





class PropietarioQueries {






    public function getDataForPropietarioIndex ($request, $currentUserRoles){



        $propietario = $request->input('propietario');
        // dd($propietario);

        switch(true){

            case $currentUserRoles->contains('administrativo'):
            case $currentUserRoles->contains('administrativoHibrido'):
            case $currentUserRoles->contains('supervisor'):

                $propietarios= DB::table('propietarios') 

                ->when($propietario, function ($query, $propietario) {
                    return $query->where('propietarios.id', $propietario);
                })

                ->orderBy('id','desc')
                ->paginate(15);
                
                // dd($propietarios);

                return $propietarios;

            case $currentUserRoles->contains('profesional'):

                $profesional = Profesional::findOrfail(Auth()->user()->profesional_id);

                $propietarios = $profesional->propietarios()

                ->when($propietario, function ($query, $propietario) {
                    return $query->where('propietarios.id', $propietario);
                })
                
                ->orderBy('id','desc')
                ->paginate(15);   
                

                // dd($propietarios);
                // dd($profesional->propietarios);

                return $propietarios;


        }

    }







    public function getDataForFiltros($currentUserRoles){

        switch(true){

            case $currentUserRoles->contains('administrativo'):
            case $currentUserRoles->contains('administrativoHibrido'):
            case $currentUserRoles->contains('supervisor'):

                $propietarios = DB::table('propietarios')
                ->orderBy('id','desc')
                ->get();

                return $propietarios;


            case $currentUserRoles->contains('profesional'):

                $profesional = Profesional::findOrfail(Auth()->user()->profesional_id);

                $propietarios = $profesional->propietarios()

                ->orderBy('id','desc')
                ->get();   


                return $propietarios;
        
        }

    }








    //      SE USA PARA CARGAR EL SELECT DE PROPIETARIOS DEL USUSARIO CON ROL PROFESIONAL EN INDEX EXPEDIENTES 
    //      Y TAMBIEN PARA EL SELECT EN NOTA DE REEMPLAZO PROFESIONAL

    public function getPropietariosForUser ($currentUserRoles){

        switch(true){

            case $currentUserRoles->contains('administrativo'):
            case $currentUserRoles->contains('administrativoHibrido'):
            case $currentUserRoles->contains('supervisor'):

                $data= DB::table('propietarios')
                ->where('alta','=','1')
                ->orderBy('id','desc')
                ->get();
                
                return $data;

                break;

            case $currentUserRoles->contains('profesional'):

                $profesional = Profesional::findOrfail(Auth()->user()->profesional_id);

                $propietarios = $profesional->propietarios()
                ->where('alta','=','1')
                ->orderBy('id','desc')
                ->get();   

                return $propietarios;

                break;

        }

    }











    // SIN USO
    public function getDataForExpedienteCreate(){

        $localidades=DB::table('localidades')
        ->orderBy('localidad_id','asc')->get();


        $objetos=DB::table('objetos')
        ->orderBy('objeto_id','asc')->get();


        $profesionales=DB::table('profesionales')
        ->where('alta','=','1')
        ->orderBy('profesional_id','desc')->get();


        $propietarios=DB::table('propietarios')
        ->where('alta','=','1')
        ->orderBy('id','desc')->get();


        $tipos_tareas=DB::table('tipos_tareas')
        ->orderBy('tipo_tarea_id','asc')->get();


        $tipologias=DB::table('tipologias')
        ->orderBy('tipologia_id','asc')->get();

        return ([
            $localidades,
            $objetos,
            $profesionales,
            $propietarios,
            $tipos_tareas,
            $tipologias
        ]);   
    

    }






}