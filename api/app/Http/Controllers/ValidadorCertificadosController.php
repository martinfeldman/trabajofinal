<?php

namespace App\Http\Controllers;

use App\Services\ValidadorCertificadosService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;







class ValidadorCertificadosController extends Controller {


    public $validadorCertificadosService;


    public function __construct(ValidadorCertificadosService $validadorCertificadosService) {

        $this->middleware('guest');
        $this->validadorCertificadosService = $validadorCertificadosService;
    }







    public function index(Request $request) {

        $randomNumber = substr($request->getPathInfo(),24);

        /* dd($randomNumber); */

        return view('validadorCertificaciones.index', (
            compact('randomNumber')
        ));
        
    }





    public function validar(Request $request){


        $randomNumbers = DB::table('certificados')
        ->select('certificados.random_number')
        ->get();

        /*  dd($randomNumbers); */

        /* dd($request->codigo); */

        $certificado = $this->validadorCertificadosService->comprobarCodigoIngresado($request->codigo);

        

        /* dd($certificado); */

        return view("validadorCertificaciones.validar", 

            compact('certificado')

        );
    }






}
