<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

use App\Models\NotaReemplazoProfesional;
use App\Models\Propietario;


use App\Http\Requests\NotaReemplazoProfesionalFormRequest;
use App\Jobs\crearNotaReemplazoProfesional;
use App\Models\Profesional;
use App\Models\User;
use App\Services\NRPService;

use App\Repositories\NRPQueries;
use App\Repositories\PropietarioQueries;


use Illuminate\Support\Facades\Auth;


use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;












class NotaReemplazoProfesionalController extends Controller {


    protected $nrpService;
    protected $nrpQueries;
    




    public function __construct(NRPService $nrpService, NRPQueries $nrpQueries) {
        $this->middleware('auth');
        $this->nrpService = $nrpService;
        $this->nrpQueries = $nrpQueries;
    }







    public function index(Request $request) {

        
        $user = Auth()->user();

        if(!$user->hasVerifiedEmail()){
            return view ('auth.instructionsForRegisteredUser');
        } elseif ($user->getRoleNames()->isEmpty()) {
            return view ('auth.waitingForRoleAssignment');
        }

        $currentUserRoles = Auth()->user()->getRoleNames();
        
        switch(true){

            case $currentUserRoles->contains('administrativo'):
            case $currentUserRoles->contains('administrativoHibrido'):
            case $currentUserRoles->contains('supervisor'):

                $notasReemplazoProfesional= $this->nrpQueries->getDataForNRPIndex($request);

                $datosExtraNRPIndex = $this->nrpQueries->getExtraDataForNRPIndex($notasReemplazoProfesional);

                /* dd( $notasReemplazoProfesional , $datosExtraNRPIndex); */
                
                $profesionales = Profesional::all();

                /* dd(asset("/public/NotasReemplazoProfesional".$notasReemplazoProfesional->first()->prueba_avance_1)); */

                return view('notaReemplazoProfesional.index',[
                    "notasReemplazoProfesional" => $notasReemplazoProfesional, 
                    "datosExtraNRP"             => $datosExtraNRPIndex,
                    'profesionales'             => $profesionales,
                ]);  


            case $currentUserRoles->contains('profesional'): 

                $profesional = Profesional::findOrFail($user->profesional_id);
        
                $notasReemplazoProfesional = NotaReemplazoProfesional::whereBelongsTo($profesional)
                ->orderBy('notas_reemplazo_profesional.nota_reemplazo_profesional_id', 'desc')
                ->paginate(15);
                /* dd($notasReemplazoProfesional[0]); */
                
                $datosExtraNRP = $this->nrpQueries->getExtraDataForNRPIndex($notasReemplazoProfesional);
                /* dd($datosExtraNRPIndex); */

                $data = $this->nrpQueries->getDataForNRPCreate($currentUserRoles);
                

                return view('notaReemplazoProfesional.index',[
                    "notasReemplazoProfesional"     => $notasReemplazoProfesional,
                    "datosExtraNRP"                 => $datosExtraNRP,
                    "localidades"                   => $data[0],
                    "profesionales"                 => $data[1],
                    "propietarios"                  => $data[2],
                ]);
                
        }
        
    
    }









    public function search(Request $request) {


        $user = Auth()->user();

        if(!$user->hasVerifiedEmail()){
            return view ('auth.instructionsForRegisteredUser');
        } elseif ($user->getRoleNames()->isEmpty()) {
            return view ('auth.waitingForRoleAssignment');
        }

        $currentUserRoles = Auth()->user()->getRoleNames();
        
        switch(true){

            case $currentUserRoles->contains('administrativo'):
            case $currentUserRoles->contains('administrativoHibrido'):
            case $currentUserRoles->contains('supervisor'):

                $notasReemplazoProfesional= $this->nrpQueries->getDataForNRPIndex($request);

                $datosExtraNRPIndex = $this->nrpQueries->getExtraDataForNRPIndex($notasReemplazoProfesional);

                /* dd( $notasReemplazoProfesional , $datosExtraNRPIndex); */
                
                $profesionales = Profesional::all();

                /* dd(asset("/public/NotasReemplazoProfesional".$notasReemplazoProfesional->first()->prueba_avance_1)); */

                return view('notaReemplazoProfesional.index',[
                    "notasReemplazoProfesional" => $notasReemplazoProfesional, 
                    "datosExtraNRP"             => $datosExtraNRPIndex,
                    'profesionales'             => $profesionales,
                ]);  


            case $currentUserRoles->contains('profesional'): 

                $profesional = Profesional::findOrFail($user->profesional_id);
        
                $notasReemplazoProfesional = NotaReemplazoProfesional::whereBelongsTo($profesional)
                ->orderBy('notas_reemplazo_profesional.nota_reemplazo_profesional_id', 'desc')
                ->paginate(15);
                /* dd($notasReemplazoProfesional[0]); */
                
                $datosExtraNRP = $this->nrpQueries->getExtraDataForNRPIndex($notasReemplazoProfesional);
                /* dd($datosExtraNRPIndex); */

                $data = $this->nrpQueries->getDataForNRPCreate($currentUserRoles);
                

                return view('notaReemplazoProfesional.index',[
                    "notasReemplazoProfesional"     => $notasReemplazoProfesional,
                    "datosExtraNRP"                 => $datosExtraNRP,
                    "localidades"                   => $data[0],
                    "profesionales"                 => $data[1],
                    "propietarios"                  => $data[2],
                ]);
                
        }

    }













    public function descargarPruebaAvance($id, $pruebaAvance){

        $nota = NotaReemplazoProfesional::findOrFail($id);

        /* dd($nota); */

        if ($pruebaAvance == 1){

            $puebaAvancePath = $nota->prueba_avance_1;

        } else {

            $puebaAvancePath = $nota->prueba_avance_2;

        }

        $rutaAlmacenamiento = 'storage/PruebasAvance/';

        $archivo = $rutaAlmacenamiento . $puebaAvancePath;
        
        /* dd($archivo); */

        
        

        if (file_exists($archivo)) {
            return Response::download(public_path($archivo));
        } else {
            echo('File not found');
        }

    }











    public function create(){

        return view('notaReemplazoProfesional.create');
    
    }






    


    public function store(NotaReemplazoProfesionalFormRequest $request){

        /* dd(config('app.direccion_obra_tarea_id') ,$requestNRP); */


        
        $nota = $this->nrpService->createNRP($request);

        // QUEDA PARA IMPLEMENTAR EN LA PROXIMA ITERACION
        // $nota = crearNotaReemplazoProfesional::dispatch($request->all());

        return Redirect::to('reemplazo-profesional')->with('success', 'Nota de Reemplazo Profesional ha sido creada. Puede observar que la condición ha cambiado');
    }










    public function show($id){
        
        $nota = NotaReemplazoProfesional::findOrFail($id);

        if ($nota->profesional_entrante_id == Auth()->user()->profesional_id
        ||  $nota->profesional_saliente_id  == Auth()->user()->profesional_id
        ||  Auth()->user()->hasRole("administrativo") 
        ||  Auth()->user()->hasRole("administrativoHibrido")
        ||  Auth()->user()->hasRole("supervisor")) {


            $rutaAlmacenamiento = 'storage/NotasReemplazoProfesional/';

            $archivo = $rutaAlmacenamiento . $nota->nombre_archivo;
            
            /* dd($archivo); */

            if (file_exists($archivo)) {
                return Response::download(public_path($archivo));
            } else {
                echo('File not found');
            }


        } else {

            return Redirect::to('reemplazo-profesional')->with('error', 'Acceso no permitido. Profesionales pueden ver sus propias Notas de Reemplazo Profesional únicamente.');      

        }
    

    
    }














    public function subirArchivoNotaFirmada($id, Request $request){

        $request->validate([
            'notaFirmada' => 'required|file|max:10240',
        ]);

        $nrpActualizada = $this->nrpService->subirArchivoNotaFirmada($id, $request);

        return Redirect()->back()->with('success', 'El archivo de Nota de Reemplazo Profesional ha sido cargado con éxito. Puede observar que la condición ha cambiado');

    }
















    public function valorarNotaReemplazoProfesional(Request $request, $id){

        $request->validate([
            'aprobadoCheckbox'          =>'required_without:desaprobadoCheckbox', 
            'desaprobadoCheckbox'       =>'required_without:aprobadoCheckbox', 
        ]);


        $this->nrpService->valorarNotaReemplazoProfesional($request, $id);

        return Redirect()->back()->with('success', 'Nota de Reemplazo Profesional ha sido valorada. Puede observar que la condición ha cambiado');

    }
















    public function desaprobarNotaReemplazoProfesional($request, $nota){


        $expediente_afectado_id = $nota->expediente_afectado_id;

        $this->nrpService->procesoAutomazitado3($nota, $expediente_afectado_id);



    }




















    // NO USO

    public function edit($id){

        $propietario= Propietario::findOrFail($id);

        return view("propietario.modalEdit", $propietario);
    }


    public function update(NotaReemplazoProfesionalFormRequest $request, $id){
        /* return $request->all(); */

       /*  $propietario= $this->nrpService->updatePropietario($request, $id); */


        return Redirect::to('propietario');
    }

    
    public function destroy($id){
        
        $propietario= $this->propietarioService->deletePropietario($id);

        return Redirect::to('propietario');
    }






}
