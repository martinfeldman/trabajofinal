<?php

namespace App\Http\Controllers;

use App\Models\Configuracion;
use App\Models\Localidad;
use App\Models\User;
use App\Services\ConfiguracionService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;




class ConfiguracionController extends Controller {

    public $configuracionService;



    public function __construct(ConfiguracionService $configuracionService) {

        $this->configuracionService = $configuracionService;
    }



    public function index(Request $request) {

        $ultima_configuracion = Configuracion::get()->first();
        
        $localidades = Localidad::all();

        return view('configuracion.index',
            compact(
                'ultima_configuracion',
                'localidades',
            )
        );
    }






    // public function create(){
    //     return view('propietario.create');
    // }






    public function update(Request $request, $id){

        // dd($request->file('logo'));

        $resultado = $this->configuracionService->updateConfiguracion($request);

        if($resultado == false){

            return Redirect::to('configuracion')->with('status', 'Nada se modificó ya que no indicó ningún cambio');    
        }

        return Redirect::to('configuracion')->with('success', 'Configuración actualizada con éxito');
    }





    // public function store(PropietarioFormRequest $request){
        
    //     $propietario= new Propietario; 
    //     $propietario->propietario_nombre=$request->get('nombre');
    //     $propietario->propietario_apellido=$request->get('apellido');
    //     $propietario->propietario_cuit=$request->get('cuit');
    //     $propietario->alta=true;


    //     $propietario->save();
    //     return Redirect::to('propietario');
    // }







    // public function show($id){
    //     return view("propietario.show",["propietario"=>Propietario::findOrFail($id)]);
    // }







    // public function edit($id){
    //     return view("propietario.edit",["propietario"=>Propietario::findOrFail($id)]);
    // }











    
    // public function destroy($id){
    //     $propietario=Propietario::findOrFail($id);
    //     $propietario->alta=false;
    //     $propietario->update();

    //     return Redirect::to('propietario');
    // }
}
