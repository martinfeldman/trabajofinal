<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Models\User;
use App\Repositories\UserQueries;
use App\Services\UserService;
use Illuminate\Support\Facades\DB;

class UsuarioController extends Controller {


    protected $userService;
    protected $userQueries;




    public function __construct(UserService $userService, UserQueries $userQueries) {
        $this->middleware('auth');
        $this->userService = $userService;
        $this->userQueries = $userQueries;
    }





    public function index(Request $request) {

        if (Auth()->user()->can('Ver Usuarios en sidebar')) {

            // dd($request);
            // $mnf = User::find(5);
            // dd("permisos de MNF: ". $mnf->getPermissionNames());

            $users  = User::all();
            // $users  = $this->userQueries->getUsers();
            $user   = null;

            $roles = Role::all();
            $permisosAdministrativo = Role::findByName('administrativo')->permissions;
            // $permisosSupervisor = Role::findByName('supervisor')->permissions;
            $permisosProfesional = Role::findByName('profesional')->permissions;
            // $permisosUnicosSupervisor = $permisosSupervisor->diff($permisosAdministrativo);
            $permisosUnicosSupervisor = Role::findByName('supervisor')->permissions->diff($permisosAdministrativo);

            // $totalRegistros = $permisosAdministrativo->count();
            // $totalRegistros += $permisosUnicosSupervisor->count();
            // $totalRegistros += $permisosProfesional->count();
            // dd($totalRegistros);

            return view('usuario.index',
                compact(
                    'users',
                    'user',
                    'roles',
                    'permisosAdministrativo',
                    'permisosUnicosSupervisor',
                    'permisosProfesional',
                )
            );

        } else {

            return Redirect::to('expedientes')->with('error', 'Acceso no permitido.');      

        }
            
        
    }






    public function search(Request $request , $id = null ) {

        // dd($request);

        // dd($request);
        if (Auth()->user()->can('Ver Usuarios en sidebar')) {    

            if($request->input('user_id')){
                $user = User::findOrFail($request->input('user_id'));
                $users = User::all();
                // debug($user);
            } elseif ($request->input('rol')) {
                $user = null;
                $users = $this->userQueries->getUsersByRole($request);
            } else {
                $user   = User::findOrFail($id);
                $users  = User::all();
            }

            $roles = Role::all();
            $permisosAdministrativo = Role::findByName('administrativo')->permissions;
            $permisosSupervisor = Role::findByName('supervisor')->permissions;
            $permisosProfesional = Role::findByName('profesional')->permissions;
            $permisosUnicosSupervisor = $permisosSupervisor->diff($permisosAdministrativo);



            return view('usuario.index',
                compact(
                    'users',
                    'user',
                    'roles',
                    'permisosAdministrativo',
                    'permisosProfesional',
                    'permisosUnicosSupervisor',
                )
            );

        } else {
            return Redirect::to('expedientes')->with('error', 'Acceso no permitido.');      
        }
    }
            





    public function show(Request $request , $id = null ){

        if (Auth()->user()->can('Ver Usuarios en sidebar')) {    

            
            $user   = User::findOrFail($id);
            $users  = User::all();
        

            $roles = Role::all();
            $permisosAdministrativo = Role::findByName('administrativo')->permissions;
            $permisosSupervisor = Role::findByName('supervisor')->permissions;
            $permisosProfesional = Role::findByName('profesional')->permissions;
            $permisosUnicosSupervisor = $permisosSupervisor->diff($permisosAdministrativo);



            return view('usuario.index',
                compact(
                    'users',
                    'user',
                    'roles',
                    'permisosAdministrativo',
                    'permisosProfesional',
                    'permisosUnicosSupervisor',
                )
            );

        } else {

            return Redirect::to('expedientes')->with('error', 'Acceso no permitido.');      

        }
    }









    public function update(Request $request, $id){

        // dd($request);

        $user= $this->userService->updateUser($request, $id);
        if($user === false){
            return redirect()->back()->with('status', 'Nada se modificó ya que no indicó algún cambio',);
        }

        return redirect()->back()->with('success','Su perfil ha sido modificado con éxito');
    }








    public function updateRolesPermisos(Request $request, $id){

        // dd($request);

        $user = $this->userService->updateUserRolesPermisos($request, $id);
        if($user === false){
            return Redirect::to('usuarios')->with('status', 'Nada se modificó ya que no indicó algún cambio',);
        } elseif (is_string($user)) {

            return Redirect::to('usuarios')->with('success', $user);

        }

        return Redirect::to('usuarios')->with('success','Usuario ha sido modificado con éxito');
    }


    // public function create(){

    //     return view('propietario.create');
    // }


    


    // public function store(PropietarioFormRequest $requestPropietario){

    //     $propietario = $this->propietarioService->createPropietario($requestPropietario);
    //     /* var_dump($propietario); */

    //     /* dd($requestPropietario); */
    //     /* dd($propietario); */

    //     if ($propietario == null) {

    //         return redirect()->back()->with('error','Ya ha agregado anteriormente al Propietario que intenta registrar.');
    //     } 

    //     return Redirect::to('propietarios')->with('success','Propietario agregado con éxito.');
    // }





    // public function show($id){

    //     return view("propietarios.show",["propietario"=>Propietario::findOrFail($id)]);
    // }






    // public function edit($id){

    //     $propietario= Propietario::findOrFail($id);

    //     return view("propietario.modalEdit", $propietario);
    // }




    
    // public function destroy($id){
        
    //     $propietario= $this->propietarioService->deletePropietario($id);

    //     return Redirect::to('propietarios')->with('success','Propietario ha sido eliminado con éxito.');
    // }






}
