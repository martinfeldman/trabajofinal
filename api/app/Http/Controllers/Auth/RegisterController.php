<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\Models\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;





class RegisterController extends Controller {
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }




    
    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */

    protected function validator(array $data) {
        return Validator::make($data, [
            'name' => ['required', 'string', 'regex:/^[\pL\s\-]+$/u', 'min:1', 'max:100'],
            'last_name' => ['required', 'string', 'regex:/^[\pL\s\-]+$/u', 'min:1', 'max:100'],
            'cuit' => ['required', 'alpha_dash', 'min:1', 'max:30'],
            'numero_matricula' => ['numeric', 'min:1'],
            'email' => ['required', 'string', 'unique:App\Models\User,email', 'max:255'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }




    
    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Models\User
     */

    protected function create(array $data){

        try{

            DB::beginTransaction();

            // dd($data);
            $user = User::create([
                'name' => trim(strtoupper($data['name'])),
                'last_name' => trim(strtoupper($data['last_name'])),
                'email' => trim(strtoupper($data['email'])),
                'cuit' => trim(strtoupper($data['cuit'])),
                'numero_matricula' => trim($data['numero_matricula']),
                'password' => Hash::make($data['password']),
            ])/* ->syncRoles('profesional') */;


            // dd($user);

            DB::commit();
            
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        } catch (\Throwable $e) {
            DB::rollback();
            throw $e;
        }

            return $user;
            // return $this->redirectTo();

            // original
            // return User::create([
            //     'name' => strtoupper($data['name']),
            //     'email' => strtoupper($data['email']),
            //     'password' => Hash::make($data['password']),
            // ]);
    }





    public function redirectTo(){

        return $this->redirectTo;
    }

}
