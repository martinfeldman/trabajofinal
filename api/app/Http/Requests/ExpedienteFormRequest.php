<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;




class ExpedienteFormRequest extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */

    public function authorize() {

        return true;
    }




    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules() {
        

        $formRequests = [
            PlanoFormRequest::class,
            ObraFormRequest::class,
        ];
    
        $rules = [
            
            'objeto_id'             =>'required',
            'tipologia_id'          =>'required',
            'superficie_a_construir'=>'required_if:objeto_id,==,1', 
            'superficie_con_permiso'=>'required_if:objeto_id,==,2', 
            'superficie_sin_permiso'=>'required_if:objeto_id,==,2', 

        ];
    
        foreach ($formRequests as $source) {
            $rules = array_merge(
                $rules,
                (new $source)->rules()
            );
        }
    
        return $rules;

    }


}
