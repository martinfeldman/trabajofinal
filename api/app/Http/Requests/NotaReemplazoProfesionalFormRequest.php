<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;





class NotaReemplazoProfesionalFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'profesional_id'=>'required',
            'propietario_id'=>'required',

            'localidad_id'=>'required',
            'seccion'=>'required|numeric|max:999999',
            'chacra'=>'required|numeric|max:999999',
            'manzana'=>'required|numeric|max:999999',
            'parcela'=>'required|numeric|max:999999',

            'calle'=>'required|alpha_spaces|regex:/^[\pL\s\-]+$/u|min:3|max:80',
            'numero'=>'required|numeric|max:999999',
            'barrio'=>'required|alpha_spaces|regex:/^[\pL\s\-]+$/u|min:3|max:100',

            'avance_obra' => 'required|min:0|max:100',

            'pruebaAvance1'=>'required|file|max:10240',
            'pruebaAvance2'=>'required|file|max:10240',
            
        ];
    }
}
