<?php

namespace App\Http\Livewire;

use Livewire\Component;





class EditarUsuarioTablaPermisos extends Component {




    public $user, $roles, $permisosAdministrativo, $permisosSupervisor, $permisosUnicosSupervisor, $permisosProfesional;
    public $administrativoCheckBoxIsChecked, $supervisorCheckBoxIsChecked, $administrativoHibridoCheckBoxIsChecked, $profesionalCheckBoxIsChecked, $propietarioCheckBoxIsChecked;
    public $todosPermisosAdministradorCheckBoxIsChecked, $todosPermisosSupervisorCheckBoxIsChecked; 
    public $userIsNotProfesional;

    // public function hydrate(){
    //     dd($this->users);
    // }

    public function mount($user, /* $userSelected, */ $roles , $permisosAdministrativo,  $permisosUnicosSupervisor, $permisosProfesional  ){
        $this->user                     = $user;
        // dd($this->users);
        // $this->userSelected             = $userSelected;
        $this->roles                    = $roles;
        $this->permisosAdministrativo   = $permisosAdministrativo;
        // $this->permisosSupervisor       = $permisosSupervisor;
        $this->permisosProfesional      = $permisosProfesional;
        $this->permisosUnicosSupervisor = $permisosUnicosSupervisor;

        $this->administrativoCheckBoxIsChecked = false;
        $this->supervisorCheckBoxIsChecked = false;
        $this->profesionalCheckBoxIsChecked = false;
        $this->propietarioCheckBoxIsChecked = false;
        $this->administrativoHibridoCheckBoxIsChecked = false;

        // dd($user);

        // cambiando la forma en que obtengo los roles

        foreach($user->roles as $role){
            
            if($role->name === 'administrativo'){
                $this->administrativoCheckBoxIsChecked = true;
            }
    
            if($role->name === 'supervisor'){
                $this->supervisorCheckBoxIsChecked = true;
            }
    
            if($role->name === 'profesional'){
                $this->profesionalCheckBoxIsChecked = true;
            }
    
            if($role->name === 'propietario'){
                $this->propietarioCheckBoxIsChecked = true;
            }
    
            if($role->name === 'administrativoHibrido'){
                $this->administrativoHibridoCheckBoxIsChecked = true;
            }
        }







        // if($user->hasRole('administrativo')){
        //     $this->administrativoCheckBoxIsChecked = true;
        // }

        // if($user->hasRole('supervisor')){
        //     $this->supervisorCheckBoxIsChecked = true;
        // }

        // if($user->hasRole('profesional')){
        //     $this->profesionalCheckBoxIsChecked = true;
        // }

        // if($user->hasRole('propietario')){
        //     $this->propietarioCheckBoxIsChecked = true;
        // }

        // if($user->hasRole('administrativoHibrido')){
        //     $this->administrativoHibridoCheckBoxIsChecked = true;
        // }

        // $this->render();
        // dd($this->users);
    }


    public function profesionalCheckBoxHasBeenChecked(){
        $this->profesionalCheckBoxIsChecked === true ? $this->profesionalCheckBoxIsChecked = false : $this->profesionalCheckBoxIsChecked = true;
        if($this->profesionalCheckBoxIsChecked && !$this->user->profesional_id){
            $this->userIsNotProfesional = true;
        }
        $this->render();
    }

    public function propietarioCheckBoxHasBeenChecked(){
        $this->propietarioCheckBoxIsChecked === true ? $this->propietarioCheckBoxIsChecked = false : $this->propietarioCheckBoxIsChecked = true;
        $this->render();
    }

    public function administrativoCheckBoxHasBeenChecked(){
        $this->administrativoCheckBoxIsChecked === true ? $this->administrativoCheckBoxIsChecked = false : $this->administrativoCheckBoxIsChecked = true;
        // debug("Valor de isAdministrativoCheckBox : ".  $this->administrativoCheckBoxIsChecked);
        $this->render();
    }

    public function supervisorCheckBoxHasBeenChecked(){
        $this->supervisorCheckBoxIsChecked === true ? $this->supervisorCheckBoxIsChecked = false : $this->supervisorCheckBoxIsChecked = true;
        $this->render();
    }

    public function administrativoHibridoCheckBoxHasBeenChecked(){
        $this->administrativoHibridoCheckBoxIsChecked === true ? $this->administrativoHibridoCheckBoxIsChecked = false : $this->administrativoHibridoCheckBoxIsChecked = true;
        $this->render();
    }


    // public function todosPermisosAdministradorCheckBoxHasBeenChecked(){
    //     $this->todosPermisosAdministradorCheckBoxIsChecked === true ? $this->todosPermisosAdministradorCheckBoxIsChecked = false : $this->todosPermisosAdministradorCheckBoxIsChecked = true;
    //     $this->render();
    // }


    // public function todosPermisosSupervisorCheckBoxHasBeenChecked(){
    //     $this->todosPermisosSupervisorCheckBoxIsChecked === true ? $this->todosPermisosSupervisorCheckBoxIsChecked = false : $this->todosPermisosSupervisorCheckBoxIsChecked = true;
    //     $this->render();
    // }


    public function render(){
        return view('livewire.editar-usuario-tabla-permisos');
    }


}
