




    const PlanosODocumentosCheckbox = document.querySelector("#PlanosODocumentosCheckbox");
    const aprobadoCheckbox = document.querySelector("#aprobadoCheckbox");
    const desaprobadoCheckbox = document.querySelector("#desaprobadoCheckbox");


    const motivoInput = document.querySelector("#motivoInput");
    const motivoComboBox = document.querySelector("#motivoComboBox");

    const PlanosODocumentosLabel = document.querySelector("#PlanosODocumentosLabel");



    
    function toggleCheckboxs(){

        
        
        switch(true){

            case PlanosODocumentosCheckbox.checked:

                aprobadoCheckbox.disabled = true;
                desaprobadoCheckbox.disabled = true;
                motivoInput.disabled = false;
                motivoComboBox.disabled = false;

                break;

            case aprobadoCheckbox.checked:

                PlanosODocumentosCheckbox.disabled = true
                desaprobadoCheckbox.disabled = true;
                motivoInput.disabled = true;
                motivoComboBox.disabled = true;

                break;

            case desaprobadoCheckbox.checked:

                PlanosODocumentosCheckbox.disabled = true
                aprobadoCheckbox.disabled = true;
                motivoInput.disabled = false;
                motivoComboBox.disabled = false;

                break;

            default: 

                PlanosODocumentosCheckbox.disabled = false;
                aprobadoCheckbox.disabled = false;
                desaprobadoCheckbox.disabled = false;
                motivoInput.disabled = false;
                motivoComboBox.disabled = false;
                
               


        }


        
    }
    
  