<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use Illuminate\Support\Facades\DB;





class ObjetoSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run() {


        // OBJETOS DE OBRA


        DB::table('objetos')->insert([
            'objeto' => 'Nuevo',
        ]);


        DB::table('objetos')->insert([
            'objeto' => 'Existente',
        ]);
        

    }
}
