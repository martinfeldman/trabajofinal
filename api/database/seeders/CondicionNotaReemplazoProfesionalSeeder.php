<?php

namespace Database\Seeders;


use Illuminate\Database\Seeder;




use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;








class CondicionNotaReemplazoProfesionalSeeder extends Seeder {


    /**
     * Seed the application's database.
     *
     * @return void
     */

    public function run(){


        // CONDICIONES NOTA REEMPLAZO PROFESIONAL

        DB::table('condiciones_nota_reemplazo_profesional')->insert([
            'condicion_nota_reemplazo_profesional' => 'Nota creada. Debe añadir firmas, escanear y luego cargar el documento',
        ]);



        DB::table('condiciones_nota_reemplazo_profesional')->insert([
            'condicion_nota_reemplazo_profesional' => 'En evaluación',
        ]);

        

        DB::table('condiciones_nota_reemplazo_profesional')->insert([
            'condicion_nota_reemplazo_profesional' => 'Aprobada',
        ]);



        DB::table('condiciones_nota_reemplazo_profesional')->insert([
            'condicion_nota_reemplazo_profesional' => 'Desaprobada',
        ]);


        
    }



}
