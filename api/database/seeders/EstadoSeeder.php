<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use Illuminate\Support\Facades\DB;





class EstadoSeeder extends Seeder {


    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run() {


        // ESTADOS DE OBRA

        DB::table('estados')->insert([
            'estado' => 'Trámite inconcluso',
        ]);



        DB::table('estados')->insert([
            'estado' => 'Para revisión',
        ]);


        DB::table('estados')->insert([
            'estado' => 'Abierto',
        ]);


        DB::table('estados')->insert([
            'estado' => 'Cerrado',
        ]);
        

    }
}
