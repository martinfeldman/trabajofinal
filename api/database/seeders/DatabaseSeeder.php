<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;







class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run() {
        
        $this->call(ProfesionalSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(CondicionSeeder::class);
        $this->call(EstadoSeeder::class);
        $this->call(LocalidadSeeder::class);
        $this->call(ObjetoSeeder::class);
        $this->call(RoleSeeder::class);
        $this->call(TipologiaSeeder::class);
        $this->call(TiposTareasSeeder::class);
        $this->call(CondicionNotaReemplazoProfesionalSeeder::class);
       
        

    }
}
