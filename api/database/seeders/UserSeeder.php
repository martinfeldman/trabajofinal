<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;




use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;



class UserSeeder extends Seeder {

    /**
     * Seed the application's database.
     *
     * @return void
     */
    
    public function run(){



         // SUPERVISORES


        DB::table('users')->insert([
            'name'              => 'supervisor1',
            'last_name'         => '',
            'cuit'              => '20-20111000-1',
            'email'             => 'supervisor1@gmail.com',
            'password'          => Hash::make('facil'),

            // 'role'              => 'supervisor',
        ]);


        DB::table('users')->insert([
            'name'              => 'supervisor2',
            'last_name'         => '',
            'cuit'              => '20-20111000-2',
            'email'             => 'supervisor2@gmail.com',
            'password'          => Hash::make('facil'),
            // 'role'              => 'supervisor',
        ]);




        // EMPLEADOS ADMINISTRATIVOS


        DB::table('users')->insert([
            'name'              => 'administrativo1',
            'last_name'         => '',
            'cuit'              => '20-20111000-3',
            'email'             => 'administrativo1@gmail.com',
            'password'          => Hash::make('facil'),
            // 'role'              => 'administrativo',
        ]);


        DB::table('users')->insert([
            'name'              => 'administrativo2',
            'last_name'         => '',
            'cuit'              => '20-20111000-4',
            'email'             => 'administrativo2@gmail.com',
            'password'          => Hash::make('facil'),
            // 'role'              => 'administrativo',
        ]);


        


        // PROFESIONALES


        DB::table('users')->insert([
            'name'              => 'Martin',
            'last_name'         => 'Feldman',
            'cuit'              => '20-20111000-5',
            'email'             => 'mnf.feldman@gmail.com',
            'password'          => Hash::make('facil'),
            // 'role'              => 'profesional',
            'profesional_id'     => 1,
        ]);


        DB::table('users')->insert([
            'name'              => 'Ariana',
            'last_name'         => 'Duarte',
            'cuit'              => '20-20111000-6',
            'email'             => 'arianaduarte@gmail.com',
            'password'          => Hash::make('facil'),
            // 'role'              => 'profesional',
            'profesional_id'    => 2,
        ]);


        DB::table('users')->insert([
            'name'              => 'Marcos',
            'last_name'         => 'Ferreira',
            'cuit'              => '20-20111000-7',
            'email'             => 'mnf.fceqyn@gmail.com',
            'password'          => Hash::make('facil'),
            // 'role'              => 'profesional',
            'profesional_id'    => 3,
        ]);


        DB::table('users')->insert([
            'name'              => 'Arquimedes de',
            'last_name'         => 'de Siracusa',
            'cuit'              => '20-20111000-8',
            'email'             => 'arquimedes@gmail.com',
            'password'          => Hash::make('facil'),
            // 'role'              => 'profesional',
            'profesional_id'    => 4,
        ]);


        DB::table('users')->insert([
            'name'              => 'Arturo',
            'last_name'         => 'Lopez',
            'cuit'              => '20-20111000-9',
            'email'             => 'arturo.lopez@gmail.com',
            'password'          => Hash::make('facil'),
            // 'role'              => 'profesional',
            'profesional_id'    => 5,
        ]);


        DB::table('users')->insert([
            'name'              => 'Aristóbulo',
            'last_name'         => 'Sosa',
            'cuit'              => '20-20222000-0',
            'email'             => 'aristobulo.sosa@gmail.com',
            'password'          => Hash::make('facil'),
            // 'role'              => 'profesional',
            'profesional_id'    => 6,
        ]);


    }
}
