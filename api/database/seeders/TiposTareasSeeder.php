<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use Illuminate\Support\Facades\DB;





class TiposTareasSeeder extends Seeder {


    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run() {


        // TIPOS DE TAREAS


        // REQUIEREN PLANOS {

        DB::table('tipos_tareas')->insert([
            'tipo_tarea' => 'Anteproyecto',
        ]);



        DB::table('tipos_tareas')->insert([
            'tipo_tarea' => 'Proyecto',
        ]);
        


        DB::table('tipos_tareas')->insert([
            'tipo_tarea' => 'Dirección de Obra',
        ]);



        DB::table('tipos_tareas')->insert([
            'tipo_tarea' => 'Relevamiento',
        ]);
        

        
        DB::table('tipos_tareas')->insert([
            'tipo_tarea' => 'Remodelación',
        ]);



        DB::table('tipos_tareas')->insert([
            'tipo_tarea' => 'Instalación Eléctrica',
        ]);
        


        DB::table('tipos_tareas')->insert([
            'tipo_tarea' => 'Instalación Gas',
        ]);



        DB::table('tipos_tareas')->insert([
            'tipo_tarea' => 'Instalación Agua, Cloaca',
        ]);



        DB::table('tipos_tareas')->insert([
            'tipo_tarea' => 'Instalación Contra Incendios',
        ]);



        DB::table('tipos_tareas')->insert([
            'tipo_tarea' => 'Ascensores',
        ]);



        DB::table('tipos_tareas')->insert([
            'tipo_tarea' => 'Cálculo de estructura ',
        ]);


        //  }



        // REQUIEREN DOCUMENTOS {



        DB::table('tipos_tareas')->insert([
            'tipo_tarea' => 'Plan regulado',
        ]);



        DB::table('tipos_tareas')->insert([
            'tipo_tarea' => 'Peritaje',
        ]);



        DB::table('tipos_tareas')->insert([
            'tipo_tarea' => 'Representación técnica',
        ]);



        DB::table('tipos_tareas')->insert([
            'tipo_tarea' => 'Tasación',
        ]);


        //  }

    }
}
