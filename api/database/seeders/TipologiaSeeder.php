<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use Illuminate\Support\Facades\DB;





class TipologiaSeeder extends Seeder {


    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run() {


        // TIPOLOGIAS DE OBRA


        DB::table('tipologias')->insert([
            'tipologia' => 'Vivienda Unifamiliar',
        ]);



        DB::table('tipologias')->insert([
            'tipologia' => 'Vivienda Colectiva',
        ]);
        


        DB::table('tipologias')->insert([
            'tipologia' => 'Equipamientos Comerciales',
        ]);



        DB::table('tipologias')->insert([
            'tipologia' => 'Equipamientos Culturales',
        ]);
        


        DB::table('tipologias')->insert([
            'tipologia' => 'Equipamientos Educativos',
        ]);



        DB::table('tipologias')->insert([
            'tipologia' => 'Equipamientos Industriales',
        ]);
        


        DB::table('tipologias')->insert([
            'tipologia' => 'Obras Especiales',
        ]);




    }
}
