<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;




use Illuminate\Support\Facades\DB;











class ProfesionalSeeder extends Seeder {


    /**
     * Seed the application's database.
     *
     * @return void
     */

    public function run(){


        // PROFESIONALES


        DB::table('profesionales')->insert([
            'profesional_nombres' => 'MARTIN',
            'profesional_apellidos' => 'FELDMAN',
            'profesional_numero_matricula' => '11111111',
            'profesional_cuit' => '11000111',
            'fecha_registro' => Carbon::now()->setTimeZone('America/Argentina/Buenos_Aires')->format('Y-m-d H:i:s'),
            'alta' => '0',
        ]);



        DB::table('profesionales')->insert([
            'profesional_nombres' => 'ARIANA',
            'profesional_apellidos' => 'DUARTE',
            'profesional_numero_matricula' => '2222222',
            'profesional_cuit' => '22000222',
            'fecha_registro' => Carbon::now()->setTimeZone('America/Argentina/Buenos_Aires')->format('Y-m-d H:i:s'),
            'alta' => '0',
        ]);



        DB::table('profesionales')->insert([
            'profesional_nombres' => 'MARCOS',
            'profesional_apellidos' => 'FERREIRA',
            'profesional_numero_matricula' => '33333333',
            'profesional_cuit' => '33033333',  
            'fecha_registro' => Carbon::now()->setTimeZone('America/Argentina/Buenos_Aires')->format('Y-m-d H:i:s'),
            'alta' => '0',          
        ]);



        DB::table('profesionales')->insert([
            'profesional_nombres' => 'ARQUIMEDES',
            'profesional_apellidos' => 'DE SIRACUSA',
            'profesional_numero_matricula' => '4444444',
            'profesional_cuit' => '44404444',
            'fecha_registro' => Carbon::now()->setTimeZone('America/Argentina/Buenos_Aires')->format('Y-m-d H:i:s'),
            'alta' => '0',
        ]);



        DB::table('profesionales')->insert([
            'profesional_nombres' => 'ARTURO',
            'profesional_apellidos' => 'LOPEZ',
            'profesional_numero_matricula' => '55555555',
            'profesional_cuit' => '55505555',
            'fecha_registro' => Carbon::now()->setTimeZone('America/Argentina/Buenos_Aires')->format('Y-m-d H:i:s'),
            'alta' => '0',
            
        ]);



        DB::table('profesionales')->insert([
            'profesional_nombres' => 'ARISTÓBULO',
            'profesional_apellidos' => 'SOSA',
            'profesional_numero_matricula' => '6666666',
            'profesional_cuit' => '66006666',
            'fecha_registro' => Carbon::now()->setTimeZone('America/Argentina/Buenos_Aires')->format('Y-m-d H:i:s'),
            'alta' => '0',
        ]);


    }



}
