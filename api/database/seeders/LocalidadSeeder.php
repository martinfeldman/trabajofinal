<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use Illuminate\Support\Facades\DB;





class LocalidadSeeder extends Seeder {


    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run() {


        // LOCALIDADES

        DB::table('localidades')->insert([
            'localidad' => 'Andresito',
        ]);



        DB::table('localidades')->insert([
            'localidad' => 'Apóstoles',
        ]);
       
        

        DB::table('localidades')->insert([
            'localidad' => 'Aristóbulo del Valle',
        ]);



        DB::table('localidades')->insert([
            'localidad' => 'Bernardo de Irigoyen',
        ]);



        DB::table('localidades')->insert([
            'localidad' => 'Candelaria',
        ]);



        DB::table('localidades')->insert([
            'localidad' => 'El Dorado',
        ]);
        


        DB::table('localidades')->insert([
            'localidad' => 'Garupá',
        ]);



        DB::table('localidades')->insert([
            'localidad' => 'Jardín América',
        ]);
        


        DB::table('localidades')->insert([
            'localidad' => 'Montecarlo',
        ]);
        


        DB::table('localidades')->insert([
            'localidad' => 'Oberá',
        ]);



        DB::table('localidades')->insert([
            'localidad' => 'Posadas',
        ]);



        DB::table('localidades')->insert([
            'localidad' => 'San Ignacio',
        ]);
        


        DB::table('localidades')->insert([
            'localidad' => 'Wanda',
        ]);
    }
}
