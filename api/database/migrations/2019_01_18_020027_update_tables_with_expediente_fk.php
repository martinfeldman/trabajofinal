<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;






class UpdateTablesWithExpedienteFK extends Migration {




    /**
     * Run the migrations.
     *
     * @return void
     */


    public function up()  {

        
/* 
        Schema::table('profesionales', function (Blueprint $table) {
    
            $table->unsignedBigInteger('expediente_id')->nullable();
            $table->foreign('expediente_id')
            -> references ('expediente_id')
            -> on('expedientes'); 
        });



        Schema::table('profesionales', function (Blueprint $table) {

            $table->unsignedBigInteger('propietario_id')->nullable()->unique;
            $table->foreign('propietario_id')
            -> references ('id')
            -> on('propietarios'); 
        }); */



        /* Schema::table('profesionales', function (Blueprint $table) {

            $table->unsignedBigInteger('nota_reemplazo_profesional_id')->nullable()->unique;
            $table->foreign('nota_reemplazo_profesional_id')
            -> references ('nota_reemplazo_profesional_id')
            -> on('notas_reemplazo_profesional'); 
        }); */


/* 
        Schema::table('objetos', function (Blueprint $table) {
    
            $table->unsignedBigInteger('expediente_id')->nullable();
            $table->foreign('expediente_id')
            -> references ('expediente_id')
            -> on('expedientes'); 
        });
 */


/* 
        Schema::table('tipologias', function (Blueprint $table) {
    
            $table->unsignedBigInteger('expediente_id')->nullable();
            $table->foreign('expediente_id')
            -> references ('expediente_id')
            -> on('expedientes'); 
        });
 */




        /* Schema::table('estados', function (Blueprint $table) {
    
            $table->unsignedBigInteger('expediente_id')->nullable();
            $table->foreign('expediente_id')
            -> references ('expediente_id')
            -> on('expedientes'); 
        });
 */


/* 
        Schema::table('planos', function (Blueprint $table) {

            $table->unsignedBigInteger('tarea_id')->nullable();
            $table->foreign('tarea_id')
            -> references ('tarea_id')
            -> on('tareas'); 
        }); */



        /* Schema::table('tipos_tareas', function (Blueprint $table) {

            $table->unsignedBigInteger('tarea_id')->nullable();
            $table->foreign('tarea_id')
            -> references ('tarea_id')
            -> on('tareas'); 
        }); */



        /* Schema::table('tareas', function (Blueprint $table) {

            $table->unsignedBigInteger('expediente_id')->nullable();
            $table->foreign('expediente_id')
            -> references ('expediente_id')
            -> on('expedientes'); 
        }); */




        /* Schema::table('localidades', function (Blueprint $table) {
    
            $table->unsignedBigInteger('obra_id')->nullable();
            $table->foreign('obra_id')
            -> references ('obra_id')
            -> on('obras'); 
        }); */




        /* Schema::table('obras', function (Blueprint $table) {
    
            $table->unsignedBigInteger('expediente_id')->nullable();
            $table->foreign('expediente_id')
            -> references ('expediente_id')
            -> on('expedientes'); 
        }); */


/* 
        Schema::table('comprobantes_pago', function (Blueprint $table) {
    
            $table->unsignedBigInteger('expediente_id')->nullable();
            $table->foreign('expediente_id')
            -> references ('expediente_id')
            -> on('expedientes'); 
        }); */



        /* Schema::table('condiciones', function (Blueprint $table) {
    
            $table->unsignedBigInteger('expediente_id')->nullable();
            $table->foreign('expediente_id')
            -> references ('expediente_id')
            -> on('expedientes'); 
        }); */



        Schema::table('historial_condiciones', function (Blueprint $table) {
    
            $table->unsignedBigInteger('expediente_id')->nullable();
            $table->foreign('expediente_id')
            -> references ('expediente_id')
            -> on('expedientes'); 
        });




        Schema::table('notas_reemplazo_profesional', function (Blueprint $table) {
    
            $table->unsignedBigInteger('expediente_afectado_id')->nullable(false)->unique;
            $table->foreign('expediente_afectado_id')
            -> references ('expediente_id')
            -> on('expedientes'); 
        });




        Schema::table('certificados', function (Blueprint $table) {
    
            $table->unsignedBigInteger('expediente_id')->nullable();
            $table->foreign('expediente_id')
            -> references ('expediente_id')
            -> on('expedientes'); 
        });




        Schema::table('users', function (Blueprint $table) {

            $table->uuid('notification_id')->nullable();
            $table->foreign('notification_id')
            -> references ('id')
            -> on('notifications'); 

        });



        
    }




    /**
     * Reverse the migrations.
     *
     * @return void
     */

    public function down()  {

        Schema::dropIfExists('expedientes');
    }
}
