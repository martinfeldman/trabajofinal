<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;






class CreateComprobantesPagoTable extends Migration {



    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up() {

    
        Schema::create('comprobantes_pago', function (Blueprint $table) {

            $table->bigIncrements('comprobante_pago_id');

            $table->binary('comprobante_pago')->nullable(false);
            $table->string('nombre_archivo', 150)->nullable(false);

            $table->timestamp('fecha')->nullable();
            
            /* $table->unsignedBigInteger('expediente_id')->nullable(false)->unique;
            $table->foreign('expediente_id')
            ->references ('expediente_id')
            ->on('expedientes');  */



            //$table->timestamps();

        });
    }



    

    /**
     * Reverse the migrations.
     *
     * @return void
     */

    public function down() {

        Schema::dropIfExists('comprobantes_pago');
    }
}
