<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;







class CreateTipologiasTable extends Migration {


    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up() {

        Schema::create('tipologias', function (Blueprint $table) {

            $table->bigIncrements('tipologia_id');

            $table->string('tipologia', 70);
   
            /* $table->unsignedBigInteger('expediente_id')->nullable();
            $table->foreign('expediente_id')
            -> references ('expediente_id')
            -> on('expedientes');  */


            //$table->timestamps();
        });

    }




    
    /**
     * Reverse the migrations.
     *
     * @return void
     */

    public function down() {

        Schema::dropIfExists('tipologias');
    }
}
