<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;








class CreateObjetosTable extends Migration {


    
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up() {

        Schema::create('objetos', function (Blueprint $table) {

            $table->bigIncrements('objeto_id');

            $table->string('objeto', 10)->nullable(false);


            /* $table->unsignedBigInteger('expediente_id')->nullable();
            $table->foreign('expediente_id')
            -> references ('expediente_id')
            -> on('expedientes');  */

            //$table->timestamps();

        });

    }




    /**
     * Reverse the migrations.
     *
     * @return void
     */

    public function down() {

        Schema::dropIfExists('objetos');
    }
}
