<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;






class CreateCertificadosTable extends Migration {


    
    /**
     * Run the migrations.
     *
     * @return void
     */


    public function up() {


        Schema::create('certificados', function (Blueprint $table) {


            $table->bigIncrements('certificado_id');

            $table->binary('certificado')->nullable(false);

            
            /* $table->unsignedBigInteger('expediente_id')->nullable(false)->unique;
            $table->foreign('expediente_id')
            -> references ('expediente_id')
            -> on('expedientes');  */



            $table->binary('codigo_qr')->nullable(false);

            $table->unsignedBigInteger('random_number')->nullable(false);


            $table->timestamp('fecha')->nullable();

            $table->timestamps();


        });
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */

    public function down() {

        Schema::dropIfExists('certificados');
    }
}
