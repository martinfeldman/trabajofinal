<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;








class CreateElementosConfigurablesTable extends Migration {


    
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up() {

        Schema::create('elementos_configurables', function (Blueprint $table) {

            $table->bigIncrements('id');

            $table->string('nombre_institucion', 256)->nullable();
            $table->string('email', 70)->nullable();
            $table->string('localidad', 256)->nullable();
            $table->string('calle', 256)->nullable();
            $table->integer('numero')->nullable();
            
            $table->binary('logo')->nullable();
            $table->string('nombre_archivo_logo', 256)->nullable();



            $table->integer('tiempo_primer_recordatorio_1er_proceso_automatizado')->nullable();
            $table->integer('tiempo_segundo_recordatorio_1er_proceso_automatizado')->nullable();
            $table->integer('tiempo_cerrar_expediente_1er_proceso_automatizado')->nullable();

            $table->timestamps();

        });

    }




    /**
     * Reverse the migrations.
     *
     * @return void
     */

    public function down() {

        Schema::dropIfExists('elementos_configurables');
    }
}
