<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Application Name
    |--------------------------------------------------------------------------
    |
    | This value is the name of your application. This value is used when the
    | framework needs to place the application's name in a notification or
    | any other location as required by the application or its packages.
    |
    */

    'name' => env('APP_NAME', 'Laravel'),

    /*
    |--------------------------------------------------------------------------
    | Application Environment
    |--------------------------------------------------------------------------
    |
    | This value determines the "environment" your application is currently
    | running in. This may determine how you prefer to configure various
    | services the application utilizes. Set this in your ".env" file.
    |
    */

    'env' => env('APP_ENV', 'production'),

    /*
    |--------------------------------------------------------------------------
    | Application Debug Mode
    |--------------------------------------------------------------------------
    |
    | When your application is in debug mode, detailed error messages with
    | stack traces will be shown on every error that occurs within your
    | application. If disabled, a simple generic error page is shown.
    |
    */

    'debug' => (bool) env('APP_DEBUG', false),

    /*
    |--------------------------------------------------------------------------
    | Application URL
    |--------------------------------------------------------------------------
    |
    | This URL is used by the console to properly generate URLs when using
    | the Artisan command line tool. You should set this to the root of
    | your application so that it is used when running Artisan tasks.
    |
    */

    'url' => env('APP_URL', 'http://localhost'),

    'asset_url' => env('ASSET_URL', null),

    /*
    |--------------------------------------------------------------------------
    | Application Timezone
    |--------------------------------------------------------------------------
    |
    | Here you may specify the default timezone for your application, which
    | will be used by the PHP date and date-time functions. We have gone
    | ahead and set this to a sensible default for you out of the box.
    |
    */

    'timezone' => 'America/Argentina/Buenos_Aires',
    // 'timezone' => 'UTC',

    /*
    |--------------------------------------------------------------------------
    | Application Locale Configuration
    |--------------------------------------------------------------------------
    |
    | The application locale determines the default locale that will be used
    | by the translation service provider. You are free to set this value
    | to any of the locales which will be supported by the application.
    |
    */

    'locale' => 'es',

    /*
    |--------------------------------------------------------------------------
    | Application Fallback Locale
    |--------------------------------------------------------------------------
    |
    | The fallback locale determines the locale to use when the current one
    | is not available. You may change the value to correspond to any of
    | the language folders that are provided through your application.
    |
    */

    'fallback_locale' => 'en',

    /*
    |--------------------------------------------------------------------------
    | Faker Locale
    |--------------------------------------------------------------------------
    |
    | This locale will be used by the Faker PHP library when generating fake
    | data for your database seeds. For example, this will be used to get
    | localized telephone numbers, street address information and more.
    |
    */

    'faker_locale' => 'en_US',

    /*
    |--------------------------------------------------------------------------
    | Encryption Key
    |--------------------------------------------------------------------------
    |
    | This key is used by the Illuminate encrypter service and should be set
    | to a random, 32 character string, otherwise these encrypted strings
    | will not be safe. Please do this before deploying an application!
    |
    */

    'key' => env('APP_KEY'),

    'cipher' => 'AES-256-CBC',







    /*
    |--------------------------------------------------------------------------
    | Environment Variables
    |--------------------------------------------------------------------------
    |
    |
    */





    // NUMERO_INICIAL_DE_EXPEDIENTES
    'numero_inicial_expedientes' => env('NUMERO_INICIAL_EXPEDIENTES', 1),



    // DATOS TRANSFERENCIA BANCARIA

    'cbu_banco' => env('CBU_BANCO', "01702046600000087865"),
    'cuit_colegio' => env('CUIT_COLEGIO', "102677777771"),



    // EXPEDIENTES_CREATE_URL
    'expedientes_create_url' => env('EXPEDIENTES_CREATE_URL', 'http://localhost:8000/expedientes/create'),





    // IDS EN BD DE CONDICIONES DE EXPEDIENTE

    'creado' => env('CREADO', 1),
    'formulario_a_revisar' => env('FORMULARIO_A_REVISAR', 2),
    'formulario_desaprobado' => env('FORMULARIO_DESAPROBADO', 3),
    'formulario_incompleto' => env('FORMULARIO_INCOMPLETO', 4),
    'para_liquidar' => env('PARA_LIQUIDAR', 5),
    'esperando_comprobante_1' => env('ESPERANDO_COMPROBANTE_1', 6),
    'esperando_comprobante_2' => env('ESPERANDO_COMPROBANTE_2', 7),
    'esperando_comprobante_3' => env('ESPERANDO_COMPROBANTE_3', 8),
    'para_cerrar' => env('PARA_CERRAR', 9),
    'cerrado' => env('CERRADO', 10),
    'cerrado_sin_dir_obra_finalizado' => env('CERRADO_SIN_DIR_OBRA_FINALIZADO', 11),
    'cerrado_con_dir_obra_finalizado' => env('CERRADO_SIN_DIR_OBRA_FINALIZADO', 12),
    'cerrado_por_falta_tareas_reemplazo_profesional' => env('CERRADO_POR_FALTA_TAREAS_REEMPLAZO_PROFESIONAL', 13),
    'cerrado_por_falta_tareas_renuncia' => env('CERRADO_POR_FALTA_TAREAS_RENUNCIA', 14),
    'cerrado_por_falta_comprobante_pago' => env('CERRADO_POR_FALTA_COMPROBANTE_PAGO', 15),




    // IDS EN BD DE ESTADOS DE EXPEDIENTE

    'tramite_inconcluso' => env('TRAMITE_INCONCLUSO', 1),
    'para_revisar' => env('PARA_REVISAR', 2),
    'estado_abierto' => env('ESTADO_ABIERTO', 3),
    'estado_cerrado' => env('ESTADO_CERRADO', 4),





    // IDS EN BD DE ESTADOS DE EXPEDIENTE

    'objeto_nuevo'      => env('OBJETO_NUEVO',      1),
    'objeto_existente'  => env('OBJETO_EXISTENTE',  2),







    // IDS EN BD DE CONDICIONES NOTA REEMPLAZO PROFESIONAL

    'nrp_faltan_firmas' => env('NRP_FALTAN_FIRMAS', 1),
    'nrp_para_revision' => env('NRP_PARA_REVISION', 2),
    'nrp_aprobada'      => env('NRP_APROBADA',      3),
    'nrp_desaprobada'   => env('NRP_DESAPROBADA',   4),








    // IDS DE TAREAS DE EXPEDIENTES

    'direccion_obra_tarea_id' => env('DIRECCION_OBRA_TAREA_ID', 3),






    // IDS DE CONDICIONES NECESARIAS PARA INFORMES
    'espera_sin_confirmacion' => env('ESPERA_SIN_CONFIRMACION', 6),
    'espera_con_confirmacion' => env('ESPERA_CON_CONFIRMACION', 7),
    'espera_ambos_notificados' => env('ESPERA_AMBOS_NOTIFICADOS', 8),








    # MOTIVOS PORQUE SE CONSIDERA INCOMPLETO UN EXPEDIENTE

    'motivo_error_en_archivo_entregado' => env('MOTIVO_ERROR_EN_ARCHIVO_ENTREGADO', "Error(es) dentro del archivo entregado"),


    # MOTIVOS PORQUE SE DESAPRUEBA UN EXPEDIENTE

    'motivo_error_en_datos_formulario_entregado' => env('MOTIVO_ERROR_EN_DATOS_FORMULARIO_ENTREGADO', "Error(es) en datos entregados"),
    
    




    # OPCIONES DEL FILTRO 'ROLES' EN INDICE DE USUARIOS
    
    'por_administrativo'                => env('POR_ADMINISTRATIVO','administrativo'),
    'por_administrativo_hibrido'        => env('POR_ADMINISTRATIVO_HIBRIDO', 'administrativoHibrido' ),
    'por_supervisor'                    => env('POR_SUPERVISOR', 'supervisor'),
    'por_profesional'                   => env('POR_PROFESIONAL', 'profesional'),
    'por_propietario'                   => env('POR_PROPIETARIO', 'propietario'),
    'por_profesional_y_propietario'     => env('POR_PROFESIONAL_Y_PROPIETARIO', "'profesional','propietario"),
    'por_administrativo_y_propietario'  => env('POR_ADMINISTRATIVO_Y_PROPIETARIO', "'administrativo','propietario"),
    'por_administrativo_y_profesional'  => env('POR_ADMINISTRATIVO_Y_PROFESIONAL', "'administrativo','profesional"),



    
    
    
    # THEME_COLOR

    'theme_color'       => env('THEME_COLOR', "#85d2ff"),
    'dropdown_color'    => env('DROPDOWN_COLOR', "#85d2ff"),
    'sidebar_color'    => env('SIDEBAR_COLOR', "#85d2ff"),












    /*
    |--------------------------------------------------------------------------
    | Autoloaded Service Providers
    |--------------------------------------------------------------------------
    |
    | The service providers listed here will be automatically loaded on the
    | request to your application. Feel free to add your own services to
    | this array to grant expanded functionality to your applications.
    |
    */

    'providers' => [

        /*
         * Laravel Framework Service Providers...
         */
        Illuminate\Auth\AuthServiceProvider::class,
        Illuminate\Broadcasting\BroadcastServiceProvider::class,
        Illuminate\Bus\BusServiceProvider::class,
        Illuminate\Cache\CacheServiceProvider::class,
        Illuminate\Foundation\Providers\ConsoleSupportServiceProvider::class,
        Illuminate\Cookie\CookieServiceProvider::class,
        Illuminate\Database\DatabaseServiceProvider::class,
        Illuminate\Encryption\EncryptionServiceProvider::class,
        Illuminate\Filesystem\FilesystemServiceProvider::class,
        Illuminate\Foundation\Providers\FoundationServiceProvider::class,
        Illuminate\Hashing\HashServiceProvider::class,
        Illuminate\Mail\MailServiceProvider::class,
        Illuminate\Notifications\NotificationServiceProvider::class,
        Illuminate\Pagination\PaginationServiceProvider::class,
        Illuminate\Pipeline\PipelineServiceProvider::class,
        Illuminate\Queue\QueueServiceProvider::class,
        Illuminate\Redis\RedisServiceProvider::class,
        Illuminate\Auth\Passwords\PasswordResetServiceProvider::class,
        Illuminate\Session\SessionServiceProvider::class,
        Illuminate\Translation\TranslationServiceProvider::class,
        Illuminate\Validation\ValidationServiceProvider::class,
        Illuminate\View\ViewServiceProvider::class,

        //added by MNF
        Barryvdh\DomPDF\ServiceProvider::class,
        SimpleSoftwareIO\QrCode\QrCodeServiceProvider::class,
        OwenIt\Auditing\AuditingServiceProvider::class,
        Barryvdh\Debugbar\ServiceProvider::class,
        // Spatie\Backup\BackupServiceProvider::class,

        /*
         * Package Service Providers...
         */

        UxWeb\SweetAlert\SweetAlertServiceProvider::class,


        /*
         * Application Service Providers...
         */
        App\Providers\AppServiceProvider::class,
        App\Providers\AuthServiceProvider::class,
        // App\Providers\BroadcastServiceProvider::class,
        App\Providers\EventServiceProvider::class,
        OwenIt\Auditing\AuditingServiceProvider::class,
        App\Providers\RouteServiceProvider::class,

    ],

    /*
    |--------------------------------------------------------------------------
    | Class Aliases
    |--------------------------------------------------------------------------
    |
    | This array of class aliases will be registered when this application
    | is started. However, feel free to register as many as you wish as
    | the aliases are "lazy" loaded so they don't hinder performance.
    |
    */

    'aliases' => [

        'Alert' => UxWeb\SweetAlert\SweetAlert::class,
        'App' => Illuminate\Support\Facades\App::class,
        'Arr' => Illuminate\Support\Arr::class,
        'Artisan' => Illuminate\Support\Facades\Artisan::class,
        'Auth' => Illuminate\Support\Facades\Auth::class,
        'Blade' => Illuminate\Support\Facades\Blade::class,
        'Broadcast' => Illuminate\Support\Facades\Broadcast::class,
        'Bus' => Illuminate\Support\Facades\Bus::class,
        'Cache' => Illuminate\Support\Facades\Cache::class,
        'Config' => Illuminate\Support\Facades\Config::class,
        'Cookie' => Illuminate\Support\Facades\Cookie::class,
        'Crypt' => Illuminate\Support\Facades\Crypt::class,
        'Date' => Illuminate\Support\Facades\Date::class,
        'DB' => Illuminate\Support\Facades\DB::class,
        'Debugbar' => Barryvdh\Debugbar\Facades\Debugbar::class,
        'Eloquent' => Illuminate\Database\Eloquent\Model::class,
        'Event' => Illuminate\Support\Facades\Event::class,
        'File' => Illuminate\Support\Facades\File::class,
        'Gate' => Illuminate\Support\Facades\Gate::class,
        'Hash' => Illuminate\Support\Facades\Hash::class,
        'Http' => Illuminate\Support\Facades\Http::class,
        'Lang' => Illuminate\Support\Facades\Lang::class,
        'Log' => Illuminate\Support\Facades\Log::class,
        'Mail' => Illuminate\Support\Facades\Mail::class,
        'Notification' => Illuminate\Support\Facades\Notification::class,
        'Password' => Illuminate\Support\Facades\Password::class,
        'PDF' => Barryvdh\DomPDF\Facade::class,
        'QrCode' => SimpleSoftwareIO\QrCode\Facades\QrCode::class,
        'Queue' => Illuminate\Support\Facades\Queue::class,
        'RateLimiter' => Illuminate\Support\Facades\RateLimiter::class,
        'Redirect' => Illuminate\Support\Facades\Redirect::class,
        // 'Redis' => Illuminate\Support\Facades\Redis::class,
        'Request' => Illuminate\Support\Facades\Request::class,
        'Response' => Illuminate\Support\Facades\Response::class,
        'Route' => Illuminate\Support\Facades\Route::class,
        'Schema' => Illuminate\Support\Facades\Schema::class,
        'Session' => Illuminate\Support\Facades\Session::class,
        'Storage' => Illuminate\Support\Facades\Storage::class,
        'Str' => Illuminate\Support\Str::class,
        'URL' => Illuminate\Support\Facades\URL::class,
        'Validator' => Illuminate\Support\Facades\Validator::class,
        'View' => Illuminate\Support\Facades\View::class,

    ],




    
    

];
