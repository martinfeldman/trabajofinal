<?php return array (
  'configuracion-index-basico' => 'App\\Http\\Livewire\\ConfiguracionIndexBasico',
  'configuracion-index-tabular' => 'App\\Http\\Livewire\\ConfiguracionIndexTabular',
  'dynamic-show' => 'App\\Http\\Livewire\\DynamicShow',
  'editar-usuario-tabla-permisos' => 'App\\Http\\Livewire\\EditarUsuarioTablaPermisos',
  'expedientes-administracion-index-table' => 'App\\Http\\Livewire\\ExpedientesAdministracionIndexTable',
  'expedientes-index-table' => 'App\\Http\\Livewire\\ExpedientesIndexTable',
  'expedientes-profesional-index-table' => 'App\\Http\\Livewire\\ExpedientesProfesionalIndexTable',
  'notification-counter' => 'App\\Http\\Livewire\\NotificationCounter',
  'notification-dropdown' => 'App\\Http\\Livewire\\NotificationDropdown',
  'test' => 'App\\Http\\Livewire\\Test',
);